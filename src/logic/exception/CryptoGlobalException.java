/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.exception;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoGlobalException extends Exception {

    private static final long serialVersionUID = 7718828512143293558L;
    public static final String ARRAY_EX_CHARACTER = "Caracter no encontrado en abecedario.", ARRAY_EX_NUMBER = "Número no encontrado en abecedario.",
            ARRAY_EX_NUMBER_EVEN = "Número par no encontrado en abecedario.", ARRAY_EX_NUMBER_ODD = "Número impar no encontrado en abecedario.",
            ARRAY_EX_NUMBER_PRIME = "Número primo empieza de 1.";

    public CryptoGlobalException() {
        super();
    }

    public CryptoGlobalException(String message) {
        super(message);
        System.err.println(message);
    }
}
