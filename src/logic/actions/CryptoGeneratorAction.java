/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.actions;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import logic.entity.CryptoAlphabetToAction;
import logic.entity.CryptoCharactersVariationConfiguration;
import logic.entity.CryptoConstants;
import logic.entity.CryptoTypeConfiguration;
import logic.entity.CryptoGlobalVariablesConfiguration;
import logic.entity.CryptoGradualDoubleAppConfiguration;
import logic.entity.CryptoNormalDoubleAppConfiguration;
import logic.entity.CryptoSucBouncesConfiguration;
import logic.entity.CryptoTranspositionConfiguration;
import logic.entity.CryptoWeftConfiguration;
import logic.exception.CryptoGlobalException;
import logic.methods.CryptoDirSucBouncesType;
import logic.methods.CryptoGradualDoubleApp;
import logic.methods.CryptoNormalDoubleApp;
import logic.methods.CryptoTranspositionType;
import logic.methods.CryptoWeftType;
import logic.utils.CryptoAlphabetUtils;
import logic.utils.CryptoOtherUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoGeneratorAction {

    public static String generateCrypto(CryptoGlobalVariablesConfiguration globalVariablesConfiguration, List<String> methods, CryptoTypeConfiguration typeConfiguration) throws CryptoGlobalException {
        char alphabet[], arrDirectionKeys[];
        alphabet = CryptoAlphabetUtils.generateAlphabetByParameters(typeConfiguration.getCharactersConfiguration().getOrdering(),
                typeConfiguration.getCharactersConfiguration().getLoops(), null);
        if (typeConfiguration.getCharactersConfiguration().getInitialCharacter() != null && typeConfiguration.getCharactersConfiguration().getInitialCharacter().length() > 0) {
            alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabet, typeConfiguration.getCharactersConfiguration().getInitialCharacter());
            //System.out.println(Arrays.toString(alphabet));
        }
        if (typeConfiguration.getCharactersConfiguration().isUsePhraseKey()) {
            if (globalVariablesConfiguration.getPhraseKey() != null && globalVariablesConfiguration.getPhraseKey().length() > 0) {
                alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfPhraseKey(alphabet, globalVariablesConfiguration.getPhraseKey(), globalVariablesConfiguration.getDirectionPhraseKey());
                //System.out.println(Arrays.toString(alphabet));
            }
        }
        //System.out.println(String.valueOf(alphabet));
        int[] arrNumericKeys;
        boolean arrFlags[];
        for (String method : methods) {
            String[] key = method.split(" ");
            String num = key[key.length - 1], name = method.substring(0, method.indexOf(num) - 1);
            CryptoConstants.Methods methodEnum = CryptoConstants.Methods.findMethodByCoincidenceLabel(name);
            String keyMethod = methodEnum.getKey().concat(num);
            //System.out.println(keyMethod);
            Object objMethod = typeConfiguration.getCharactersMethods().get(keyMethod);
            switch (methodEnum) {
                case CHARACTERS_VARIATION:
                    CryptoCharactersVariationConfiguration charsVarConf = (CryptoCharactersVariationConfiguration) objMethod;
                    alphabet = CryptoAlphabetUtils.sortCharactersByTypeOrdering(alphabet, charsVarConf.getOrdering(), charsVarConf.getLoops());
                    //System.out.println(Arrays.toString(alphabet));
                    if (charsVarConf.getInitialCharacter() != null && charsVarConf.getInitialCharacter().length() > 0) {
                        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabet, charsVarConf.getInitialCharacter());
                        //System.out.println(Arrays.toString(alphabet));
                    }
                    if (charsVarConf.getPhraseKey() != null && charsVarConf.getPhraseKey().length() > 0) {
                        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfPhraseKey(alphabet, charsVarConf.getPhraseKey(), CryptoConstants.FromDirection.START);
                        //System.out.println(Arrays.toString(alphabet));
                    }
                    break;
                case SUCCESSIVE_BOUNCES:
                    CryptoSucBouncesConfiguration sucBouncesConf = (CryptoSucBouncesConfiguration) objMethod;
                    if (typeConfiguration.getCharactersConfiguration().getInitialCharacter() != null && typeConfiguration.getCharactersConfiguration().getInitialCharacter().length() > 0) {
                        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabet, typeConfiguration.getCharactersConfiguration().getInitialCharacter());
                        //System.out.println(Arrays.toString(alphabet));
                    }
                    arrNumericKeys = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabet, sucBouncesConf.getNumericKey());
                    arrDirectionKeys = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabet, sucBouncesConf.getDirectionKey());
                    arrFlags = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabet);
                    alphabet = CryptoDirSucBouncesType.processAlphabet(alphabet, arrNumericKeys, arrDirectionKeys, arrFlags, sucBouncesConf.isInitial());
                    //System.out.println(Arrays.toString(alphabet));
                    break;
                case TRANSPOSITION:
                    CryptoTranspositionConfiguration transConf = (CryptoTranspositionConfiguration) objMethod;
                    alphabet = CryptoTranspositionType.processAlphabet(alphabet, transConf.getNumericKey(), transConf.getFirstRestriction(), transConf.getSecondRestriction(),
                            transConf.getThirdRestriction(), transConf.getFourthRestriction(), transConf.getFifthRestriction(), transConf.getSixthRestriction());
                    //System.out.println(Arrays.toString(alphabet));
                    break;
                case WEFT:
                    CryptoWeftConfiguration weftConf = (CryptoWeftConfiguration) objMethod;
                    alphabet = CryptoWeftType.processAlphabet(alphabet, weftConf.getNumericKey(), weftConf.getAssignRestriction(), weftConf.getSortRestriction());
                    //System.out.println(Arrays.toString(alphabet));
                    break;
                case CHARACTERS_DIVISION:
                    break;
                case NORMAL_DOUBLE_APP:
                    CryptoNormalDoubleAppConfiguration ndpConf = (CryptoNormalDoubleAppConfiguration) objMethod;
                    alphabet = CryptoNormalDoubleApp.processAlphabet(alphabet, ndpConf.getNumericKeyFirstApp(), ndpConf.getOrderingFirstAppRestriction(),
                            ndpConf.getExtractingFirstAppRestriction(), ndpConf.getNumericKeySecondApp(), ndpConf.getOrderingSecondAppRestriction(),
                            ndpConf.getExtractingSecondAppRestriction());
                    //System.out.println(Arrays.toString(alphabet));
                    break;
                case GRADUAL_DOUBLE_APP:
                    CryptoGradualDoubleAppConfiguration gdpConf = (CryptoGradualDoubleAppConfiguration) objMethod;
                    alphabet = CryptoGradualDoubleApp.processAlphabet(alphabet, gdpConf.getNumericKeyFirstApp(), gdpConf.getOrderingFirstAppRestriction(),
                            gdpConf.getExtractingFirstAppRestriction(), gdpConf.getNumericKeySecondApp(), gdpConf.getOrderingSecondAppRestriction(),
                            gdpConf.getExtractingSecondAppRestriction());
                    //System.out.println(Arrays.toString(alphabet));
                    break;
            }
        }
        validArrayLength(190, alphabet);
        return String.valueOf(alphabet);
    }

    private static void validArrayLength(int length, char alphabet[]) {
        LinkedHashSet<Character> testSet = new LinkedHashSet<>(Arrays.asList(ArrayUtils.toObject(alphabet)));
        Character[] testList = testSet.toArray(new Character[0]);
        int lengthNew = testList.length;
        if (lengthNew != length) {
            System.err.println("Longitud diferente en proceso");
            System.err.println(lengthNew);
        }
    }

    public static String ecryptText(String text) {
        int numCharacter;
        char arrTextEncrypted[] = {}, flat[] = CryptoAlphabetToAction.customFlat.toCharArray(), cripto[] = CryptoAlphabetToAction.customCripto.toCharArray();
        for (char character : text.toCharArray()) {
            numCharacter = (int) character;
            if (numCharacter == 32) {
                character = Collections.max(Arrays.asList(ArrayUtils.toObject(flat)));
            }
            arrTextEncrypted = ArrayUtils.add(arrTextEncrypted, cripto[ArrayUtils.indexOf(flat, character)]);
        }
        return String.valueOf(arrTextEncrypted);
    }

    public static String decryptText(String text) {
        int numCharacter;
        char arrTextEncrypted[] = {}, flat[] = CryptoAlphabetToAction.customFlat.toCharArray(), cripto[] = CryptoAlphabetToAction.customCripto.toCharArray();
        for (char character : text.toCharArray()) {
            character = flat[ArrayUtils.indexOf(cripto, character)];
            numCharacter = (int) character;
            if (numCharacter > 255) {
                character = (char) 32;
            }
            arrTextEncrypted = ArrayUtils.add(arrTextEncrypted, character);
        }
        return String.valueOf(arrTextEncrypted);
    }
}
