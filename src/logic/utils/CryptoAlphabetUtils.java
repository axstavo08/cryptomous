/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import logic.entity.CryptoConstants;
import logic.exception.CryptoGlobalException;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoAlphabetUtils {

    public static void printUnicodeCharacters() {
        char letra = 0;
        for (int i = 0; i <= 255; i++) {
            System.out.println("El codigo ASCII de la letra " + letra++ + " es " + i);
        }
    }

    public static char[] generateAlphabet() {
        String characters = "";//, indexUnicode = "", index = ""
        int max = 1024, min = 256, loops = 10;
        //int counter = 0;
        for (int i = -1; i <= 255; i++) {
            if ((i >= 32 && i <= 126) || (i >= 161 && i <= 255)) {
                //counter++;
                if (i == 32) {
                    i = CryptoNumbersUtils.findIndexCharacterByRangeAndLoops(max, min, loops);
                    //System.out.println((char) i);
                    characters += String.valueOf((char) i);
                    i = 32;
                } else {
                    characters += String.valueOf((char) i);
                }
                //indexUnicode += String.valueOf(i) + ",";
                //index += String.valueOf(counter) + ",";
            }
        }
        return characters.toCharArray();
    }

    public static char[] generateAlphabetByParameters(CryptoConstants.TypeArranged type, int loops, Integer numCharacterByWhiteSpaces) {
        String characters = "";
        int min = 256, max = 1024, counter = 0, posChar;
        Integer[] range = ArrayUtils.addAll(ArrayUtils.toObject(IntStream.rangeClosed(32, 126).toArray()), ArrayUtils.toObject(IntStream.rangeClosed(161, 255).toArray()));
        int[] numsCharacters = {};
        switch (type) {
            case ARRANGED:
                numsCharacters = ArrayUtils.toPrimitive(range);
                break;
            case REVERSE:
                ArrayUtils.reverse(range);
                numsCharacters = ArrayUtils.toPrimitive(range);
                break;
            case RANDOM:
                numsCharacters = CryptoNumbersUtils.sortArrayNumbersByLoops(range, loops);
                break;
        }
        //System.out.println(Arrays.toString(numsCharacters));
        do {
            posChar = numsCharacters[counter];
            characters += String.valueOf((char) ((posChar == 32)
                    ? (numCharacterByWhiteSpaces != null) ? numCharacterByWhiteSpaces : CryptoNumbersUtils.findIndexCharacterByRangeAndLoops(max, min, loops)
                    : posChar));
            counter++;
        } while (counter < numsCharacters.length);
        return characters.toCharArray();
    }

    public static char[] sortCharactersByTypeOrdering(char[] alphabet, CryptoConstants.TypeArranged type, int loops) {
        switch (type) {
            case REVERSE:
                ArrayUtils.reverse(alphabet);
                break;
            case RANDOM:
                List<Character> list;
                if (loops > 0) {
                    int counter = 0;
                    list = Arrays.asList(ArrayUtils.toObject(alphabet));
                    do {
                        Collections.shuffle(list);
                        counter++;
                    } while (counter < loops);
                } else {
                    list = Arrays.asList(ArrayUtils.toObject(alphabet));
                    Collections.shuffle(list);
                }
                alphabet = ArrayUtils.toPrimitive((Character[]) list.toArray());
                break;
        }
        return alphabet;
    }

    public static char findCharacterInsteadOfWhiteSpace(int loops) {
        int min = 256, max = 1024;
        return (char) CryptoNumbersUtils.findIndexCharacterByRangeAndLoops(max, min, loops);
    }

    public static char[] generateAlphabetOnlyLowercaseLetters() {
        String characters = "";
        for (int i = -1; i <= 255; i++) {
            if (i >= 97 && i <= 122) {
                characters += String.valueOf((char) i);
            }
        }
        return characters.toCharArray();
    }

    public static char[] generateAlphabetOnlyUpperLetters() {
        String characters = "";
        for (int i = -1; i <= 255; i++) {
            if (i >= 65 && i <= 90) {
                characters += String.valueOf((char) i);
            }
        }
        return characters.toCharArray();
    }

    public static char[] convertAlphabetToInverse(char alphabet[]) {
        String characters = "";
        for (int i = (alphabet.length - 1); i >= 0; i--) {
            characters += alphabet[i];
        }
        return characters.toCharArray();
    }

    public static char[] convertAlphabetStartingOfCharacter(char alphabet[], String character) throws CryptoGlobalException {
        String characters = String.valueOf(alphabet);
        try {
            int indexChar = characters.indexOf(character), totalIndex = alphabet.length - 1;
            char firstGArr[] = Arrays.copyOfRange(alphabet, indexChar, alphabet.length);
            if ((totalIndex - indexChar) < totalIndex) {
                char secondGArr[] = Arrays.copyOfRange(alphabet, 0, indexChar);
                return ArrayUtils.addAll(firstGArr, secondGArr);
            }
            return firstGArr;
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new CryptoGlobalException(CryptoGlobalException.ARRAY_EX_CHARACTER);
        }
    }

    public static char[][] divideAlphabetByNumberSegments(char alphabet[], int segments) {
        char divideAlphabet[][] = {}, alphabetTransSegment[];
        int quantityAlphabet = alphabet.length, multiTimes = quantityAlphabet / segments, restTimes = quantityAlphabet % segments, counterTimes = 0,
                indexStartSegment = 0, indexEndSegment = indexStartSegment;
        do {
            indexEndSegment += multiTimes;
            alphabetTransSegment = ArrayUtils.subarray(alphabet, indexStartSegment, indexEndSegment);
            divideAlphabet = ArrayUtils.add(divideAlphabet, alphabetTransSegment);
            indexStartSegment = indexEndSegment;
            counterTimes++;
        } while (counterTimes < segments);
        if (restTimes > 0) {
            indexEndSegment += restTimes;
            alphabetTransSegment = ArrayUtils.subarray(alphabet, indexStartSegment, indexEndSegment);
            divideAlphabet = ArrayUtils.add(divideAlphabet, alphabetTransSegment);
        }
        return divideAlphabet;
    }

    public static char[] convertAlphabetStartingOfPhraseKey(char alphabet[], String phraseKey, CryptoConstants.FromDirection direction) throws CryptoGlobalException {
        int index;
        phraseKey = phraseKey.replace(" ", "");
        String[] arrPhrase = new LinkedHashSet<>(Arrays.asList(phraseKey.split(""))).toArray(new String[0]);
        char[] charPhrase = {};
        for (String letter : arrPhrase) {
            charPhrase = ArrayUtils.add(charPhrase, letter.charAt(0));
            index = ArrayUtils.indexOf(alphabet, letter.charAt(0));
            if (index != -1) {
                alphabet = ArrayUtils.remove(alphabet, index);
            }
        }
        switch (direction) {
            case START:
                alphabet = ArrayUtils.addAll(charPhrase, alphabet);
                break;
            case END:
                ArrayUtils.reverse(alphabet);
                alphabet = ArrayUtils.addAll(alphabet, charPhrase);
                break;
        }
        return alphabet;
    }

    public static String generateRandomDirectionKeyByParameter(int quantity) {
        String key = "", separator = ",", directions = "ID";
        Random random = new Random();
        int counter = 0;
        char character;
        do {
            counter++;
            character = directions.charAt(random.nextInt(directions.length()));
            key = key.concat(String.valueOf(character));
            if (counter < quantity) {
                key = key.concat(separator);
            }
        } while (counter < quantity);
        return key;
    }

    /*public static void main(String[] args) throws CryptoGlobalException {
        String key = "GUSTAVO123 PRUEBA@";
        char alphabet[] = generateAlphabetByParameters(CryptoConstants.TypeArranged.RANDOM, 999);
        System.out.println(Arrays.toString(alphabet));
        alphabet = convertAlphabetStartingOfPhraseKey(alphabet, key, CryptoConstants.FromDirection.END);
        System.out.println(Arrays.toString(alphabet));
        int quantity = 20, counter = 0;
        String chars = "ID";
        Random random = new Random();
        String key = "", separator = ",";
        char character;
        do {
            counter++;
            character = chars.charAt(random.nextInt(chars.length()));
            key = key.concat(String.valueOf(character));
            if (counter < quantity) {
                key = key.concat(separator);
            }
        } while (counter < quantity);
        System.out.println(key);
    }*/
}
