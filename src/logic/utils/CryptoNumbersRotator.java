/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.utils;

import com.sun.istack.NotNull;
import logic.exception.CryptoGlobalException;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoNumbersRotator {
    
    public static void rotateNumberArraysByKey(int[] array, @NotNull String key, boolean ignoreException) throws CryptoGlobalException {
        int index = ArrayUtils.indexOf(array, Integer.parseInt(key));
        if (index != -1) {
            index++;
            rotateArrayByStartingIndex(array, index);
        } else {
            if (!ignoreException) {
                throw new CryptoGlobalException(CryptoGlobalException.ARRAY_EX_NUMBER);
            }
        }
    }

    public static int[] rotateNumberArraysByEvenAndOddKeys(int[] array, String evenKey, String oddKey, boolean ignoreException) throws CryptoGlobalException {
        int[] evenArray = {}, oddArray = {};
        Object[] helperArray;
        helperArray = CryptoNumbersUtils.findEvenAndOddNumbers(array, evenArray, oddArray);
        evenArray = (int[]) helperArray[0];
        oddArray = (int[]) helperArray[1];
        if (evenKey != null) {
            rotateNumberArraysByKey(evenArray, evenKey, ignoreException);
        }
        if (oddKey != null) {
            rotateNumberArraysByKey(oddArray, oddKey, ignoreException);
        }
        return ArrayUtils.addAll(evenArray, oddArray);
    }

    public static int[] rotateNumberArraysByOddAndEvenKeys(int[] array, String oddKey, String evenKey, boolean ignoreException) throws CryptoGlobalException {
        int[] oddArray = {}, evenArray = {};
        Object[] helperArray;
        helperArray = CryptoNumbersUtils.findEvenAndOddNumbers(array, evenArray, oddArray);
        evenArray = (int[]) helperArray[0];
        oddArray = (int[]) helperArray[1];
        if (evenKey != null) {
            rotateNumberArraysByKey(evenArray, evenKey, ignoreException);
        }
        if (oddKey != null) {
            rotateNumberArraysByKey(oddArray, oddKey, ignoreException);
        }
        return ArrayUtils.addAll(oddArray, evenArray);
    }

    public static void rotateNumberArraysByKeyOther(int[] array, String key) throws CryptoGlobalException {
        int index = ArrayUtils.indexOf(array, Integer.parseInt(key));
        if (index == -1) {
            throw new CryptoGlobalException(CryptoGlobalException.ARRAY_EX_NUMBER_EVEN);
        }
        index++;
        rotateArrayByStartingIndexOther(array, index);
    }

    private static void rotateArrayByStartingIndex(int[] array, int index) throws CryptoGlobalException {
        if (index < 0 || index > array.length) {
            throw new CryptoGlobalException(CryptoGlobalException.ARRAY_EX_NUMBER);
        }
        for (int i = 0, j = index - 2; i < j; i++, j--) {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }
        for (int i = index - 1, j = array.length - 1; i < j; i++, j--) {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }
        for (int i = 0, j = array.length - 1; i < j; i++, j--) {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }
    }

    private static void rotateArrayByStartingIndexOther(int[] array, int index) throws CryptoGlobalException {
        if (index < 0 || index > array.length) {
            throw new CryptoGlobalException(CryptoGlobalException.ARRAY_EX_NUMBER);
        }
        for (int i = 0; i < index - 1; i++) {
            int tmp = array[0];
            for (int j = 0; j < array.length - 1; j++) {
                array[j] = array[j + 1];
            }
            array[array.length - 1] = tmp;
        }
    }
}
