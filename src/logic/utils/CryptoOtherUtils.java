/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.utils;

import java.util.Arrays;
import logic.entity.CryptoNumbersSort;
import logic.entity.CryptoNumbersSortStructure;
import logic.exception.CryptoGlobalException;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoOtherUtils {

    public static int[] buildNumericKeysByInitialKeyOnBasesAlphabet(char alphabet[], String key) {
        int[] keys = Arrays.stream(key.split(",")).mapToInt(Integer::parseInt).toArray(), finalKeys = {};
        int indexKeys = keys.length, multiTimes = alphabet.length / indexKeys, restTimes = alphabet.length % indexKeys, counter = 0;
        do {
            finalKeys = ArrayUtils.addAll(finalKeys, keys);
            counter++;
        } while (counter < multiTimes);
        for (int i = 0; i < restTimes; i++) {
            finalKeys = ArrayUtils.add(finalKeys, keys[i]);
        }
        return finalKeys;
    }

    public static int[] buildNumericKeysByKeyValuesOnBasesAlphabet(char alphabet[], int[] keys) {
        int[] finalKeys = {};
        int indexKeys = keys.length, multiTimes = alphabet.length / indexKeys, restTimes = alphabet.length % indexKeys, counter = 0;
        do {
            finalKeys = ArrayUtils.addAll(finalKeys, keys);
            counter++;
        } while (counter < multiTimes);
        for (int i = 0; i < restTimes; i++) {
            finalKeys = ArrayUtils.add(finalKeys, keys[i]);
        }
        return finalKeys;
    }

    public static boolean[] buildFlagsOnBasesAlphabet(char alphabet[]) {
        boolean[] flagNumericDir = {};
        int indexAlphabet = alphabet.length, counter = 0;
        do {
            flagNumericDir = ArrayUtils.add(flagNumericDir, true);
            counter++;
        } while (counter < indexAlphabet);
        return flagNumericDir;
    }

    public static char[] buildDirectionKeysByInitialDirOnBasesAlphabet(char alphabet[], String direction) {
        int indexDir = direction.length(), multiTimes = alphabet.length / indexDir, restTimes = alphabet.length % indexDir, counter = 0;
        String finalDirection = "";
        do {
            finalDirection += direction;
            counter++;
        } while (counter < multiTimes);
        finalDirection += direction.substring(0, restTimes);
        return finalDirection.toCharArray();
    }

    public static int[] buildNumericKeysByKeyOnBasedParameters(String[] keys, CryptoNumbersSort structure) throws CryptoGlobalException {
        int[] numKeys = {}, keysHelper = Arrays.stream(keys).mapToInt(Integer::parseInt).toArray();
        if (structure != null) {
            CryptoNumbersSort.TypeSort typeSort = structure.getType();
            CryptoNumbersSort.TypeNumber typeNumber = structure.getTypeNumber();
            switch (typeSort) {
                case NORMAL:
                    CryptoNumbersSortStructure fStructure = structure.getFirstStructure();
                    if (fStructure != null) {
                        CryptoNumbersSortStructure.DirectionSort directionSort = fStructure.getDirection();
                        String initialSort = fStructure.getInitial();
                        Integer[] helperNormalKeys = ArrayUtils.toObject(keysHelper);
                        switch (directionSort) {
                            case ASCENDANT:
                                Arrays.sort(helperNormalKeys, new CryptoNumbersComparator(CryptoNumbersComparator.ComparationCases.ONLY_ASCENDANT));
                                break;
                            case DESCENDANT:
                                Arrays.sort(helperNormalKeys, new CryptoNumbersComparator(CryptoNumbersComparator.ComparationCases.ONLY_DESCENDANT));
                                break;
                            default:
                                Arrays.sort(helperNormalKeys, new CryptoNumbersComparator(CryptoNumbersComparator.ComparationCases.ONLY_ASCENDANT));
                                break;
                        }
                        numKeys = ArrayUtils.toPrimitive(helperNormalKeys);
                        if (initialSort != null) {
                            CryptoNumbersRotator.rotateNumberArraysByKey(numKeys, initialSort, true);
                        }
                    }
                    break;
                case COMPOUND:
                    CryptoNumbersSortStructure firstStructure = structure.getFirstStructure(),
                     secondStructure = structure.getSecondStructure();
                    CryptoNumbersSortStructure.DirectionSort firstDirectionSort,
                     secondDirectionSort;
                    String firstInitialSort = null,
                     secondInitialSort = null;
                    Boolean firstTypeNumber = Boolean.TRUE,
                     secondTypeNumber = Boolean.TRUE;
                    if (firstStructure != null) {
                        firstDirectionSort = structure.getFirstStructure().getDirection();
                        firstInitialSort = structure.getFirstStructure().getInitial();
                        if (firstDirectionSort == CryptoNumbersSortStructure.DirectionSort.DESCENDANT) {
                            firstTypeNumber = Boolean.FALSE;
                        }
                    }
                    if (secondStructure != null) {
                        secondDirectionSort = structure.getSecondStructure().getDirection();
                        secondInitialSort = structure.getSecondStructure().getInitial();
                        if (secondDirectionSort == CryptoNumbersSortStructure.DirectionSort.DESCENDANT) {
                            secondTypeNumber = Boolean.FALSE;
                        }
                    } else {
                        secondTypeNumber = firstTypeNumber;
                    }
                    Integer[] helperCompoundKeys = ArrayUtils.toObject(keysHelper);
                    switch (typeNumber) {
                        case EVEN:
                            Arrays.sort(helperCompoundKeys, CryptoNumbersComparator.createComparatorByEvenAndOdd(firstTypeNumber, secondTypeNumber));
                            numKeys = ArrayUtils.toPrimitive(helperCompoundKeys);
                            numKeys = CryptoNumbersRotator.rotateNumberArraysByEvenAndOddKeys(numKeys, firstInitialSort, secondInitialSort, true);
                            break;
                        case ODD:
                            Arrays.sort(helperCompoundKeys, CryptoNumbersComparator.createComparatorByOddAndEven(firstTypeNumber, secondTypeNumber));
                            numKeys = ArrayUtils.toPrimitive(helperCompoundKeys);
                            numKeys = CryptoNumbersRotator.rotateNumberArraysByOddAndEvenKeys(numKeys, firstInitialSort, secondInitialSort, true);
                            break;
                    }
                    break;
            }
        }
        if (numKeys.length == 0) {
            numKeys = keysHelper;
        }
        return numKeys;
    }
}
