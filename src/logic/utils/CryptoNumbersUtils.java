/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.utils;

import com.sun.istack.NotNull;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import logic.exception.CryptoGlobalException;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoNumbersUtils {

    public static Object[] findEvenAndOddNumbers(int[] array, int[] evenArray, int[] oddArray) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                evenArray = ArrayUtils.add(evenArray, array[i]);
            } else {
                oddArray = ArrayUtils.add(oddArray, array[i]);
            }
        }
        return new Object[]{evenArray, oddArray};
    }

    public static int[] findListPrimeNumbersByLimitNumbers(Integer min, @NotNull int max) throws CryptoGlobalException {
        int[] array = {};
        int counter;
        if (min == null) {
            min = 1;
        }
        if (min < 1) {
            throw new CryptoGlobalException(CryptoGlobalException.ARRAY_EX_NUMBER_PRIME);
        }
        for (int i = min; i <= max; i++) {
            counter = 0;
            for (int j = i; j >= 1; j--) {
                if (i % j == 0) {
                    counter = counter + 1;
                }
            }
            if (counter == 2) {
                array = ArrayUtils.add(array, i);
            }
        }
        return array;
    }

    public static int[] findPrimeNumbersByKeyPositions(@NotNull String key) throws CryptoGlobalException {
        int[] array = {}, arrayHelper = {}, numKeys = Arrays.stream(key.split(",")).mapToInt(Integer::parseInt).toArray(), numKeysHelper = ArrayUtils.clone(numKeys);
        Arrays.sort(numKeysHelper);
        int counter, position = 0, numCounter = 1, lastPosition = numKeysHelper[numKeysHelper.length - 1];
        do {
            counter = 0;
            for (int j = numCounter; j >= 1; j--) {
                if (numCounter % j == 0) {
                    counter = counter + 1;
                }
            }
            if (counter == 2) {
                arrayHelper = ArrayUtils.add(arrayHelper, numCounter);
                position++;
            }
            numCounter++;
        } while (position < lastPosition);
        for (int i = 0; i < numKeys.length; i++) {
            array = ArrayUtils.add(array, arrayHelper[numKeys[i] - 1]);
        }
        return array;
    }

    public static int findIndexCharacterByRangeAndLoops(int max, int min, int loops) {
        Random random = new Random();
        int indexChar = 0;
        for (int i = 0; i < loops; i++) {
            indexChar = random.nextInt((max - min) + 1) + min;
            //System.out.println(indexChar);
        }
        //char character = (char) indexChar;
        return indexChar;
    }

    public static int[] sortArrayNumbersByLoops(Integer[] array, int loops) {
        int counter = 0;
        List<Integer> list = Arrays.asList(array);
        do {
            Collections.shuffle(list);
            counter++;
        } while (counter < loops);
        return ArrayUtils.toPrimitive((Integer[]) list.toArray());
    }

    public static int[] buildRandomValuesByParameters(int min, int max, int quantity) {
        int[] numberKeys = {};
        int randomNumber, counter = 0, existenceNumber;
        Random random = new Random();
        do {
            randomNumber = random.nextInt((max - min) + 1) + min;
            existenceNumber = ArrayUtils.indexOf(numberKeys, randomNumber);
            if (existenceNumber == -1) {
                numberKeys = ArrayUtils.add(numberKeys, randomNumber);
                counter++;
            }
        } while (counter < quantity);
        //System.out.println(Arrays.toString(numberKeys));
        return numberKeys;
    }

    public static String buildKeysByArrayNumbers(int[] numbers) {
        String keys = "", separator = ",";
        for (int i = 0; i < numbers.length; i++) {
            keys = keys.concat(String.valueOf(numbers[i]));
            if (i < numbers.length - 1) {
                keys = keys.concat(separator);
            }
        }
        //System.out.println(keys);
        return keys;
    }

    /*public static void main(String[] args) throws CryptoGlobalException {
        int minA = 0, maxA = 1000, quantityA = 10;
        int[] arrA = buildRandomValuesByParameters(minA, maxA, quantityA);
        buildKeysByArrayNumbers(arrA);
        String keyPrimes = "100,55,67,90,106,425,76,12,56,4,5,3,6,1,2";
        int[] arrB = findPrimeNumbersByKeyPositions(keyPrimes);
        buildKeysByArrayNumbers(arrB);
    }*/
}
