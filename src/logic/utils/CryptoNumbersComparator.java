/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.utils;

import java.util.Comparator;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoNumbersComparator implements Comparator<Integer> {

    private final ComparationCases compareCase;

    public CryptoNumbersComparator() {
        compareCase = ComparationCases.ONLY_ASCENDANT;
    }

    public CryptoNumbersComparator(ComparationCases compCase) {
        compareCase = compCase;
    }

    public static CryptoNumbersComparator createComparatorByEvenAndOdd(Boolean isEvenAscendant, Boolean isOddAscendant) {
        boolean evenSortAsc, oddSortAsc;
        ComparationCases preCompareCase;
        evenSortAsc = (isEvenAscendant == null) ? true : isEvenAscendant;
        oddSortAsc = (isOddAscendant == null) ? true : isOddAscendant;
        if (evenSortAsc && oddSortAsc) {
            preCompareCase = ComparationCases.EVEN_ASCENDANT_AND_ODD_ASCENDANT;
        } else if (evenSortAsc && !oddSortAsc) {
            preCompareCase = ComparationCases.EVEN_ASCENDANT_AND_ODD_DESCENDANT;
        } else if (!evenSortAsc && oddSortAsc) {
            preCompareCase = ComparationCases.EVEN_DESCENDANT_AND_ODD_ASCENDANT;
        } else {
            preCompareCase = ComparationCases.EVEN_DESCENDANT_AND_ODD_DESCENDANT;
        }
        return new CryptoNumbersComparator(preCompareCase);
    }

    public static CryptoNumbersComparator createComparatorByOddAndEven(Boolean isOddAscendant, Boolean isEvenAscendant) {
        boolean oddSortAsc, evenSortAsc;
        ComparationCases preCompareCase;
        oddSortAsc = (isOddAscendant == null) ? true : isOddAscendant;
        evenSortAsc = (isEvenAscendant == null) ? true : isEvenAscendant;
        if (oddSortAsc && evenSortAsc) {
            preCompareCase = ComparationCases.ODD_ASCENDANT_AND_EVEN_ASCENDANT;
        } else if (oddSortAsc && !evenSortAsc) {
            preCompareCase = ComparationCases.ODD_ASCENDANT_AND_EVEN_DESCENDANT;
        } else if (!oddSortAsc && evenSortAsc) {
            preCompareCase = ComparationCases.ODD_DESCENDANT_AND_EVEN_ASCENDANT;
        } else {
            preCompareCase = ComparationCases.ODD_DESCENDANT_AND_EVEN_DESCENDANT;
        }
        return new CryptoNumbersComparator(preCompareCase);
    }

    public static enum ComparationCases {
        ONLY_ASCENDANT, ONLY_DESCENDANT, EVEN_ASCENDANT_AND_ODD_ASCENDANT, EVEN_ASCENDANT_AND_ODD_DESCENDANT, EVEN_DESCENDANT_AND_ODD_ASCENDANT,
        EVEN_DESCENDANT_AND_ODD_DESCENDANT, ODD_ASCENDANT_AND_EVEN_ASCENDANT, ODD_ASCENDANT_AND_EVEN_DESCENDANT, ODD_DESCENDANT_AND_EVEN_ASCENDANT,
        ODD_DESCENDANT_AND_EVEN_DESCENDANT;
    }

    @Override
    public int compare(Integer num1, Integer num2) {
        int comparation, modNum1 = Math.abs(num1 % 2), modNum2 = Math.abs(num2 % 2);
        switch (compareCase) {
            case ONLY_ASCENDANT:
                comparation = num1.compareTo(num2);
                break;
            case ONLY_DESCENDANT:
                comparation = num2.compareTo(num1);
                break;
            case EVEN_ASCENDANT_AND_ODD_ASCENDANT:
                comparation = ((modNum1 == modNum2) ? ((modNum1 == 0) ? num1.compareTo(num2) : num1.compareTo(num2)) : ((modNum1 < modNum2) ? -1 : 1));
                break;
            case EVEN_ASCENDANT_AND_ODD_DESCENDANT:
                comparation = ((modNum1 == modNum2) ? ((modNum1 == 0) ? num1.compareTo(num2) : num2.compareTo(num1)) : ((modNum1 < modNum2) ? -1 : 1));
                break;
            case EVEN_DESCENDANT_AND_ODD_ASCENDANT:
                comparation = ((modNum1 == modNum2) ? ((modNum1 == 0) ? num2.compareTo(num1) : num1.compareTo(num2)) : ((modNum1 < modNum2) ? -1 : 1));
                break;
            case EVEN_DESCENDANT_AND_ODD_DESCENDANT:
                comparation = ((modNum1 == modNum2) ? ((modNum1 == 0) ? num2.compareTo(num1) : num2.compareTo(num1)) : ((modNum1 < modNum2) ? -1 : 1));
                break;
            case ODD_ASCENDANT_AND_EVEN_ASCENDANT:
                comparation = ((modNum1 == modNum2) ? ((modNum1 == 0) ? num1.compareTo(num2) : num1.compareTo(num2)) : ((modNum1 > modNum2) ? -1 : 1));
                break;
            case ODD_ASCENDANT_AND_EVEN_DESCENDANT:
                comparation = ((modNum1 == modNum2) ? ((modNum1 == 0) ? num2.compareTo(num1) : num1.compareTo(num2)) : ((modNum1 > modNum2) ? -1 : 1));
                break;
            case ODD_DESCENDANT_AND_EVEN_ASCENDANT:
                comparation = ((modNum1 == modNum2) ? ((modNum1 == 0) ? num1.compareTo(num2) : num2.compareTo(num1)) : ((modNum1 > modNum2) ? -1 : 1));
                break;
            case ODD_DESCENDANT_AND_EVEN_DESCENDANT:
                comparation = ((modNum1 == modNum2) ? ((modNum1 == 0) ? num2.compareTo(num1) : num2.compareTo(num1)) : ((modNum1 > modNum2) ? -1 : 1));
                break;
            default:
                comparation = 0;
                break;
        }
        return comparation;
    }
}
