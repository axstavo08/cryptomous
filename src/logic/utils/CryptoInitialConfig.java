/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.utils;

import logic.entity.CryptoAlphabetToAction;
import logic.entity.CryptoConstants;
import logic.entity.CryptoNumbersSort;
import logic.entity.CryptoNumbersSortStructure;
import logic.exception.CryptoGlobalException;
import logic.methods.CryptoDirSucBouncesType;
import logic.methods.CryptoGradualDoubleApp;
import logic.methods.CryptoNormalDoubleApp;
import logic.methods.CryptoTranspositionType;
import logic.methods.CryptoWeftType;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoInitialConfig {

    public static void configAlphabet() throws CryptoGlobalException {
        char alphabet[];
        String phraseKey = "GUSTAVO123 PRUEBA@", numericKey = "34,87,45,99,150,400,97,320", initialCharacter = "@";
        int loops = 999;
        CryptoConstants.TypeArranged typeArranged = CryptoConstants.TypeArranged.RANDOM;
        CryptoConstants.FromDirection directionType = CryptoConstants.FromDirection.END;
        alphabet = CryptoAlphabetUtils.generateAlphabetByParameters(typeArranged, loops, null);
        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabet, initialCharacter);
        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfPhraseKey(alphabet, phraseKey, directionType);
        String initialCharacterSucBouncesCripto1 = "$", numericKeySucBouncesCripto1 = "200,467,100,87,23,45,76,39", directionKeySucBouncesCripto1 = "IDIIDIDDIII";
        int[] arrNumericKeySucBouncesCripto1 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabet, numericKeySucBouncesCripto1);
        char arrDirectionKeySucBouncesCripto1[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabet, directionKeySucBouncesCripto1);
        boolean arrFlagsSucBouncesCripto1[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabet);
        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabet, initialCharacterSucBouncesCripto1);
        alphabet = CryptoDirSucBouncesType.processAlphabet(alphabet, arrNumericKeySucBouncesCripto1, arrDirectionKeySucBouncesCripto1, arrFlagsSucBouncesCripto1, true);
        String transKeysCripto1 = null;
        if (transKeysCripto1 == null) {
            transKeysCripto1 = numericKey;
        }
        CryptoNumbersSort transOptionCriptoA1 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "150"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "45"), CryptoNumbersSort.TypeNumber.EVEN);
        CryptoNumbersSort transOptionCriptoB1 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "99"),
                null, CryptoNumbersSort.TypeNumber.ODD);
        CryptoNumbersSort transOptionCriptoC1 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "400"), null);
        alphabet = CryptoTranspositionType.processAlphabet(alphabet, transKeysCripto1, transOptionCriptoA1, transOptionCriptoB1, transOptionCriptoC1);
        String numericKeySucBouncesCripto2 = "876,32,45,101,215,467,90,191", directionKeySucBouncesCripto2 = "D";
        int[] arrNumericKeySucBouncesCripto2 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabet, numericKeySucBouncesCripto2);
        char arrDirectionKeySucBouncesCripto2[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabet, directionKeySucBouncesCripto2);
        boolean arrFlagsSucBouncesCripto2[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabet);
        alphabet = CryptoDirSucBouncesType.processAlphabet(alphabet, arrNumericKeySucBouncesCripto2, arrDirectionKeySucBouncesCripto2, arrFlagsSucBouncesCripto2, false);
        String numericKeyWeftCripto1 = "34,67,324,576,87,109,312,76";
        CryptoNumbersSort weftOptionCriptoA1 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "109"), null);
        CryptoNumbersSort weftOptionCriptoA2 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "67"),
                null, CryptoNumbersSort.TypeNumber.ODD);
        alphabet = CryptoWeftType.processAlphabet(alphabet, numericKeyWeftCripto1, weftOptionCriptoA1, weftOptionCriptoA2);
        char alphabetCompundFinalCharCripto1[] = {}, alphabetCompoundTempCharCripto1[][] = CryptoAlphabetUtils.divideAlphabetByNumberSegments(alphabet, 12);
        String numericKeysCompSucBounces1 = null;
        if (numericKeysCompSucBounces1 == null) {
            numericKeysCompSucBounces1 = numericKey;
        }
        String directionKeysCompSucBounces1 = "IDDIIDDID";
        int[] arrNumericKeysCompSucBounces1 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[0], numericKeysCompSucBounces1);
        char arrDirectionKeysCompSucBounces1[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[0], directionKeysCompSucBounces1);
        boolean arrFlagsCompSucBounces1[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[0]);
        alphabetCompoundTempCharCripto1[0] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[0], arrNumericKeysCompSucBounces1, arrDirectionKeysCompSucBounces1, arrFlagsCompSucBounces1, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[0]);
        String transKeysCripto2 = null;
        if (transKeysCripto2 == null) {
            transKeysCripto2 = numericKey;
        }
        CryptoNumbersSort transOptionCriptoA2 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "400"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "87"), CryptoNumbersSort.TypeNumber.EVEN);
        CryptoNumbersSort transOptionCriptoB2 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "97"),
                null, CryptoNumbersSort.TypeNumber.ODD);
        CryptoNumbersSort transOptionCriptoC2 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "320"), null);
        alphabetCompoundTempCharCripto1[1] = CryptoTranspositionType.processAlphabet(alphabetCompoundTempCharCripto1[1], transKeysCripto2, transOptionCriptoA2, transOptionCriptoB2, transOptionCriptoC2);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[1]);
        String numericKeysCompSucBounces3 = "320,90,123,654,765,12", directionKeysCompSucBounces3 = "DIDD";
        int[] arrNumericKeysCompSucBounces3 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[2], numericKeysCompSucBounces3);
        char arrDirectionKeysCompSucBounces3[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[2], directionKeysCompSucBounces3);
        boolean arrFlagsCompSucBounces3[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[2]);
        alphabetCompoundTempCharCripto1[2] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[2], arrNumericKeysCompSucBounces3, arrDirectionKeysCompSucBounces3, arrFlagsCompSucBounces3, true);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[2]);
        String numericKeysCompSucBounces4 = "123,65,345,698,100,54,23", directionKeysCompSucBounces4 = "I";
        int[] arrNumericKeysCompSucBounces4 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[3], numericKeysCompSucBounces4);
        char arrDirectionKeysCompSucBounces4[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[3], directionKeysCompSucBounces4);
        boolean arrFlagsCompSucBounces4[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[3]);
        alphabetCompoundTempCharCripto1[3] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[3], arrNumericKeysCompSucBounces4, arrDirectionKeysCompSucBounces4, arrFlagsCompSucBounces4, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[3]);
        String numericKeysCompSucBounces5 = null;
        if (numericKeysCompSucBounces5 == null) {
            numericKeysCompSucBounces5 = numericKey;
        }
        String directionKeysCompSucBounces5 = "IDIDDII";
        int[] arrNumericKeysCompSucBounces5 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[4], numericKeysCompSucBounces5);
        char arrDirectionKeysCompSucBounces5[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[4], directionKeysCompSucBounces5);
        boolean arrFlagsCompSucBounces5[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[4]);
        alphabetCompoundTempCharCripto1[4] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[4], arrNumericKeysCompSucBounces5, arrDirectionKeysCompSucBounces5, arrFlagsCompSucBounces5, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[4]);
        String transKeysCripto3 = "45,67,123,546,876,980,32";
        CryptoNumbersSort transOptionCriptoA3 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "980"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "67"), CryptoNumbersSort.TypeNumber.ODD);
        CryptoNumbersSort transOptionCriptoB3 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "546"),
                null, CryptoNumbersSort.TypeNumber.EVEN);
        CryptoNumbersSort transOptionCriptoC3 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "32"), null);
        alphabetCompoundTempCharCripto1[5] = CryptoTranspositionType.processAlphabet(alphabetCompoundTempCharCripto1[5], transKeysCripto3,
                transOptionCriptoA3, transOptionCriptoB3, transOptionCriptoC3);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[5]);
        String numericKeysCompSucBounces7 = null;
        if (numericKeysCompSucBounces7 == null) {
            numericKeysCompSucBounces7 = numericKey;
        }
        String directionKeysCompSucBounces7 = "IDDIIDDID";
        int[] arrNumericKeysCompSucBounces7 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[6], numericKeysCompSucBounces7);
        char arrDirectionKeysCompSucBounces7[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[6], directionKeysCompSucBounces7);
        boolean arrFlagsCompSucBounces7[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[6]);
        alphabetCompoundTempCharCripto1[6] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[6], arrNumericKeysCompSucBounces7, arrDirectionKeysCompSucBounces7, arrFlagsCompSucBounces7, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[6]);
        String transKeysCripto8 = "430,87,65,124,654,76";
        CryptoNumbersSort transOptionCriptoA4 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "430"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "65"), CryptoNumbersSort.TypeNumber.EVEN);
        CryptoNumbersSort transOptionCriptoB4 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "76"),
                null, CryptoNumbersSort.TypeNumber.EVEN);
        CryptoNumbersSort transOptionCriptoC4 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "87"), null);
        alphabetCompoundTempCharCripto1[7] = CryptoTranspositionType.processAlphabet(alphabetCompoundTempCharCripto1[7], transKeysCripto8, transOptionCriptoA4,
                transOptionCriptoB4, transOptionCriptoC4);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[7]);
        String numericKeysCompSucBounces8 = "123,65,345,698,100,54,23", directionKeysCompSucBounces8 = "IIDIIDIIDDD";
        int[] arrNumericKeysCompSucBounces8 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[8], numericKeysCompSucBounces8);
        char arrDirectionKeysCompSucBounces8[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[8], directionKeysCompSucBounces8);
        boolean arrFlagsCompSucBounces8[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[8]);
        alphabetCompoundTempCharCripto1[8] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[8], arrNumericKeysCompSucBounces8, arrDirectionKeysCompSucBounces8, arrFlagsCompSucBounces8, true);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[8]);
        String numericKeysCompSucBounces9 = null;
        if (numericKeysCompSucBounces9 == null) {
            numericKeysCompSucBounces9 = numericKey;
        }
        String directionKeysCompSucBounces9 = "DDDDIIDIDII";
        int[] arrNumericKeysCompSucBounces9 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[9], numericKeysCompSucBounces9);
        char arrDirectionKeysCompSucBounces9[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[9], directionKeysCompSucBounces9);
        boolean arrFlagsCompSucBounces9[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[9]);
        alphabetCompoundTempCharCripto1[9] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[9], arrNumericKeysCompSucBounces9,
                arrDirectionKeysCompSucBounces9, arrFlagsCompSucBounces9, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[9]);
        String numericKeysCompSucBounces10 = "320,345,69,65,154,967,63,267", directionKeysCompSucBounces10 = "IDIIIDDIIID";
        int[] arrNumericKeysCompSucBounces10 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[10], numericKeysCompSucBounces10);
        char arrDirectionKeysCompSucBounces10[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[10], directionKeysCompSucBounces10);
        boolean arrFlagsCompSucBounces10[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[10]);
        alphabetCompoundTempCharCripto1[10] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[10], arrNumericKeysCompSucBounces10,
                arrDirectionKeysCompSucBounces10, arrFlagsCompSucBounces10, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[10]);
        String numericKeysCompSucBounces11 = "435,654,12,790,546,325,768", directionKeysCompSucBounces11 = "IDIIIDDIIID";
        int[] arrNumericKeysCompSucBounces11 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[11], numericKeysCompSucBounces11);
        char arrDirectionKeysCompSucBounces11[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[11], directionKeysCompSucBounces11);
        boolean arrFlagsCompSucBounces11[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[11]);
        alphabetCompoundTempCharCripto1[11] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[11], arrNumericKeysCompSucBounces11,
                arrDirectionKeysCompSucBounces11, arrFlagsCompSucBounces11, true);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[11]);
        String transKeysCripto9 = null;
        if (transKeysCripto9 == null) {
            transKeysCripto9 = numericKey;
        }
        CryptoNumbersSort transOptionCriptoA5 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "34"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "99"), CryptoNumbersSort.TypeNumber.EVEN);
        CryptoNumbersSort transOptionCriptoB5 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "97"),
                null, CryptoNumbersSort.TypeNumber.ODD);
        CryptoNumbersSort transOptionCriptoC5 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "400"), null);
        alphabetCompoundTempCharCripto1[12] = CryptoTranspositionType.processAlphabet(alphabetCompoundTempCharCripto1[12], transKeysCripto9, transOptionCriptoA5,
                transOptionCriptoB5, transOptionCriptoC5);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[12]);
        alphabet = alphabetCompundFinalCharCripto1;
        String numericKeyWeftCripto2 = "435,647,54,67,1,265,980,213,445,748,324,653,875,923,126";
        CryptoNumbersSort weftOptionCriptoA3 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "213"), null);
        CryptoNumbersSort weftOptionCriptoA4 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "748"),
                null, CryptoNumbersSort.TypeNumber.EVEN);
        alphabet = CryptoWeftType.processAlphabet(alphabet, numericKeyWeftCripto2, weftOptionCriptoA3, weftOptionCriptoA4);
        String firstNormalDoubleKey = "324,56,12,435,78,89,590,432,657,43,256,467,84", secondNormalDoubleKey = "21,425,65,34,128,76,890,546,32,54,67,43,18,438";
        CryptoNumbersSort firstNormalOptionCripto1 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null);
        CryptoNumbersSort firstNormalOptionCripto2 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "435"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "590"), CryptoNumbersSort.TypeNumber.ODD);
        CryptoNumbersSort firstNormalOptionCripto3 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "546"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "43"), CryptoNumbersSort.TypeNumber.EVEN);
        alphabet = CryptoNormalDoubleApp.processAlphabet(alphabet, firstNormalDoubleKey, firstNormalOptionCripto2,
                firstNormalOptionCripto1, secondNormalDoubleKey, firstNormalOptionCripto3, firstNormalOptionCripto1);
        alphabet = CryptoAlphabetUtils.convertAlphabetToInverse(alphabet);
        String firstGradualDoubleKey = "345,123,765,872,980,435,647,135,654,436,769", secondGradualDoubleKey = "325,127,658,980,437,652,436,674,326,469,547";
        CryptoNumbersSort firstGradualOptionCripto1 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null);
        CryptoNumbersSort firstGradualOptionCripto2 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "425"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "78"), CryptoNumbersSort.TypeNumber.ODD);
        CryptoNumbersSort firstGradualOptionCripto3 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "438"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "67"), CryptoNumbersSort.TypeNumber.EVEN);
        alphabet = CryptoGradualDoubleApp.processAlphabet(alphabet, firstGradualDoubleKey, firstGradualOptionCripto2,
                firstGradualOptionCripto1, secondGradualDoubleKey, firstGradualOptionCripto3, firstGradualOptionCripto1);
        String initialCharacter2 = "*";
        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabet, initialCharacter2);
        alphabet = CryptoAlphabetUtils.convertAlphabetToInverse(alphabet);
        char alphabetFlat[];
        String numericKeyFlat = "546,768,154,975,452,764,825,143,65", initialCharacterFlat = "8";
        int loopsFlat = 999;
        CryptoConstants.TypeArranged typeArrangedFlat = CryptoConstants.TypeArranged.REVERSE;
        alphabetFlat = CryptoAlphabetUtils.generateAlphabetByParameters(typeArrangedFlat, loopsFlat, null);
        alphabetFlat = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabetFlat, initialCharacterFlat);
        String initialCharacterSucBouncesFlat1 = "?", numericKeySucBouncesPrimesFlat1 = "57,3,10,13,42,100,43,65,23,45,32,55", directionKeySucBouncesFlat1 = "IIDDIIIDIDIDD",
                numerciKeysSucBouncesFlat1 = CryptoNumbersUtils.buildKeysByArrayNumbers(CryptoNumbersUtils.findPrimeNumbersByKeyPositions(numericKeySucBouncesPrimesFlat1));
        int[] arrNumericKeySucBouncesFlat1 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetFlat, numerciKeysSucBouncesFlat1);
        char arrDirectionKeySucBouncesFlat1[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetFlat, directionKeySucBouncesFlat1);
        boolean arrFlagsSucBouncesFlat1[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetFlat);
        alphabetFlat = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabetFlat, initialCharacterSucBouncesFlat1);
        alphabetFlat = CryptoDirSucBouncesType.processAlphabet(alphabetFlat, arrNumericKeySucBouncesFlat1, arrDirectionKeySucBouncesFlat1, arrFlagsSucBouncesFlat1, true);
        String transKeysFlat1 = null;
        if (transKeysFlat1 == null) {
            transKeysFlat1 = numericKeyFlat;
        }
        CryptoNumbersSort transOptionFlatA1 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "150"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "45"), CryptoNumbersSort.TypeNumber.EVEN);
        CryptoNumbersSort transOptionFlatB1 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "99"),
                null, CryptoNumbersSort.TypeNumber.ODD);
        CryptoNumbersSort transOptionFlatC1 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "400"), null);
        alphabetFlat = CryptoTranspositionType.processAlphabet(alphabetFlat, transKeysFlat1, transOptionFlatA1, transOptionFlatB1, transOptionFlatC1);
        String initialCharacter2Flat = "+";
        alphabetFlat = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabetFlat, initialCharacter2Flat);
        alphabetFlat = CryptoAlphabetUtils.convertAlphabetToInverse(alphabetFlat);
        CryptoAlphabetToAction.defaultCripto = String.valueOf(alphabet);
        CryptoAlphabetToAction.defaultFlat = String.valueOf(alphabetFlat);
        CryptoAlphabetToAction.customCripto = String.valueOf(alphabet);
        CryptoAlphabetToAction.customFlat = String.valueOf(alphabetFlat);
        /*System.out.println(CryptoAlphabetToAction.customCripto);
        System.out.println(CryptoAlphabetToAction.customFlat);*/
    }
    /*public static void main(String[] args) throws CryptoGlobalException {
        configAlphabet();
    }*/
}
