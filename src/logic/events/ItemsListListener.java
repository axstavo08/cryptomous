/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.events;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import logic.entity.CryptoConstants;
import views.CustomizeView;

/**
 *
 * @author Gustavo Ramos M.
 */
public class ItemsListListener extends MouseAdapter {

    private JList list;
    private int pressIndex = 0, releaseIndex = 0;
    private final Object oView;
    private final CryptoConstants.TypeCrypto type;

    public ItemsListListener(JList list, Object view, CryptoConstants.TypeCrypto type) {
        if (!(list.getModel() instanceof DefaultListModel)) {
            throw new IllegalArgumentException("List must have a DefaultListModel");
        }
        this.list = list;
        this.oView = view;
        this.type = type;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        pressIndex = list.locationToIndex(e.getPoint());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        releaseIndex = list.locationToIndex(e.getPoint());
        if (releaseIndex != pressIndex && releaseIndex != -1) {
            reorder();
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseReleased(e);
        pressIndex = releaseIndex;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        JList theList = (JList) mouseEvent.getSource();
        if (mouseEvent.getClickCount() == 1) {
            if (oView instanceof CustomizeView) {
                CustomizeView view = (CustomizeView) oView;
                view.enableRemoveOptionItemMethod(type);
            }
        } else if (mouseEvent.getClickCount() == 2) {
            int index = theList.locationToIndex(mouseEvent.getPoint());
            if (index >= 0) {
                String item = theList.getModel().getElementAt(index).toString();
                if (oView instanceof CustomizeView) {
                    CustomizeView view = (CustomizeView) oView;
                    view.showMethodConfiguration(type, item);
                }
            }
        }
    }

    private void reorder() {
        DefaultListModel model = (DefaultListModel) list.getModel();
        Object dragee = model.elementAt(pressIndex);
        model.removeElementAt(pressIndex);
        model.insertElementAt(dragee, releaseIndex);
    }
}
