/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.events;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.TransferHandler;

/**
 *
 * @author Gustavo Ramos M.
 */
public class ExportTransferHandler extends TransferHandler {

    private final JList jList;

    public ExportTransferHandler(JList jList) {
        this.jList = jList;
    }

    @Override
    public int getSourceActions(JComponent c) {
        return TransferHandler.COPY_OR_MOVE;
    }

    @Override
    public Transferable createTransferable(JComponent c) {
        return new StringSelection(jList.getSelectedValue().toString());
    }
}
