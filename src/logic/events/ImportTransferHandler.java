/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.events;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import javax.swing.TransferHandler;
import logic.entity.CryptoConstants;
import views.CustomizeView;

/**
 *
 * @author Gustavo Ramos M.
 */
public class ImportTransferHandler extends TransferHandler {

    private final Object oView;
    private final CryptoConstants.TypeCrypto type;
    
    public ImportTransferHandler(Object view, CryptoConstants.TypeCrypto type) {
        this.oView = view;
        this.type = type;
    }

    @Override
    public boolean canImport(TransferHandler.TransferSupport supp) {
        return supp.isDataFlavorSupported(DataFlavor.stringFlavor);
    }

    @Override
    public boolean importData(TransferHandler.TransferSupport supp) {
        Transferable t = supp.getTransferable();
        String data;
        try {
            data = (String) t.getTransferData(DataFlavor.stringFlavor);
            if (oView instanceof CustomizeView) {
                CustomizeView view = (CustomizeView) oView;
                view.addItemToJListByType(type, data);
            }
        } catch (UnsupportedFlavorException | IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
}
