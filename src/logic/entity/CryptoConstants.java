/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoConstants {

    public static enum TypeArranged {
        ARRANGED, REVERSE, RANDOM;
    }

    public static enum FromDirection {
        START, END;
    }

    public static enum TypeGenerationNumericKey {
        RANDOM, POSITION_PRIME_NUMBERS;
    }

    public static enum TypeCrypto {
        CRIPTO, FLAT;
    }

    public static enum WeftRestrictionNumber {
        ASSIGN, SORT;
    }

    public static enum TranspositionRestrictionNumber {
        FIRST, SECOND, THIRD, FOURTH, FIFTH, SIXTH;
    }

    public static enum NormalDoubleAppRestrictionNumber {
        ORDERING_FIRST_APP, EXTRACTING_FIRST_APP, ORDERING_SECOND_APP, EXTRACTING_SECOND_APP;
    }

    public static enum DoubleApplicationType {
        FIRST_APPLCATION, SECOND_APPLICATION;
    }
    
    public static enum GradualDoubleAppRestrictionNumber {
        ORDERING_FIRST_APP, EXTRACTING_FIRST_APP, ORDERING_SECOND_APP, EXTRACTING_SECOND_APP;
    }
    
    public static enum Methods {
        CHARACTERS_VARIATION("Variación de Caracteres", "chars_var"),
        SUCCESSIVE_BOUNCES("Saltos Sucesivos", "suc_bounces"),
        TRANSPOSITION("Transposición", "transposition"),
        WEFT("Tramas", "weft"),
        CHARACTERS_DIVISION("División de Caracteres", "chars_div"),
        NORMAL_DOUBLE_APP("Doble Aplicación Normal", "normal_doub_app"),
        GRADUAL_DOUBLE_APP("Doble Aplicación Gradual", "gradual_doub_app");

        private final String label;
        private final String key;

        Methods(String label, String key) {
            this.label = label;
            this.key = key;
        }

        public String getLabel() {
            return label;
        }

        public String getKey() {
            return key;
        }

        public static Methods findMethodByLabel(String label) {
            Methods obj = null;
            for (Methods method : Methods.values()) {
                if (method.getLabel().equals(label)) {
                    obj = method;
                    break;
                }
            }
            return obj;
        }

        public static Methods findMethodByCoincidenceLabel(String label) {
            Methods obj = null;
            for (Methods method : Methods.values()) {
                if (method.getLabel().contains(label)) {
                    obj = method;
                    break;
                }
            }
            return obj;
        }

        public static Methods findMethodByCoincidenceKey(String key) {
            Methods obj = null;
            for (Methods method : Methods.values()) {
                if (key.contains(method.getKey())) {
                    obj = method;
                    break;
                }
            }
            return obj;
        }
    }
}
