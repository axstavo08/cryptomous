/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoCharactersVariationConfiguration {

    private CryptoConstants.TypeArranged ordering;
    private int loops;
    private String initialCharacter;
    private String phraseKey;
    private boolean useGlobalPhraseKey;
    private boolean isConfigured;

    public CryptoCharactersVariationConfiguration() {
        this.ordering = CryptoConstants.TypeArranged.ARRANGED;
        this.useGlobalPhraseKey = false;
        this.isConfigured = false;
    }

    public CryptoCharactersVariationConfiguration(CryptoConstants.TypeArranged ordering, int loops, String initialCharacter, String phraseKey, boolean useGlobalPhraseKey) {
        this.ordering = ordering;
        this.loops = loops;
        this.initialCharacter = initialCharacter;
        this.phraseKey = phraseKey;
        this.useGlobalPhraseKey = useGlobalPhraseKey;
        this.isConfigured = true;
    }

    public CryptoConstants.TypeArranged getOrdering() {
        return ordering;
    }

    public void setOrdering(CryptoConstants.TypeArranged ordering) {
        this.ordering = ordering;
    }

    public int getLoops() {
        return loops;
    }

    public void setLoops(int loops) {
        this.loops = loops;
    }

    public String getInitialCharacter() {
        return initialCharacter;
    }

    public void setInitialCharacter(String initialCharacter) {
        this.initialCharacter = initialCharacter;
    }

    public String getPhraseKey() {
        return phraseKey;
    }

    public void setPhraseKey(String phraseKey) {
        this.phraseKey = phraseKey;
    }

    public boolean isUseGlobalPhraseKey() {
        return useGlobalPhraseKey;
    }

    public void setUseGlobalPhraseKey(boolean useGlobalPhraseKey) {
        this.useGlobalPhraseKey = useGlobalPhraseKey;
    }

    public boolean isIsConfigured() {
        return isConfigured;
    }

    public void setIsConfigured(boolean isConfigured) {
        this.isConfigured = isConfigured;
    }
}
