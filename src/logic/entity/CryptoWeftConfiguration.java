/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoWeftConfiguration {

    private String numericKey;
    private CryptoNumbersSort assignRestriction;
    private CryptoNumbersSort sortRestriction;
    private boolean isConfigured;
    private boolean useGlobalNumericKey;

    public CryptoWeftConfiguration() {
        /*this.assignRestriction = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null);
        this.sortRestriction = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null);*/
        this.assignRestriction = new CryptoNumbersSort();
        this.sortRestriction = new CryptoNumbersSort();
        this.isConfigured = false;
    }

    public CryptoWeftConfiguration(String numericKey, CryptoNumbersSort assignRestriction, CryptoNumbersSort sortRestriction, boolean useGlobalNumericKey) {
        this.numericKey = numericKey;
        this.assignRestriction = assignRestriction;
        this.sortRestriction = sortRestriction;
        this.useGlobalNumericKey = useGlobalNumericKey;
    }

    public String getNumericKey() {
        return numericKey;
    }

    public void setNumericKey(String numericKey) {
        this.numericKey = numericKey;
    }

    public CryptoNumbersSort getAssignRestriction() {
        return assignRestriction;
    }

    public void setAssignRestriction(CryptoNumbersSort assignRestriction) {
        this.assignRestriction = assignRestriction;
    }

    public CryptoNumbersSort getSortRestriction() {
        return sortRestriction;
    }

    public void setSortRestriction(CryptoNumbersSort sortRestriction) {
        this.sortRestriction = sortRestriction;
    }

    public boolean isIsConfigured() {
        return isConfigured;
    }

    public void setIsConfigured(boolean isConfigured) {
        this.isConfigured = isConfigured;
    }

    public boolean isUseGlobalNumericKey() {
        return useGlobalNumericKey;
    }

    public void setUseGlobalNumericKey(boolean useGlobalNumericKey) {
        this.useGlobalNumericKey = useGlobalNumericKey;
    }
}
