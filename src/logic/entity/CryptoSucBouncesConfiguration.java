/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoSucBouncesConfiguration {

    private String numericKey;
    private String directionKey;
    private String initialCharacter;
    private boolean initial;
    private boolean useGlobalNumericKey;

    public CryptoSucBouncesConfiguration() {
        this.initial = false;
        this.useGlobalNumericKey = false;
    }

    public CryptoSucBouncesConfiguration(String numericKey, String directionKey, String initialCharacter, boolean initial, boolean useGlobalNumericKey) {
        this.numericKey = numericKey;
        this.directionKey = directionKey;
        this.initialCharacter = initialCharacter;
        this.initial = initial;
        this.useGlobalNumericKey = useGlobalNumericKey;
    }

    public String getNumericKey() {
        return numericKey;
    }

    public void setNumericKey(String numericKey) {
        this.numericKey = numericKey;
    }

    public String getDirectionKey() {
        return directionKey;
    }

    public void setDirectionKey(String directionKey) {
        this.directionKey = directionKey;
    }

    public String getInitialCharacter() {
        return initialCharacter;
    }

    public void setInitialCharacter(String initialCharacter) {
        this.initialCharacter = initialCharacter;
    }

    public boolean isInitial() {
        return initial;
    }

    public void setInitial(boolean initial) {
        this.initial = initial;
    }

    public boolean isUseGlobalNumericKey() {
        return useGlobalNumericKey;
    }

    public void setUseGlobalNumericKey(boolean useGlobalNumericKey) {
        this.useGlobalNumericKey = useGlobalNumericKey;
    }
}
