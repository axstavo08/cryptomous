/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoAlphabetConfiguration {

    private CryptoConstants.TypeArranged ordering;
    private int loops;
    private String initialCharacter;
    private boolean usePhraseKey;
    private boolean isConfigured;

    public CryptoAlphabetConfiguration() {
        this.ordering = CryptoConstants.TypeArranged.ARRANGED;
        this.usePhraseKey = true;
        this.isConfigured = false;
    }

    public CryptoConstants.TypeArranged getOrdering() {
        return ordering;
    }

    public void setOrdering(CryptoConstants.TypeArranged ordering) {
        this.ordering = ordering;
    }

    public int getLoops() {
        return loops;
    }

    public void setLoops(int loops) {
        this.loops = loops;
    }

    public String getInitialCharacter() {
        return initialCharacter;
    }

    public void setInitialCharacter(String initialCharacter) {
        this.initialCharacter = initialCharacter;
    }

    public boolean isUsePhraseKey() {
        return usePhraseKey;
    }

    public void setUsePhraseKey(boolean usePhraseKey) {
        this.usePhraseKey = usePhraseKey;
    }

    public boolean isIsConfigured() {
        return isConfigured;
    }

    public void setIsConfigured(boolean isConfigured) {
        this.isConfigured = isConfigured;
    }
}
