/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoTypeConfiguration {

    private CryptoAlphabetConfiguration charactersConfiguration;
    private Map<String, Object> charactersMethods;
    private int charsVarCounter;
    private int sucBouncesCounter;
    private int transpositionCounter;
    private int weftCounter;
    private int charsDivCounter;
    private int normalDoubAppCounter;
    private int gradualDoubAppCounter;

    public CryptoTypeConfiguration() {
        this.charactersConfiguration = new CryptoAlphabetConfiguration();
        this.charactersMethods = new LinkedHashMap<>();
    }

    public CryptoTypeConfiguration(CryptoAlphabetConfiguration charactersConfiguration, Map<String, Object> charactersMethods) {
        this.charactersConfiguration = charactersConfiguration;
        this.charactersMethods = charactersMethods;
    }

    public int getMethodCounter(CryptoConstants.Methods method) {
        int counter = 0;
        switch (method) {
            case CHARACTERS_VARIATION:
                charsVarCounter++;
                counter = charsVarCounter;
                break;
            case SUCCESSIVE_BOUNCES:
                sucBouncesCounter++;
                counter = sucBouncesCounter;
                break;
            case TRANSPOSITION:
                transpositionCounter++;
                counter = transpositionCounter;
                break;
            case WEFT:
                weftCounter++;
                counter = weftCounter;
                break;
            case CHARACTERS_DIVISION:
                charsDivCounter++;
                counter = charsDivCounter;
                break;
            case NORMAL_DOUBLE_APP:
                normalDoubAppCounter++;
                counter = normalDoubAppCounter;
                break;
            case GRADUAL_DOUBLE_APP:
                gradualDoubAppCounter++;
                counter = gradualDoubAppCounter;
                break;
        }
        return counter;
    }

    public CryptoAlphabetConfiguration getCharactersConfiguration() {
        return charactersConfiguration;
    }

    public void setCharactersConfiguration(CryptoAlphabetConfiguration charactersConfiguration) {
        this.charactersConfiguration = charactersConfiguration;
    }

    public Map<String, Object> getCharactersMethods() {
        return charactersMethods;
    }

    public void setCharactersMethods(Map<String, Object> charactersMethods) {
        this.charactersMethods = charactersMethods;
    }
}
