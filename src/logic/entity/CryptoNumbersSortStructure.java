/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoNumbersSortStructure {

    private DirectionSort direction;
    private String initial;

    public static enum DirectionSort {
        ASCENDANT, DESCENDANT;
    }

    public CryptoNumbersSortStructure(DirectionSort direction, String initial) {
        this.direction = (direction == null) ? CryptoNumbersSortStructure.DirectionSort.ASCENDANT : direction;
        this.initial = initial;
    }

    public DirectionSort getDirection() {
        return direction;
    }

    public void setDirection(DirectionSort direction) {
        this.direction = direction;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }
}
