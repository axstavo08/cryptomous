/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoNumbersSort {

    private TypeSort type;
    private CryptoNumbersSortStructure firstStructure;
    private CryptoNumbersSortStructure secondStructure;
    private TypeNumber typeNumber;
    private boolean isConfigured;

    public static enum TypeSort {
        NORMAL, COMPOUND;
    }

    public static enum TypeNumber {
        EVEN, ODD;
    }

    public static enum TypeArranged {
        ARRANGED, INVERSE, RANDOM;
    }

    public CryptoNumbersSort() {
        this.isConfigured = false;
    }

    public CryptoNumbersSort(TypeSort type, CryptoNumbersSortStructure firstStructure, TypeNumber typeNumber) {
        this.type = (type == null) ? CryptoNumbersSort.TypeSort.NORMAL : type;
        this.firstStructure = firstStructure;
        if (this.type == CryptoNumbersSort.TypeSort.NORMAL) {
            this.typeNumber = typeNumber;
        } else {
            this.typeNumber = (typeNumber == null) ? CryptoNumbersSort.TypeNumber.EVEN : typeNumber;
        }
        this.isConfigured = true;
    }

    public CryptoNumbersSort(CryptoNumbersSortStructure firstStructure, CryptoNumbersSortStructure secondStructure, TypeNumber typeNumber) {
        this.type = CryptoNumbersSort.TypeSort.COMPOUND;
        this.firstStructure = firstStructure;
        this.secondStructure = secondStructure;
        this.typeNumber = (typeNumber == null) ? CryptoNumbersSort.TypeNumber.EVEN : typeNumber;
        this.isConfigured = true;
    }

    public TypeSort getType() {
        return type;
    }

    public void setType(TypeSort type) {
        this.type = type;
    }

    public CryptoNumbersSortStructure getFirstStructure() {
        return firstStructure;
    }

    public void setFirstStructure(CryptoNumbersSortStructure firstStructure) {
        this.firstStructure = firstStructure;
    }

    public CryptoNumbersSortStructure getSecondStructure() {
        return secondStructure;
    }

    public void setSecondStructure(CryptoNumbersSortStructure secondStructure) {
        this.secondStructure = secondStructure;
    }

    public TypeNumber getTypeNumber() {
        return typeNumber;
    }

    public void setTypeNumber(TypeNumber typeNumber) {
        this.typeNumber = typeNumber;
    }

    public boolean isIsConfigured() {
        return isConfigured;
    }

    public void setIsConfigured(boolean isConfigured) {
        this.isConfigured = isConfigured;
    }
}
