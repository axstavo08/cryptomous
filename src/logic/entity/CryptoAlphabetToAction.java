/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoAlphabetToAction {

    public static String defaultCripto;
    public static String defaultFlat;
    public static String customCripto;
    public static String customFlat;
}
