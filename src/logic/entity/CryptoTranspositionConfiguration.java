/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoTranspositionConfiguration {

    private String numericKey;
    private CryptoNumbersSort firstRestriction;
    private CryptoNumbersSort secondRestriction;
    private CryptoNumbersSort thirdRestriction;
    private CryptoNumbersSort fourthRestriction;
    private CryptoNumbersSort fifthRestriction;
    private CryptoNumbersSort sixthRestriction;
    private boolean isConfigured;
    private boolean useGlobalNumericKey;

    public CryptoTranspositionConfiguration() {
        this.firstRestriction = new CryptoNumbersSort();
        this.secondRestriction = new CryptoNumbersSort();
        this.thirdRestriction = new CryptoNumbersSort();
        this.fourthRestriction = new CryptoNumbersSort();
        this.fifthRestriction = new CryptoNumbersSort();
        this.sixthRestriction = new CryptoNumbersSort();
        this.isConfigured = false;
    }

    public CryptoTranspositionConfiguration(String numericKey, CryptoNumbersSort firstRestriction, CryptoNumbersSort secondRestriction,
            CryptoNumbersSort thirdRestriction, CryptoNumbersSort fourthRestriction, CryptoNumbersSort fifthRestriction, CryptoNumbersSort sixthRestriction,
            boolean useGlobalNumericKey) {
        this.numericKey = numericKey;
        this.firstRestriction = firstRestriction;
        this.secondRestriction = secondRestriction;
        this.thirdRestriction = thirdRestriction;
        this.fourthRestriction = fourthRestriction;
        this.fifthRestriction = fifthRestriction;
        this.sixthRestriction = sixthRestriction;
        this.useGlobalNumericKey = useGlobalNumericKey;
    }

    public String getNumericKey() {
        return numericKey;
    }

    public void setNumericKey(String numericKey) {
        this.numericKey = numericKey;
    }

    public CryptoNumbersSort getFirstRestriction() {
        return firstRestriction;
    }

    public void setFirstRestriction(CryptoNumbersSort firstRestriction) {
        this.firstRestriction = firstRestriction;
    }

    public CryptoNumbersSort getSecondRestriction() {
        return secondRestriction;
    }

    public void setSecondRestriction(CryptoNumbersSort secondRestriction) {
        this.secondRestriction = secondRestriction;
    }

    public CryptoNumbersSort getThirdRestriction() {
        return thirdRestriction;
    }

    public void setThirdRestriction(CryptoNumbersSort thirdRestriction) {
        this.thirdRestriction = thirdRestriction;
    }

    public CryptoNumbersSort getFourthRestriction() {
        return fourthRestriction;
    }

    public void setFourthRestriction(CryptoNumbersSort fourthRestriction) {
        this.fourthRestriction = fourthRestriction;
    }

    public CryptoNumbersSort getFifthRestriction() {
        return fifthRestriction;
    }

    public void setFifthRestriction(CryptoNumbersSort fifthRestriction) {
        this.fifthRestriction = fifthRestriction;
    }

    public CryptoNumbersSort getSixthRestriction() {
        return sixthRestriction;
    }

    public void setSixthRestriction(CryptoNumbersSort sixthRestriction) {
        this.sixthRestriction = sixthRestriction;
    }

    public boolean isIsConfigured() {
        return isConfigured;
    }

    public void setIsConfigured(boolean isConfigured) {
        this.isConfigured = isConfigured;
    }

    public boolean isUseGlobalNumericKey() {
        return useGlobalNumericKey;
    }

    public void setUseGlobalNumericKey(boolean useGlobalNumericKey) {
        this.useGlobalNumericKey = useGlobalNumericKey;
    }
}
