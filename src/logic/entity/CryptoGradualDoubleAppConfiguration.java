/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoGradualDoubleAppConfiguration {

    private String numericKeyFirstApp;
    private CryptoNumbersSort orderingFirstAppRestriction;
    private CryptoNumbersSort extractingFirstAppRestriction;
    private boolean useGlobalNumericKeyFirstApp;
    private String numericKeySecondApp;
    private CryptoNumbersSort orderingSecondAppRestriction;
    private CryptoNumbersSort extractingSecondAppRestriction;
    private boolean useGlobalNumericKeySecondApp;
    private boolean isConfigured;

    public CryptoGradualDoubleAppConfiguration() {
        this.orderingFirstAppRestriction = new CryptoNumbersSort();
        this.extractingFirstAppRestriction = new CryptoNumbersSort();
        this.orderingSecondAppRestriction = new CryptoNumbersSort();
        this.extractingSecondAppRestriction = new CryptoNumbersSort();
        this.isConfigured = false;
    }

    public CryptoGradualDoubleAppConfiguration(String numericKeyFirstApp, CryptoNumbersSort orderingFirstAppRestriction, CryptoNumbersSort extractingFirstAppRestriction,
            boolean useGlobalNumericKeyFirstApp, String numericKeySecondApp, CryptoNumbersSort orderingSecondAppRestriction, CryptoNumbersSort extractingSecondAppRestriction,
            boolean useGlobalNumericKeySecondApp) {
        this.numericKeyFirstApp = numericKeyFirstApp;
        this.orderingFirstAppRestriction = orderingFirstAppRestriction;
        this.extractingFirstAppRestriction = extractingFirstAppRestriction;
        this.useGlobalNumericKeyFirstApp = useGlobalNumericKeyFirstApp;
        this.numericKeySecondApp = numericKeySecondApp;
        this.orderingSecondAppRestriction = orderingSecondAppRestriction;
        this.extractingSecondAppRestriction = extractingSecondAppRestriction;
        this.useGlobalNumericKeySecondApp = useGlobalNumericKeySecondApp;
    }

    public String getNumericKeyFirstApp() {
        return numericKeyFirstApp;
    }

    public void setNumericKeyFirstApp(String numericKeyFirstApp) {
        this.numericKeyFirstApp = numericKeyFirstApp;
    }

    public CryptoNumbersSort getOrderingFirstAppRestriction() {
        return orderingFirstAppRestriction;
    }

    public void setOrderingFirstAppRestriction(CryptoNumbersSort orderingFirstAppRestriction) {
        this.orderingFirstAppRestriction = orderingFirstAppRestriction;
    }

    public CryptoNumbersSort getExtractingFirstAppRestriction() {
        return extractingFirstAppRestriction;
    }

    public void setExtractingFirstAppRestriction(CryptoNumbersSort extractingFirstAppRestriction) {
        this.extractingFirstAppRestriction = extractingFirstAppRestriction;
    }

    public boolean isUseGlobalNumericKeyFirstApp() {
        return useGlobalNumericKeyFirstApp;
    }

    public void setUseGlobalNumericKeyFirstApp(boolean useGlobalNumericKeyFirstApp) {
        this.useGlobalNumericKeyFirstApp = useGlobalNumericKeyFirstApp;
    }

    public String getNumericKeySecondApp() {
        return numericKeySecondApp;
    }

    public void setNumericKeySecondApp(String numericKeySecondApp) {
        this.numericKeySecondApp = numericKeySecondApp;
    }

    public CryptoNumbersSort getOrderingSecondAppRestriction() {
        return orderingSecondAppRestriction;
    }

    public void setOrderingSecondAppRestriction(CryptoNumbersSort orderingSecondAppRestriction) {
        this.orderingSecondAppRestriction = orderingSecondAppRestriction;
    }

    public CryptoNumbersSort getExtractingSecondAppRestriction() {
        return extractingSecondAppRestriction;
    }

    public void setExtractingSecondAppRestriction(CryptoNumbersSort extractingSecondAppRestriction) {
        this.extractingSecondAppRestriction = extractingSecondAppRestriction;
    }

    public boolean isUseGlobalNumericKeySecondApp() {
        return useGlobalNumericKeySecondApp;
    }

    public void setUseGlobalNumericKeySecondApp(boolean useGlobalNumericKeySecondApp) {
        this.useGlobalNumericKeySecondApp = useGlobalNumericKeySecondApp;
    }

    public boolean isIsConfigured() {
        return isConfigured;
    }

    public void setIsConfigured(boolean isConfigured) {
        this.isConfigured = isConfigured;
    }
}
