/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.entity;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoGlobalVariablesConfiguration {

    private String numericKey;
    private String phraseKey;
    private CryptoConstants.FromDirection directionPhraseKey;

    public CryptoGlobalVariablesConfiguration() {
    }

    public String getNumericKey() {
        return numericKey;
    }

    public void setNumericKey(String numericKey) {
        this.numericKey = numericKey;
    }

    public String getPhraseKey() {
        return phraseKey;
    }

    public void setPhraseKey(String phraseKey) {
        this.phraseKey = phraseKey;
    }

    public CryptoConstants.FromDirection getDirectionPhraseKey() {
        return directionPhraseKey;
    }

    public void setDirectionPhraseKey(CryptoConstants.FromDirection directionPhraseKey) {
        this.directionPhraseKey = directionPhraseKey;
    }
}
