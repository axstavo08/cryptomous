/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.methods;

import java.util.Arrays;
import logic.entity.CryptoNumbersSort;
import logic.exception.CryptoGlobalException;
import logic.utils.CryptoOtherUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoNormalDoubleApp {

    public static char[] processAlphabet(char alphabet[], String firstKeysApp, CryptoNumbersSort firstStructureApp,
            CryptoNumbersSort secondStructureExtract, String secondKeysApp, CryptoNumbersSort secondStructureApp,
            CryptoNumbersSort resultStructureExtract) throws CryptoGlobalException {
        char alphabetNormalDoubleApp[] = {};
        String[] firstTimesKeys = firstKeysApp.split(","), secondTimesKeys = secondKeysApp.split(",");
        int firstQuantityKeys = firstTimesKeys.length, secondQuantityKeys = secondTimesKeys.length, mainCounter = 0, rowAppCounter = 0, characterRowCounter = 0;
        Character[][] firstApplication = {}, secondApplication = {};
        Character alphabetObj[] = ArrayUtils.toObject(alphabet), firstRowApp[] = new Character[firstQuantityKeys], secondRowApp[] = new Character[secondQuantityKeys];
        Character characterHelper;
        int[] firstNumKeys = CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(firstTimesKeys, firstStructureApp),
                firstExtractKeys = CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(firstTimesKeys, secondStructureExtract),
                secondNumKeys = CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(secondTimesKeys, secondStructureApp),
                secondExtractKeys = CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(secondTimesKeys, resultStructureExtract);
        for (Character character : alphabetObj) {
            firstRowApp[ArrayUtils.indexOf(firstTimesKeys, String.valueOf(firstNumKeys[rowAppCounter]))] = character;
            rowAppCounter++;
            if (rowAppCounter == firstQuantityKeys || mainCounter == alphabet.length - 1) {
                firstApplication = ArrayUtils.add(firstApplication, firstRowApp);
                firstRowApp = new Character[firstQuantityKeys];
                rowAppCounter = 0;
            }
            mainCounter++;
        }
        /*for (Character[] firstApplication1 : firstApplication) {
            for (Character firstApplication11 : firstApplication1) {
                System.out.print(firstApplication11 + " ");
            }
            System.out.println();
        }*/
        mainCounter = 0;
        rowAppCounter = 0;
        do {
            for (Character[] rowsApp : firstApplication) {
                characterRowCounter++;
                characterHelper = rowsApp[ArrayUtils.indexOf(firstTimesKeys, String.valueOf(firstExtractKeys[mainCounter]))];
                if (characterHelper != null) {
                    secondRowApp[ArrayUtils.indexOf(secondTimesKeys, String.valueOf(secondNumKeys[rowAppCounter]))] = characterHelper;
                    rowAppCounter++;
                }
                if (rowAppCounter == secondQuantityKeys || (mainCounter == firstQuantityKeys - 1 && characterRowCounter == firstApplication.length)) {
                    secondApplication = ArrayUtils.add(secondApplication, secondRowApp);
                    secondRowApp = new Character[secondQuantityKeys];
                    rowAppCounter = 0;
                }
            }
            characterRowCounter = 0;
            mainCounter++;
        } while (mainCounter < firstQuantityKeys);
        /*for (Character[] firstApplication1 : secondApplication) {
            for (Character firstApplication11 : firstApplication1) {
                System.out.print(firstApplication11 + " ");
            }
            System.out.println();
        }*/
        mainCounter = 0;
        do {
            for (Character[] rowsApp : secondApplication) {
                characterHelper = rowsApp[ArrayUtils.indexOf(secondTimesKeys, String.valueOf(secondExtractKeys[mainCounter]))];
                if (characterHelper != null) {
                    alphabetNormalDoubleApp = ArrayUtils.add(alphabetNormalDoubleApp, characterHelper);
                }
            }
            mainCounter++;
        } while (mainCounter < secondQuantityKeys);
        return alphabetNormalDoubleApp;
    }
}
