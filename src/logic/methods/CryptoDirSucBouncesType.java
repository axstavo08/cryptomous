/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.methods;

import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoDirSucBouncesType {

    private static final char RIGHT_DIRECTION = 'D';

    public static char[] processAlphabet(char alphabet[], int[] keysDir, char bounceDir[], boolean flagDir[], boolean initial) {
        char alphabetDirectionMethod[] = {};
        int totalIndex = alphabet.length - 1;
        int counterDir = 0, alphabetIndex = 0, alphabetCounter = 0, bounceCounter, currentKey;
        char currentDir, finalChar;
        do {
            bounceCounter = 0;
            currentKey = keysDir[counterDir];
            currentDir = bounceDir[counterDir];
            if (!initial) {
                if (currentDir == RIGHT_DIRECTION) {
                    alphabetIndex = processAlphabetForRightDirection(totalIndex, alphabetIndex, alphabetCounter, flagDir, bounceCounter, currentKey);
                } else {
                    alphabetIndex = processAlphabetForLeftDirection(totalIndex, alphabetIndex, alphabetCounter, flagDir, bounceCounter, currentKey);
                }
            } else {
                initial = false;
            }
            finalChar = alphabet[alphabetIndex];
            flagDir[alphabetIndex] = false;
            alphabetDirectionMethod = ArrayUtils.add(alphabetDirectionMethod, finalChar);
            counterDir++;
        } while (counterDir <= totalIndex);
        return alphabetDirectionMethod;
    }

    public static int processAlphabetForRightDirection(int totalIndex, int alphabetIndex, int alphabetCounter, boolean[] flagDir, int bounceCounter,
            int currentKey) {
        boolean currentValid;
        if (alphabetIndex == totalIndex) {
            alphabetIndex = 0;
        } else {
            alphabetCounter = alphabetIndex + 1;
        }
        do {
            currentValid = flagDir[alphabetCounter];
            if (currentValid) {
                bounceCounter++;
            }
            alphabetCounter++;
            if (alphabetCounter > totalIndex) {
                alphabetCounter = 0;
            }
        } while (bounceCounter < currentKey);
        if (alphabetCounter == 0) {
            alphabetIndex = totalIndex;
        } else {
            alphabetIndex = alphabetCounter - 1;
        }
        return alphabetIndex;
    }

    public static int processAlphabetForLeftDirection(int totalIndex, int alphabetIndex, int alphabetCounter, boolean[] flagDir, int bounceCounter,
            int currentKey) {
        boolean currentValid;
        if (alphabetIndex == 0) {
            alphabetIndex = totalIndex;
            alphabetCounter = totalIndex;
        } else {
            alphabetCounter = alphabetIndex - 1;
        }
        do {
            currentValid = flagDir[alphabetCounter];
            if (currentValid) {
                bounceCounter++;
            }
            alphabetCounter--;
            if (alphabetCounter < 0) {
                alphabetCounter = totalIndex;
            }
        } while (bounceCounter < currentKey);
        if (alphabetCounter == totalIndex) {
            alphabetIndex = 0;
        } else {
            alphabetIndex = alphabetCounter + 1;
        }
        return alphabetIndex;
    }
}
