/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.methods;

import logic.entity.CryptoNumbersSort;
import logic.exception.CryptoGlobalException;
import logic.utils.CryptoOtherUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoWeftType {

    public static char[] processAlphabet(char alphabet[], String keys, CryptoNumbersSort assignStructure, CryptoNumbersSort sortStructure) throws CryptoGlobalException {
        char alphabetWeftMethod[] = {}, alphabetTransSegment[], alphabetWeftHelper[][] = {};
        String[] multiTimesKeys = keys.split(",");
        int[] numKeysAssign = CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(multiTimesKeys, assignStructure),
                numKeysSort = CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(multiTimesKeys, sortStructure);
        int quantityAlphabet = alphabet.length, quantityTransKeys = multiTimesKeys.length, multiTimes = quantityAlphabet / quantityTransKeys,
                restTimes = quantityAlphabet % quantityTransKeys, counterTimes = 0, indexStartSegment = 0, indexEndSegment = indexStartSegment;
        do {
            indexEndSegment += multiTimes;
            alphabetTransSegment = ArrayUtils.subarray(alphabet, indexStartSegment, indexEndSegment);
            alphabetWeftHelper = ArrayUtils.add(alphabetWeftHelper, alphabetTransSegment);
            indexStartSegment = indexEndSegment;
            counterTimes++;
        } while (counterTimes < quantityTransKeys);
        for (int i = 0; i < numKeysSort.length; i++) {
            alphabetWeftMethod = ArrayUtils.addAll(alphabetWeftMethod, alphabetWeftHelper[ArrayUtils.indexOf(numKeysAssign, numKeysSort[i])]);
        }
        indexEndSegment += restTimes;
        alphabetTransSegment = ArrayUtils.subarray(alphabet, indexStartSegment, indexEndSegment);
        alphabetWeftMethod = ArrayUtils.addAll(alphabetWeftMethod, alphabetTransSegment);
        return alphabetWeftMethod;
    }
}
