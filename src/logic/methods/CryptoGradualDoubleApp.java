/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.methods;

import java.util.Arrays;
import logic.entity.CryptoNumbersSort;
import logic.exception.CryptoGlobalException;
import logic.utils.CryptoOtherUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoGradualDoubleApp {

    public static char[] processAlphabet(char alphabet[], String firstKeysApp, CryptoNumbersSort firstStructureApp,
            CryptoNumbersSort secondStructureExtract, String secondKeysApp, CryptoNumbersSort secondStructureApp,
            CryptoNumbersSort resultStructureExtract) throws CryptoGlobalException {
        char alphabetGradualDoubleApp[] = {};
        String[] firstTimesKeys = firstKeysApp.split(","), secondTimesKeys = secondKeysApp.split(",");
        int firstQuantityKeys = firstTimesKeys.length, secondQuantityKeys = secondTimesKeys.length, mainCounter = 0,
                rowAppCounter = 0, characterRowCounter = 0, secondPositionKey = 0;
        Character[][] firstApplication = {}, secondApplication = {};
        Character alphabetObj[] = ArrayUtils.toObject(alphabet), firstRowApp[] = new Character[firstQuantityKeys], secondRowApp[] = new Character[secondQuantityKeys];
        boolean secondRowEndFlag = true, secondAppAddFlag = true;
        Character characterHelper;
        int[] firstNumKeys = CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(firstTimesKeys, firstStructureApp),
                firstExtractKeys = CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(firstTimesKeys, secondStructureExtract),
                secondNumKeys = CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(secondTimesKeys, secondStructureApp),
                secondExtractKeys = CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(secondTimesKeys, resultStructureExtract);
        do {
            rowAppCounter = ArrayUtils.indexOf(firstTimesKeys, String.valueOf(firstNumKeys[mainCounter]));
            do {
                firstRowApp[rowAppCounter] = alphabetObj[characterRowCounter];
                characterRowCounter++;
                rowAppCounter++;
                if (characterRowCounter == alphabetObj.length) {
                    mainCounter = firstQuantityKeys - 1;
                    break;
                }
            } while (rowAppCounter < firstRowApp.length);
            firstApplication = ArrayUtils.add(firstApplication, firstRowApp);
            firstRowApp = new Character[firstQuantityKeys];
            mainCounter++;
            if (mainCounter == firstQuantityKeys && characterRowCounter < alphabetObj.length) {
                mainCounter = 0;
            }
        } while (mainCounter < firstQuantityKeys);
        /*for (Character[] firstApplication1 : firstApplication) {
            for (Character firstApplication11 : firstApplication1) {
                System.out.print(firstApplication11 + " ");
            }
            System.out.println();
        }
        System.out.println();*/
        mainCounter = 0;
        characterRowCounter = 0;
        /*System.out.println(Arrays.toString(firstExtractKeys));
        System.out.println(Arrays.toString(secondNumKeys));*/
        do {
            for (Character[] rowsApp : firstApplication) {
                characterRowCounter++;
                characterHelper = rowsApp[ArrayUtils.indexOf(firstTimesKeys, String.valueOf(firstExtractKeys[mainCounter]))];
                if (characterHelper != null) {
                    secondAppAddFlag = true;
                    if (secondRowEndFlag) {
                        rowAppCounter = ArrayUtils.indexOf(secondTimesKeys, String.valueOf(secondNumKeys[secondPositionKey]));
                        secondRowEndFlag = false;
                    }
                    secondRowApp[rowAppCounter] = characterHelper;
                    rowAppCounter++;
                }
                if (rowAppCounter == secondQuantityKeys || (mainCounter == firstQuantityKeys - 1 && characterRowCounter == firstApplication.length)) {
                    if (secondAppAddFlag) {
                        secondApplication = ArrayUtils.add(secondApplication, secondRowApp);
                        secondRowApp = new Character[secondQuantityKeys];
                        secondAppAddFlag = false;
                        secondRowEndFlag = true;
                    }
                    if (characterRowCounter == firstApplication.length) {
                        secondPositionKey++;
                        if (secondPositionKey == secondQuantityKeys) {
                            secondPositionKey = 0;
                        }
                    }
                }
            }
            characterRowCounter = 0;
            mainCounter++;
        } while (mainCounter < firstQuantityKeys);
        /*for (Character[] firstApplication1 : secondApplication) {
            for (Character firstApplication11 : firstApplication1) {
                System.out.print(firstApplication11 + " ");
            }
            System.out.println();
        }
        System.out.println();*/
        mainCounter = 0;
        do {
            for (Character[] rowsApp : secondApplication) {
                characterHelper = rowsApp[ArrayUtils.indexOf(secondTimesKeys, String.valueOf(secondExtractKeys[mainCounter]))];
                if (characterHelper != null) {
                    alphabetGradualDoubleApp = ArrayUtils.add(alphabetGradualDoubleApp, characterHelper);
                }
            }
            mainCounter++;
        } while (mainCounter < secondQuantityKeys);
        return alphabetGradualDoubleApp;
    }
}
