/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package logic.methods;

import java.util.Arrays;
import logic.entity.CryptoNumbersSort;
import logic.exception.CryptoGlobalException;
import logic.utils.CryptoOtherUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoTranspositionType {

    public static char[] processAlphabet(char alphabet[], String keys, CryptoNumbersSort... structures) throws CryptoGlobalException {
        char alphabetTranspositionMethod[] = {}, alphabetTransSegment[];
        String[] multiTimesKeys = keys.split(","), restTimesKeys;
        int[][] numKeysMultiTimes = {}, numKeysRestTimes = {};
        for (CryptoNumbersSort structure : structures) {
            numKeysMultiTimes = ArrayUtils.add(numKeysMultiTimes, CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(multiTimesKeys, structure));
        }
        int quantityAlphabet = alphabet.length, quantityTransKeys = multiTimesKeys.length, multiTimes = quantityAlphabet / quantityTransKeys,
                restTimes = quantityAlphabet % quantityTransKeys, counterTimes = 0, indexStartSegment = 0, indexEndSegment = indexStartSegment,
                counterStructure = 0, quantityStructure = structures.length;
        restTimesKeys = Arrays.copyOfRange(multiTimesKeys, 0, restTimes);
        for (CryptoNumbersSort structure : structures) {
            numKeysRestTimes = ArrayUtils.add(numKeysRestTimes, CryptoOtherUtils.buildNumericKeysByKeyOnBasedParameters(restTimesKeys, structure));
        }
        do {
            indexEndSegment += quantityTransKeys;
            alphabetTransSegment = ArrayUtils.subarray(alphabet, indexStartSegment, indexEndSegment);
            for (int i = 0; i < quantityTransKeys; i++) {
                alphabetTranspositionMethod = ArrayUtils.add(alphabetTranspositionMethod,
                        alphabetTransSegment[ArrayUtils.indexOf(multiTimesKeys, String.valueOf(numKeysMultiTimes[counterStructure][i]))]);
            }
            indexStartSegment = indexEndSegment;
            counterTimes++;
            counterStructure++;
            if (counterStructure == quantityStructure) {
                counterStructure = 0;
            }
        } while (counterTimes < multiTimes);
        indexEndSegment += restTimes;
        alphabetTransSegment = ArrayUtils.subarray(alphabet, indexStartSegment, indexEndSegment);
        for (int i = 0; i < restTimes; i++) {
            alphabetTranspositionMethod = ArrayUtils.add(alphabetTranspositionMethod,
                    alphabetTransSegment[ArrayUtils.indexOf(restTimesKeys, String.valueOf(numKeysRestTimes[counterStructure][i]))]);
        }
        return alphabetTranspositionMethod;
    }
}
