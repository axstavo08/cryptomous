/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package test;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import logic.entity.CryptoConstants;
import logic.entity.CryptoNumbersSort;
import logic.entity.CryptoNumbersSortStructure;
import logic.exception.CryptoGlobalException;
import logic.methods.CryptoDirSucBouncesType;
import logic.methods.CryptoGradualDoubleApp;
import logic.methods.CryptoNormalDoubleApp;
import logic.methods.CryptoTranspositionType;
import logic.methods.CryptoWeftType;
import logic.utils.CryptoAlphabetUtils;
import logic.utils.CryptoNumbersUtils;
import logic.utils.CryptoOtherUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CryptoPresentation {

    public static void main(String[] args) throws CryptoGlobalException {
        String textToEcrypt = "ií Este es una prueba, para el curso de criptología (02/12/2018).";
        //for (int i = 1; i <= 200; i++) {
        //Variables globales
        //System.out.println("Vuelta: " + i);
        System.out.println("INICIO DE PROCESO CRIPTO");
        char alphabet[];
        String phraseKey = "GUSTAVO123 PRUEBA@";
        String numericKey = "34,87,45,99,150,400,97,320";
        String initialCharacter = "@";
        int loops = 999;
        int alphabetLength = 190;
        CryptoConstants.TypeArranged typeArranged = CryptoConstants.TypeArranged.RANDOM;
        CryptoConstants.FromDirection directionType = CryptoConstants.FromDirection.END;
        //char characterTest = CryptoAlphabetUtils.findCharacterInsteadOfWhiteSpace(loops);

        System.out.println("INICIO DE GENERA ALFABETO");
        //Genera alfabeto
        //alphabet = CryptoAlphabetUtils.generateAlphabetByParameters(typeArranged, loops, (int) characterTest);
        alphabet = CryptoAlphabetUtils.generateAlphabetByParameters(typeArranged, loops, null);
        //alphabet = CryptoAlphabetUtils.generateAlphabet();
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN DE GENERA ALFABETO");

        System.out.println("INICIO LETRA INICIAL");
        //Ordena alfabeto por letra inicial
        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabet, initialCharacter);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN LETRA INICIAL");

        System.out.println("INICIO FRASE CLAVE");
        //Usa frase clave para ordenar alfabeto
        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfPhraseKey(alphabet, phraseKey, directionType);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN FRASE CLAVE");

        /*-----------------------------------------------------------------------------------------------------------------------------------*/
 /*GENERACION DE CRIPTO*/
 /*Saltos sucesivos iniciando en el caracter $*/
        System.out.println("INICIO SALTOS SUCESIVOS");
        String initialCharacterSucBouncesCripto1 = "$";
        String numericKeySucBouncesCripto1 = "200,467,100,87,23,45,76,39";
        String directionKeySucBouncesCripto1 = "IDIIDIDDIII";
        int[] arrNumericKeySucBouncesCripto1 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabet, numericKeySucBouncesCripto1);
        char arrDirectionKeySucBouncesCripto1[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabet, directionKeySucBouncesCripto1);
        boolean arrFlagsSucBouncesCripto1[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabet);
        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabet, initialCharacterSucBouncesCripto1);
        alphabet = CryptoDirSucBouncesType.processAlphabet(alphabet, arrNumericKeySucBouncesCripto1, arrDirectionKeySucBouncesCripto1, arrFlagsSucBouncesCripto1, true);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN SALTOS SUCESIVOS");

        /*Transposicion con varios criterios para la clave numerica*/
        System.out.println("INICIO TRANSPOSICION");
        String transKeysCripto1 = null;
        if (transKeysCripto1 == null) {
            transKeysCripto1 = numericKey;
        }
        //Par 150 ascendente, impar 45 descendente
        CryptoNumbersSort transOptionCriptoA1 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "150"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "45"), CryptoNumbersSort.TypeNumber.EVEN);
        //Impar 99 descendente, par descendente
        CryptoNumbersSort transOptionCriptoB1 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "99"),
                null, CryptoNumbersSort.TypeNumber.ODD);
        //400 descendente
        CryptoNumbersSort transOptionCriptoC1 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "400"), null);
        alphabet = CryptoTranspositionType.processAlphabet(alphabet, transKeysCripto1, transOptionCriptoA1, transOptionCriptoB1, transOptionCriptoC1);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN TRANSPOSICION");

        /*Saltos sucesivos con clave numerica y direccion y con comienza*/
        System.out.println("INICIO SALTOS SUCESIVOS");
        String numericKeySucBouncesCripto2 = "876,32,45,101,215,467,90,191";
        String directionKeySucBouncesCripto2 = "D";
        int[] arrNumericKeySucBouncesCripto2 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabet, numericKeySucBouncesCripto2);
        char arrDirectionKeySucBouncesCripto2[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabet, directionKeySucBouncesCripto2);
        boolean arrFlagsSucBouncesCripto2[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabet);
        alphabet = CryptoDirSucBouncesType.processAlphabet(alphabet, arrNumericKeySucBouncesCripto2, arrDirectionKeySucBouncesCripto2, arrFlagsSucBouncesCripto2, false);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN SALTOS SUCESIVOS");

        /*Tramas con clave numerica y ordern de criterio*/
        System.out.println("INICIO TRAMAS");
        String numericKeyWeftCripto1 = "34,67,324,576,87,109,312,76";
        //215 ascendente
        CryptoNumbersSort weftOptionCriptoA1 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "109"), null);
        //Impar 109 descendente, par descendente
        CryptoNumbersSort weftOptionCriptoA2 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "67"),
                null, CryptoNumbersSort.TypeNumber.ODD);
        alphabet = CryptoWeftType.processAlphabet(alphabet, numericKeyWeftCripto1, weftOptionCriptoA1, weftOptionCriptoA2);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN TRAMAS");

        /*Division de alfabeto y asignacion de metodos*/
        System.out.println("INICIO DIVISION ALFABETO Y APLICACION DE METODOS");
        char alphabetCompundFinalCharCripto1[] = {};
        //Division de alfabeto en 12 pedazos mas con lo que resta
        char alphabetCompoundTempCharCripto1[][] = CryptoAlphabetUtils.divideAlphabetByNumberSegments(alphabet, 12);
        /*Porcion 1*/
        //Saltos sucesivos con clave numerica y direcciones para la primera porcion
        String numericKeysCompSucBounces1 = null;
        if (numericKeysCompSucBounces1 == null) {
            numericKeysCompSucBounces1 = numericKey;
        }
        String directionKeysCompSucBounces1 = "IDDIIDDID";
        int[] arrNumericKeysCompSucBounces1 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[0], numericKeysCompSucBounces1);
        char arrDirectionKeysCompSucBounces1[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[0], directionKeysCompSucBounces1);
        boolean arrFlagsCompSucBounces1[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[0]);
        alphabetCompoundTempCharCripto1[0] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[0], arrNumericKeysCompSucBounces1, arrDirectionKeysCompSucBounces1, arrFlagsCompSucBounces1, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[0]);
        /*Porcion 2*/
        String transKeysCripto2 = null;
        if (transKeysCripto2 == null) {
            transKeysCripto2 = numericKey;
        }
        //Par 400 ascendente, impar 87 descendente
        CryptoNumbersSort transOptionCriptoA2 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "400"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "87"), CryptoNumbersSort.TypeNumber.EVEN);
        //Impar 97 descendente, par descendente
        CryptoNumbersSort transOptionCriptoB2 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "97"),
                null, CryptoNumbersSort.TypeNumber.ODD);
        //400 descendente
        CryptoNumbersSort transOptionCriptoC2 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "320"), null);
        alphabetCompoundTempCharCripto1[1] = CryptoTranspositionType.processAlphabet(alphabetCompoundTempCharCripto1[1], transKeysCripto2, transOptionCriptoA2, transOptionCriptoB2, transOptionCriptoC2);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[1]);
        /*Porcion 3*/
        //Clave numerica y direccion con criterio de inicia en el primer caracter
        String numericKeysCompSucBounces3 = "320,90,123,654,765,12";
        if (numericKeysCompSucBounces3 == null) {
            numericKeysCompSucBounces3 = numericKey;
        }
        String directionKeysCompSucBounces3 = "DIDD";
        int[] arrNumericKeysCompSucBounces3 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[2], numericKeysCompSucBounces3);
        char arrDirectionKeysCompSucBounces3[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[2], directionKeysCompSucBounces3);
        boolean arrFlagsCompSucBounces3[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[2]);
        alphabetCompoundTempCharCripto1[2] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[2], arrNumericKeysCompSucBounces3, arrDirectionKeysCompSucBounces3, arrFlagsCompSucBounces3, true);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[2]);
        /*Porcion 4*/
        //Clave numerica y direccion con criterio de inicia en el primer caracter
        String numericKeysCompSucBounces4 = "123,65,345,698,100,54,23";
        if (numericKeysCompSucBounces4 == null) {
            numericKeysCompSucBounces4 = numericKey;
        }
        String directionKeysCompSucBounces4 = "I";
        int[] arrNumericKeysCompSucBounces4 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[3], numericKeysCompSucBounces4);
        char arrDirectionKeysCompSucBounces4[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[3], directionKeysCompSucBounces4);
        boolean arrFlagsCompSucBounces4[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[3]);
        alphabetCompoundTempCharCripto1[3] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[3], arrNumericKeysCompSucBounces4, arrDirectionKeysCompSucBounces4, arrFlagsCompSucBounces4, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[3]);
        /*Porcion 5*/
        //Clave numerica y direccion con criterio de inicia en el primer caracter
        String numericKeysCompSucBounces5 = null;
        if (numericKeysCompSucBounces5 == null) {
            numericKeysCompSucBounces5 = numericKey;
        }
        String directionKeysCompSucBounces5 = "IDIDDII";
        int[] arrNumericKeysCompSucBounces5 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[4], numericKeysCompSucBounces5);
        char arrDirectionKeysCompSucBounces5[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[4], directionKeysCompSucBounces5);
        boolean arrFlagsCompSucBounces5[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[4]);
        alphabetCompoundTempCharCripto1[4] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[4], arrNumericKeysCompSucBounces5, arrDirectionKeysCompSucBounces5, arrFlagsCompSucBounces5, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[4]);
        /*Porcion 6*/
        //Clave numerica y direccion con criterio de inicia en el primer caracter
        String transKeysCripto3 = "45,67,123,546,876,980,32";
        if (transKeysCripto3 == null) {
            transKeysCripto3 = numericKey;
        }
        //Impar 980 descendente, par 67 ascendente
        CryptoNumbersSort transOptionCriptoA3 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "980"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "67"), CryptoNumbersSort.TypeNumber.ODD);
        //Par 546 ascendente, impar ascendente
        CryptoNumbersSort transOptionCriptoB3 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "546"),
                null, CryptoNumbersSort.TypeNumber.EVEN);
        //32 descendente
        CryptoNumbersSort transOptionCriptoC3 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "32"), null);
        alphabetCompoundTempCharCripto1[5] = CryptoTranspositionType.processAlphabet(alphabetCompoundTempCharCripto1[5], transKeysCripto3,
                transOptionCriptoA3, transOptionCriptoB3, transOptionCriptoC3);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[5]);
        /*Porcion 7*/
        //Saltos sucesivos con clave numerica y direcciones para la primera porcion
        String numericKeysCompSucBounces7 = null;
        if (numericKeysCompSucBounces7 == null) {
            numericKeysCompSucBounces7 = numericKey;
        }
        String directionKeysCompSucBounces7 = "IDDIIDDID";
        int[] arrNumericKeysCompSucBounces7 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[6], numericKeysCompSucBounces7);
        char arrDirectionKeysCompSucBounces7[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[6], directionKeysCompSucBounces7);
        boolean arrFlagsCompSucBounces7[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[6]);
        alphabetCompoundTempCharCripto1[6] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[6], arrNumericKeysCompSucBounces7, arrDirectionKeysCompSucBounces7, arrFlagsCompSucBounces7, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[6]);
        /*Porcion 8*/
        String transKeysCripto8 = "430,87,65,124,654,76";
        if (transKeysCripto8 == null) {
            transKeysCripto8 = numericKey;
        }
        //Par 400 ascendente, impar 87 descendente
        CryptoNumbersSort transOptionCriptoA4 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "430"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "65"), CryptoNumbersSort.TypeNumber.EVEN);
        //Impar 97 descendente, par descendente
        CryptoNumbersSort transOptionCriptoB4 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "76"),
                null, CryptoNumbersSort.TypeNumber.EVEN);
        //400 descendente
        CryptoNumbersSort transOptionCriptoC4 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "87"), null);
        alphabetCompoundTempCharCripto1[7] = CryptoTranspositionType.processAlphabet(alphabetCompoundTempCharCripto1[7], transKeysCripto8, transOptionCriptoA4,
                transOptionCriptoB4, transOptionCriptoC4);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[7]);
        /*Porcion 9*/
        //Clave numerica y direccion con criterio de inicia en el primer caracter
        String numericKeysCompSucBounces8 = "123,65,345,698,100,54,23";
        if (numericKeysCompSucBounces8 == null) {
            numericKeysCompSucBounces8 = numericKey;
        }
        String directionKeysCompSucBounces8 = "IIDIIDIIDDD";
        int[] arrNumericKeysCompSucBounces8 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[8], numericKeysCompSucBounces8);
        char arrDirectionKeysCompSucBounces8[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[8], directionKeysCompSucBounces8);
        boolean arrFlagsCompSucBounces8[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[8]);
        alphabetCompoundTempCharCripto1[8] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[8], arrNumericKeysCompSucBounces8, arrDirectionKeysCompSucBounces8, arrFlagsCompSucBounces8, true);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[8]);
        /*Porcion 10*/
        //Clave numerica y direccion con criterio de inicia en el primer caracter
        String numericKeysCompSucBounces9 = null;
        if (numericKeysCompSucBounces9 == null) {
            numericKeysCompSucBounces9 = numericKey;
        }
        String directionKeysCompSucBounces9 = "DDDDIIDIDII";
        int[] arrNumericKeysCompSucBounces9 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[9], numericKeysCompSucBounces9);
        char arrDirectionKeysCompSucBounces9[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[9], directionKeysCompSucBounces9);
        boolean arrFlagsCompSucBounces9[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[9]);
        alphabetCompoundTempCharCripto1[9] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[9], arrNumericKeysCompSucBounces9,
                arrDirectionKeysCompSucBounces9, arrFlagsCompSucBounces9, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[9]);
        /*Porcion 11*/
        //Clave numerica y direccion con criterio de inicia en el primer caracter
        String numericKeysCompSucBounces10 = "320,345,69,65,154,967,63,267";
        if (numericKeysCompSucBounces10 == null) {
            numericKeysCompSucBounces10 = numericKey;
        }
        String directionKeysCompSucBounces10 = "IDIIIDDIIID";
        int[] arrNumericKeysCompSucBounces10 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[10], numericKeysCompSucBounces10);
        char arrDirectionKeysCompSucBounces10[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[10], directionKeysCompSucBounces10);
        boolean arrFlagsCompSucBounces10[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[10]);
        alphabetCompoundTempCharCripto1[10] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[10], arrNumericKeysCompSucBounces10,
                arrDirectionKeysCompSucBounces10, arrFlagsCompSucBounces10, false);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[10]);
        /*Porcion 12*/
        //Clave numerica y direccion con criterio de inicia en el primer caracter
        String numericKeysCompSucBounces11 = "435,654,12,790,546,325,768";
        if (numericKeysCompSucBounces11 == null) {
            numericKeysCompSucBounces11 = numericKey;
        }
        String directionKeysCompSucBounces11 = "IDIIIDDIIID";
        int[] arrNumericKeysCompSucBounces11 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetCompoundTempCharCripto1[11], numericKeysCompSucBounces11);
        char arrDirectionKeysCompSucBounces11[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetCompoundTempCharCripto1[11], directionKeysCompSucBounces11);
        boolean arrFlagsCompSucBounces11[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetCompoundTempCharCripto1[11]);
        alphabetCompoundTempCharCripto1[11] = CryptoDirSucBouncesType.processAlphabet(alphabetCompoundTempCharCripto1[11], arrNumericKeysCompSucBounces11,
                arrDirectionKeysCompSucBounces11, arrFlagsCompSucBounces11, true);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[11]);
        /*Porcion Restante*/
        String transKeysCripto9 = null;
        if (transKeysCripto9 == null) {
            transKeysCripto9 = numericKey;
        }
        //Par 400 ascendente, impar 87 descendente
        CryptoNumbersSort transOptionCriptoA5 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "34"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "99"), CryptoNumbersSort.TypeNumber.EVEN);
        //Impar 97 descendente, par descendente
        CryptoNumbersSort transOptionCriptoB5 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "97"),
                null, CryptoNumbersSort.TypeNumber.ODD);
        //400 descendente
        CryptoNumbersSort transOptionCriptoC5 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "400"), null);
        alphabetCompoundTempCharCripto1[12] = CryptoTranspositionType.processAlphabet(alphabetCompoundTempCharCripto1[12], transKeysCripto9, transOptionCriptoA5,
                transOptionCriptoB5, transOptionCriptoC5);
        alphabetCompundFinalCharCripto1 = ArrayUtils.addAll(alphabetCompundFinalCharCripto1, alphabetCompoundTempCharCripto1[12]);
        alphabet = alphabetCompundFinalCharCripto1;
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN DIVISION ALFABETO Y APLICACION DE METODOS");

        /*Tramas con clave numerica y ordern de criterio*/
        System.out.println("INICIO TRAMAS");
        String numericKeyWeftCripto2 = "435,647,54,67,1,265,980,213,445,748,324,653,875,923,126";
        //215 ascendente
        CryptoNumbersSort weftOptionCriptoA3 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "213"), null);
        //Impar 109 descendente, par descendente
        CryptoNumbersSort weftOptionCriptoA4 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "748"),
                null, CryptoNumbersSort.TypeNumber.EVEN);
        alphabet = CryptoWeftType.processAlphabet(alphabet, numericKeyWeftCripto2, weftOptionCriptoA3, weftOptionCriptoA4);
        System.out.println(Arrays.toString(alphabet));
        System.out.println("FIN TRAMAS");

        /*Doble aplicacion normal*/
        System.out.println("INICIO DOBLE APLICACION NORMAL");
        String firstNormalDoubleKey = "324,56,12,435,78,89,590,432,657,43,256,467,84";
        String secondNormalDoubleKey = "21,425,65,34,128,76,890,546,32,54,67,43,18,438";
        CryptoNumbersSort firstNormalOptionCripto1 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null);
        CryptoNumbersSort firstNormalOptionCripto2 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "435"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "590"), CryptoNumbersSort.TypeNumber.ODD);
        CryptoNumbersSort firstNormalOptionCripto3 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "546"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "43"), CryptoNumbersSort.TypeNumber.EVEN);
        alphabet = CryptoNormalDoubleApp.processAlphabet(alphabet, firstNormalDoubleKey, firstNormalOptionCripto2,
                firstNormalOptionCripto1, secondNormalDoubleKey, firstNormalOptionCripto3, firstNormalOptionCripto1);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN DOBLE APLICACION NORMAL");

        /*Inversa */
        System.out.println("INICIO INVERSA");
        alphabet = CryptoAlphabetUtils.convertAlphabetToInverse(alphabet);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN INVERSA");

        /*Doble aplicacion gradual*/
        System.out.println("INICIO DOBLE APLICACION GRADUAL");
        String firstGradualDoubleKey = "345,123,765,872,980,435,647,135,654,436,769";
        String secondGradualDoubleKey = "325,127,658,980,437,652,436,674,326,469,547";
        CryptoNumbersSort firstGradualOptionCripto1 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null);
        CryptoNumbersSort firstGradualOptionCripto2 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "425"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "78"), CryptoNumbersSort.TypeNumber.ODD);
        CryptoNumbersSort firstGradualOptionCripto3 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "438"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "67"), CryptoNumbersSort.TypeNumber.EVEN);
        alphabet = CryptoGradualDoubleApp.processAlphabet(alphabet, firstGradualDoubleKey, firstGradualOptionCripto2,
                firstGradualOptionCripto1, secondGradualDoubleKey, firstGradualOptionCripto3, firstGradualOptionCripto1);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN DOBLE APLICACION GRADUAL");

        /*Inversa y que empiece con el caracter + */
        System.out.println("INICIO LETRA INICIAL");
        String initialCharacter2 = "*";
        alphabet = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabet, initialCharacter2);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN LETRA INICIAL");

        System.out.println("INICIO INVERSO");
        alphabet = CryptoAlphabetUtils.convertAlphabetToInverse(alphabet);
        System.out.println(Arrays.toString(alphabet));
        validArrayLength(alphabetLength, alphabet);
        System.out.println("FIN INVERSO");

        System.out.println("FIN DE PROCESO CRIPTO");
        //}
        /*------------------------------------------------------------------------------------------------------------------------------------*/
 /*GENERACION DE LLANO*/
        System.out.println("INICIO DE PROCESO LLANO");
        char alphabetFlat[];
        String numericKeyFlat = "546,768,154,975,452,764,825,143,65";
        String initialCharacterFlat = "8";
        int loopsFlat = 999;
        CryptoConstants.TypeArranged typeArrangedFlat = CryptoConstants.TypeArranged.REVERSE;
        CryptoConstants.FromDirection directionTypeFlat = CryptoConstants.FromDirection.START;

        /*GENERACION DE CRIPTO*/
        System.out.println("INICIO DE GENERA ALFABETO");
        //Genera alfabeto
        //alphabetFlat = CryptoAlphabetUtils.generateAlphabetByParameters(typeArrangedFlat, loopsFlat, (int) characterTest);
        alphabetFlat = CryptoAlphabetUtils.generateAlphabetByParameters(typeArrangedFlat, loopsFlat, null);
        //alphabet = CryptoAlphabetUtils.generateAlphabet();
        System.out.println(Arrays.toString(alphabetFlat));
        validArrayLength(alphabetLength, alphabetFlat);
        System.out.println("FIN DE GENERA ALFABETO");

        System.out.println("INICIO LETRA INICIAL");
        //Ordena alfabeto por letra inicial
        alphabetFlat = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabetFlat, initialCharacterFlat);
        System.out.println(Arrays.toString(alphabetFlat));
        validArrayLength(alphabetLength, alphabetFlat);
        System.out.println("FIN LETRA INICIAL");

        /*Saltos sucesivos iniciando en el caracter $*/
        System.out.println("INICIO SALTOS SUCESIVOS");
        String initialCharacterSucBouncesFlat1 = "?";
        String numericKeySucBouncesPrimesFlat1 = "57,3,10,13,42,100,43,65,23,45,32,55";
        String directionKeySucBouncesFlat1 = "IIDDIIIDIDIDD";
        String numerciKeysSucBouncesFlat1 = CryptoNumbersUtils.buildKeysByArrayNumbers(CryptoNumbersUtils.findPrimeNumbersByKeyPositions(numericKeySucBouncesPrimesFlat1));
        int[] arrNumericKeySucBouncesFlat1 = CryptoOtherUtils.buildNumericKeysByInitialKeyOnBasesAlphabet(alphabetFlat, numerciKeysSucBouncesFlat1);
        char arrDirectionKeySucBouncesFlat1[] = CryptoOtherUtils.buildDirectionKeysByInitialDirOnBasesAlphabet(alphabetFlat, directionKeySucBouncesFlat1);
        boolean arrFlagsSucBouncesFlat1[] = CryptoOtherUtils.buildFlagsOnBasesAlphabet(alphabetFlat);
        alphabetFlat = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabetFlat, initialCharacterSucBouncesFlat1);
        alphabetFlat = CryptoDirSucBouncesType.processAlphabet(alphabetFlat, arrNumericKeySucBouncesFlat1, arrDirectionKeySucBouncesFlat1, arrFlagsSucBouncesFlat1, true);
        System.out.println(Arrays.toString(alphabetFlat));
        validArrayLength(alphabetLength, alphabetFlat);
        System.out.println("FIN SALTOS SUCESIVOS");

        /*Transposicion con varios criterios para la clave numerica*/
        System.out.println("INICIO TRANSPOSICION");
        String transKeysFlat1 = null;
        if (transKeysFlat1 == null) {
            transKeysFlat1 = numericKeyFlat;
        }
        //Par 150 ascendente, impar 45 descendente
        CryptoNumbersSort transOptionFlatA1 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, "150"),
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "45"), CryptoNumbersSort.TypeNumber.EVEN);
        //Impar 99 descendente, par descendente
        CryptoNumbersSort transOptionFlatB1 = new CryptoNumbersSort(new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "99"),
                null, CryptoNumbersSort.TypeNumber.ODD);
        //400 descendente
        CryptoNumbersSort transOptionFlatC1 = new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.DESCENDANT, "400"), null);
        alphabetFlat = CryptoTranspositionType.processAlphabet(alphabetFlat, transKeysFlat1, transOptionFlatA1, transOptionFlatB1, transOptionFlatC1);
        System.out.println(Arrays.toString(alphabetFlat));
        validArrayLength(alphabetLength, alphabetFlat);
        System.out.println("FIN TRANSPOSICION");

        /*Inversa y que empiece con el caracter + */
        System.out.println("INICIO LETRA INICIAL");
        String initialCharacter2Flat = "+";
        alphabetFlat = CryptoAlphabetUtils.convertAlphabetStartingOfCharacter(alphabetFlat, initialCharacter2Flat);
        System.out.println(Arrays.toString(alphabetFlat));
        validArrayLength(alphabetLength, alphabetFlat);
        System.out.println("FIN LETRA INICIAL");

        System.out.println("INICIO INVERSO");
        alphabetFlat = CryptoAlphabetUtils.convertAlphabetToInverse(alphabetFlat);
        System.out.println(Arrays.toString(alphabetFlat));
        validArrayLength(alphabetLength, alphabetFlat);
        System.out.println("FIN INVERSO");

        System.out.println("INICIO DE PROCESO LLANO");

        System.out.println("INICIO DE PROCESO ENCRIPTACION");
        System.out.println("CRIPTO");
        System.out.println(Arrays.toString(alphabet));
        System.out.println("LLANO");
        System.out.println(Arrays.toString(alphabetFlat));
        /*char character1 = Collections.max(Arrays.asList(ArrayUtils.toObject(alphabet)));
        System.out.println(character1);
        char character2 = Collections.max(Arrays.asList(ArrayUtils.toObject(alphabetFlat)));
        System.out.println(character2);*/
        String textEncrypted;
        char arrTextEncrypted[] = {};
        int numCharacter;
        for (char character : textToEcrypt.toCharArray()) {
            numCharacter = (int) character;
            if (numCharacter == 32) {
                character = Collections.max(Arrays.asList(ArrayUtils.toObject(alphabetFlat)));
            }
            arrTextEncrypted = ArrayUtils.add(arrTextEncrypted, alphabet[ArrayUtils.indexOf(alphabetFlat, character)]);
        }
        textEncrypted = String.valueOf(arrTextEncrypted);

        String textDecrypted;
        char arrTextDecrypted[] = {};
        int numCharacterDecrypted;
        for (char character : textEncrypted.toCharArray()) {
            character = alphabetFlat[ArrayUtils.indexOf(alphabet, character)];
            numCharacterDecrypted = (int) character;
            if (numCharacterDecrypted > 255) {
                character = (char) 32;
            }
            arrTextDecrypted = ArrayUtils.add(arrTextDecrypted, character);
        }
        textDecrypted = String.valueOf(arrTextDecrypted);
        System.out.println("INICIO VALIDACION DE ENCRIPTACION");
        System.out.println(textToEcrypt);
        System.out.println(textEncrypted);
        System.out.println(textDecrypted);
        System.out.println("FIN VALIDACION DE ENCRIPTACION");

        System.out.println("FIN DE PROCESO ENCRIPTACION");
    }

    private static void validArrayLength(int length, char alphabet[]) {
        LinkedHashSet<Character> testSet = new LinkedHashSet<>(Arrays.asList(ArrayUtils.toObject(alphabet)));
        Character[] testList = testSet.toArray(new Character[0]);
        int lengthNew = testList.length;
        if (lengthNew != length) {
            System.err.println("Longitud diferente en proceso");
            System.err.println(lengthNew);
        }
    }
}
