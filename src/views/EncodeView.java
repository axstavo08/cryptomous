/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package views;

import com.sun.awt.AWTUtilities;
import java.awt.Color;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import logic.actions.CryptoGeneratorAction;
import logic.exception.CryptoGlobalException;
import resources.alerts.MandatoryFieldToEncryptAlert;

/**
 *
 * @author Gustavo Ramos M.
 */
public class EncodeView extends javax.swing.JFrame {

    private int posX, posY;

    public EncodeView() {
        initComponents();
        this.setLocationRelativeTo(null);
        Shape shape = new RoundRectangle2D.Double(0, 0, this.getBounds().width, this.getBounds().height, 15, 15);
        AWTUtilities.setWindowShape(this, shape);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/images/crypto_logo.png"));
        setIconImage(icon);
        txaToEncode.requestFocus();
        btnMinimize.setContentAreaFilled(false);
        btnClose.setContentAreaFilled(false);
        btnGoToHome.setContentAreaFilled(false);
        btnEncode.setContentAreaFilled(false);
        btnClear.setContentAreaFilled(false);
        pnlClose.setVisible(true);
        pnlClose.setBackground(null);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);
        lblGoToDecode.setForeground(new Color(13, 13, 13));
        lblGoToHome.setForeground(new Color(13, 13, 13));
        pnlEncode.setBackground(null);
        pnlClear.setBackground(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        pnlMinimize = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnMinimize = new javax.swing.JButton();
        pnlClose = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txaToEncoded = new javax.swing.JTextArea();
        lblGoToDecode = new javax.swing.JLabel();
        btnGoToHome = new javax.swing.JButton();
        pnlEncode = new javax.swing.JPanel();
        btnEncode = new javax.swing.JButton();
        pnlClear = new javax.swing.JPanel();
        btnClear = new javax.swing.JButton();
        lblGoToHome = new javax.swing.JLabel();
        btnGoToDecode = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        txaToEncode = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(139, 195, 74));
        jPanel1.setForeground(new java.awt.Color(242, 242, 242));
        jPanel1.setLayout(null);

        jPanel3.setBackground(new java.awt.Color(104, 159, 56));
        jPanel3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel3MouseDragged(evt);
            }
        });
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel3MousePressed(evt);
            }
        });
        jPanel3.setLayout(null);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_icon_01.png"))); // NOI18N
        jPanel3.add(jLabel4);
        jLabel4.setBounds(5, 0, 30, 30);

        pnlMinimize.setBackground(new java.awt.Color(204, 204, 204));
        pnlMinimize.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(null);
        pnlMinimize.add(jPanel6);
        jPanel6.setBounds(660, 0, 50, 30);

        btnMinimize.setBackground(new java.awt.Color(104, 159, 56));
        btnMinimize.setFont(new java.awt.Font("Eras Light ITC", 0, 48)); // NOI18N
        btnMinimize.setForeground(new java.awt.Color(255, 255, 255));
        btnMinimize.setText("-");
        btnMinimize.setToolTipText("Minimizar");
        btnMinimize.setBorder(null);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited(evt);
            }
        });
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });
        pnlMinimize.add(btnMinimize);
        btnMinimize.setBounds(0, 0, 50, 30);

        jPanel3.add(pnlMinimize);
        pnlMinimize.setBounds(900, 0, 50, 30);

        pnlClose.setBackground(new java.awt.Color(204, 0, 0));
        pnlClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pnlCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pnlCloseMouseExited(evt);
            }
        });
        pnlClose.setLayout(null);

        btnClose.setBackground(new java.awt.Color(104, 159, 56));
        btnClose.setFont(new java.awt.Font("Eras Light ITC", 1, 18)); // NOI18N
        btnClose.setForeground(new java.awt.Color(255, 255, 255));
        btnClose.setText("X");
        btnClose.setToolTipText("Cerrar");
        btnClose.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 39, 60), 0, true));
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        pnlClose.add(btnClose);
        btnClose.setBounds(0, 0, 50, 30);

        jPanel3.add(pnlClose);
        pnlClose.setBounds(950, 0, 50, 30);

        jLabel5.setBackground(new java.awt.Color(104, 159, 56));
        jLabel5.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Cifrar");
        jPanel3.add(jLabel5);
        jLabel5.setBounds(0, 0, 1000, 30);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(0, 0, 1000, 30);

        jLabel1.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 20)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Este es el mensaje cifrado");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(90, 340, 250, 40);

        jLabel2.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 20)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Ingrese el mensaje que desea cifrar");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(90, 90, 330, 40);

        txaToEncoded.setEditable(false);
        txaToEncoded.setBackground(new java.awt.Color(204, 204, 204));
        txaToEncoded.setColumns(20);
        txaToEncoded.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 18)); // NOI18N
        txaToEncoded.setForeground(new java.awt.Color(13, 13, 13));
        txaToEncoded.setLineWrap(true);
        txaToEncoded.setRows(5);
        txaToEncoded.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(62, 94, 33)));
        txaToEncoded.setCaretColor(new java.awt.Color(13, 13, 13));
        txaToEncoded.setSelectionColor(new java.awt.Color(139, 195, 74));
        jScrollPane2.setViewportView(txaToEncoded);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(90, 380, 820, 150);

        lblGoToDecode.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 16)); // NOI18N
        lblGoToDecode.setForeground(new java.awt.Color(255, 255, 255));
        lblGoToDecode.setText("Ir a descifrar");
        lblGoToDecode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblGoToDecode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblGoToDecodeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblGoToDecodeMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblGoToDecodeMouseReleased(evt);
            }
        });
        jPanel1.add(lblGoToDecode);
        lblGoToDecode.setBounds(860, 40, 100, 30);

        btnGoToHome.setBackground(new java.awt.Color(62, 94, 33));
        btnGoToHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/if_Left_arrow_2202280.png"))); // NOI18N
        btnGoToHome.setBorder(null);
        btnGoToHome.setContentAreaFilled(false);
        btnGoToHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGoToHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGoToHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGoToHomeMouseExited(evt);
            }
        });
        btnGoToHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoToHomeActionPerformed(evt);
            }
        });
        jPanel1.add(btnGoToHome);
        btnGoToHome.setBounds(10, 40, 50, 30);

        btnEncode.setBackground(new java.awt.Color(139, 195, 74));
        btnEncode.setFont(new java.awt.Font("Microsoft JhengHei Light", 0, 18)); // NOI18N
        btnEncode.setForeground(new java.awt.Color(255, 255, 255));
        btnEncode.setText("Cifrar mensaje");
        btnEncode.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(104, 159, 56)));
        btnEncode.setContentAreaFilled(false);
        btnEncode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEncode.setFocusPainted(false);
        btnEncode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEncodeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnEncodeMouseExited(evt);
            }
        });
        btnEncode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEncodeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlEncodeLayout = new javax.swing.GroupLayout(pnlEncode);
        pnlEncode.setLayout(pnlEncodeLayout);
        pnlEncodeLayout.setHorizontalGroup(
            pnlEncodeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEncodeLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnEncode, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnlEncodeLayout.setVerticalGroup(
            pnlEncodeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEncodeLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnEncode, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(pnlEncode);
        pnlEncode.setBounds(420, 290, 170, 40);

        btnClear.setBackground(new java.awt.Color(139, 195, 74));
        btnClear.setFont(new java.awt.Font("Microsoft JhengHei Light", 0, 18)); // NOI18N
        btnClear.setForeground(new java.awt.Color(255, 255, 255));
        btnClear.setText("Limpiar campos");
        btnClear.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnClear.setContentAreaFilled(false);
        btnClear.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnClear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnClearMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnClearMouseExited(evt);
            }
        });
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlClearLayout = new javax.swing.GroupLayout(pnlClear);
        pnlClear.setLayout(pnlClearLayout);
        pnlClearLayout.setHorizontalGroup(
            pnlClearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlClearLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnlClearLayout.setVerticalGroup(
            pnlClearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlClearLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(pnlClear);
        pnlClear.setBounds(420, 550, 170, 40);

        lblGoToHome.setFont(new java.awt.Font("Microsoft JhengHei Light", 0, 16)); // NOI18N
        lblGoToHome.setForeground(new java.awt.Color(255, 255, 255));
        lblGoToHome.setText("Ir a Inicio");
        lblGoToHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblGoToHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblGoToHomeMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblGoToHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblGoToHomeMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblGoToHomeMouseReleased(evt);
            }
        });
        jPanel1.add(lblGoToHome);
        lblGoToHome.setBounds(40, 40, 90, 30);

        btnGoToDecode.setBackground(new java.awt.Color(62, 94, 33));
        btnGoToDecode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/if_Right_arrow_2202241.png"))); // NOI18N
        btnGoToDecode.setBorder(null);
        btnGoToDecode.setContentAreaFilled(false);
        btnGoToDecode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGoToDecode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGoToDecodeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGoToDecodeMouseExited(evt);
            }
        });
        btnGoToDecode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoToDecodeActionPerformed(evt);
            }
        });
        jPanel1.add(btnGoToDecode);
        btnGoToDecode.setBounds(940, 40, 50, 30);

        txaToEncode.setBackground(new java.awt.Color(242, 242, 242));
        txaToEncode.setColumns(20);
        txaToEncode.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 18)); // NOI18N
        txaToEncode.setForeground(new java.awt.Color(13, 13, 13));
        txaToEncode.setLineWrap(true);
        txaToEncode.setRows(5);
        txaToEncode.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(104, 159, 56), new java.awt.Color(104, 159, 56), new java.awt.Color(104, 159, 56), new java.awt.Color(104, 159, 56)));
        txaToEncode.setCaretColor(new java.awt.Color(13, 13, 13));
        txaToEncode.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txaToEncode.setSelectionColor(new java.awt.Color(139, 195, 74));
        jScrollPane3.setViewportView(txaToEncode);

        jPanel1.add(jScrollPane3);
        jScrollPane3.setBounds(90, 130, 830, 140);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 996, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 630, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        pnlClose.setVisible(true);
        pnlClose.setBackground(new Color(230, 0, 0));
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        pnlClose.setBackground(null);
        btnClose.setVisible(true);
    }//GEN-LAST:event_btnCloseMouseExited

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnCloseActionPerformed

    private void pnlCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseEntered

    }//GEN-LAST:event_pnlCloseMouseEntered

    private void pnlCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseExited

    }//GEN-LAST:event_pnlCloseMouseExited

    private void btnMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseEntered
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnMinimizeMouseEntered

    private void btnMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseExited
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);
    }//GEN-LAST:event_btnMinimizeMouseExited

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void jPanel3MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseDragged
        java.awt.Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_jPanel3MouseDragged

    private void jPanel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_jPanel3MousePressed

    @SuppressWarnings("DeadBranch")
    private void btnEncodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEncodeActionPerformed
        String input = txaToEncode.getText();
        if(input.length() > 0){
           String encrypted = CryptoGeneratorAction.ecryptText(input);
           txaToEncoded.setText(encrypted);
        } else {
            new MandatoryFieldToEncryptAlert(this, true).setVisible(true);
            txaToEncode.requestFocus();
        }
    }//GEN-LAST:event_btnEncodeActionPerformed

    private void lblGoToDecodeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToDecodeMouseEntered
        lblGoToDecode.setForeground(new Color(242, 242, 242));
    }//GEN-LAST:event_lblGoToDecodeMouseEntered

    private void lblGoToDecodeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToDecodeMouseExited
        lblGoToDecode.setForeground(new Color(13, 13, 13));
    }//GEN-LAST:event_lblGoToDecodeMouseExited

    private void lblGoToDecodeMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToDecodeMouseReleased
        DecodeView view = new DecodeView();
        view.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_lblGoToDecodeMouseReleased

    private void btnGoToHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToHomeMouseEntered
        lblGoToHome.setForeground(new Color(242, 242, 242));
    }//GEN-LAST:event_btnGoToHomeMouseEntered

    private void btnGoToHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToHomeMouseExited
        lblGoToHome.setForeground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnGoToHomeMouseExited

    private void btnGoToHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoToHomeActionPerformed
        try {
            new MainView().setVisible(true);
        } catch (CryptoGlobalException ex) {
            Logger.getLogger(EncodeView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_btnGoToHomeActionPerformed

    private void btnEncodeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEncodeMouseEntered
        pnlEncode.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnEncodeMouseEntered

    private void btnClearMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnClearMouseEntered
        pnlClear.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnClearMouseEntered

    private void btnEncodeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEncodeMouseExited
        pnlEncode.setBackground(null);
    }//GEN-LAST:event_btnEncodeMouseExited

    private void btnClearMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnClearMouseExited
        pnlClear.setBackground(null);
    }//GEN-LAST:event_btnClearMouseExited

    private void lblGoToHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToHomeMouseEntered
        lblGoToHome.setForeground(new Color(242, 242, 242));
    }//GEN-LAST:event_lblGoToHomeMouseEntered

    private void lblGoToHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToHomeMouseExited
        lblGoToHome.setForeground(new Color(13, 13, 13));
    }//GEN-LAST:event_lblGoToHomeMouseExited

    private void lblGoToHomeMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToHomeMouseReleased

    }//GEN-LAST:event_lblGoToHomeMouseReleased

    private void lblGoToHomeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToHomeMouseClicked
        try {
            new MainView().setVisible(true);
        } catch (CryptoGlobalException ex) {
            Logger.getLogger(EncodeView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_lblGoToHomeMouseClicked

    private void btnGoToDecodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoToDecodeActionPerformed
        DecodeView view = new DecodeView();
        view.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnGoToDecodeActionPerformed

    private void btnGoToDecodeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToDecodeMouseEntered
        lblGoToDecode.setForeground(Color.white);
    }//GEN-LAST:event_btnGoToDecodeMouseEntered

    private void btnGoToDecodeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToDecodeMouseExited
        lblGoToDecode.setForeground(Color.gray);
    }//GEN-LAST:event_btnGoToDecodeMouseExited

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        txaToEncode.setText("");
        txaToEncoded.setText("");
        txaToEncode.requestFocus();
    }//GEN-LAST:event_btnClearActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EncodeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new EncodeView().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnEncode;
    private javax.swing.JButton btnGoToDecode;
    private javax.swing.JButton btnGoToHome;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblGoToDecode;
    private javax.swing.JLabel lblGoToHome;
    private javax.swing.JPanel pnlClear;
    private javax.swing.JPanel pnlClose;
    private javax.swing.JPanel pnlEncode;
    private javax.swing.JPanel pnlMinimize;
    private javax.swing.JTextArea txaToEncode;
    private javax.swing.JTextArea txaToEncoded;
    // End of variables declaration//GEN-END:variables
}
