/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package views;

import com.sun.awt.AWTUtilities;
import java.awt.Color;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import logic.exception.CryptoGlobalException;
import logic.utils.CryptoInitialConfig;
import resources.helpers.WelcomeCryptomousHelper;

/**
 *
 * @author Gustavo Ramos M.
 */
public class MainView extends javax.swing.JFrame {

    private int posX, posY;
    private static MainView oView;

    public MainView() throws CryptoGlobalException {
        initComponents();
        Shape shape = new RoundRectangle2D.Double(0, 0, this.getBounds().width, this.getBounds().height, 15, 15);
        AWTUtilities.setWindowShape(this, shape);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/images/crypto_logo.png"));
        setIconImage(icon);
        oView = this;
        btnMinimize.setContentAreaFilled(false);
        btnClose.setContentAreaFilled(false);
        btnEncode.setBackground(null);
        btnDecode.setContentAreaFilled(false);
        btnCustomize.setContentAreaFilled(false);
        pnlClose.setVisible(true);
        pnlClose.setBackground(null);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);
        pnlEncode.setBackground(null);
        pnlDecode.setBackground(null);
        pnlCustomize.setBackground(new Color(104, 159, 56));
        CryptoInitialConfig.configAlphabet();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        pnlMainToolbar = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        pnlClose = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        pnlMinimize = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnMinimize = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        pnlEncode = new javax.swing.JPanel();
        btnEncode = new javax.swing.JButton();
        pnlDecode = new javax.swing.JPanel();
        btnDecode = new javax.swing.JButton();
        pnlCustomize = new javax.swing.JPanel();
        btnCustomize = new javax.swing.JButton();
        lblMainTitle = new javax.swing.JLabel();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(153, 153, 153));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setForeground(new java.awt.Color(153, 153, 153));
        setUndecorated(true);
        setType(java.awt.Window.Type.POPUP);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(139, 195, 74));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setBackground(new java.awt.Color(104, 159, 56));
        jLabel1.setFont(new java.awt.Font("Microsoft JhengHei Light", 0, 39)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_logo.png"))); // NOI18N
        jLabel1.setMaximumSize(new java.awt.Dimension(140, 140));
        jLabel1.setMinimumSize(new java.awt.Dimension(140, 140));
        jLabel1.setPreferredSize(new java.awt.Dimension(140, 140));
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 870, 160));

        pnlMainToolbar.setBackground(new java.awt.Color(104, 159, 56));
        pnlMainToolbar.setToolTipText("");
        pnlMainToolbar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                pnlMainToolbarMouseDragged(evt);
            }
        });
        pnlMainToolbar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                pnlMainToolbarMousePressed(evt);
            }
        });
        pnlMainToolbar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_icon_01.png"))); // NOI18N
        jLabel4.setToolTipText("");
        pnlMainToolbar.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 0, 30, 30));

        pnlClose.setBackground(new java.awt.Color(204, 0, 0));
        pnlClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pnlCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pnlCloseMouseExited(evt);
            }
        });
        pnlClose.setLayout(null);

        btnClose.setBackground(new java.awt.Color(179, 0, 0));
        btnClose.setFont(new java.awt.Font("Eras Light ITC", 1, 18)); // NOI18N
        btnClose.setForeground(new java.awt.Color(255, 255, 255));
        btnClose.setText("X");
        btnClose.setToolTipText("Cerrar");
        btnClose.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 39, 60), 0, true));
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        pnlClose.add(btnClose);
        btnClose.setBounds(0, 0, 50, 30);

        pnlMainToolbar.add(pnlClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 0, 50, 30));

        pnlMinimize.setBackground(new java.awt.Color(204, 204, 204));
        pnlMinimize.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(null);
        pnlMinimize.add(jPanel6);
        jPanel6.setBounds(660, 0, 50, 30);

        btnMinimize.setBackground(new java.awt.Color(104, 159, 56));
        btnMinimize.setFont(new java.awt.Font("Eras Light ITC", 0, 48)); // NOI18N
        btnMinimize.setForeground(new java.awt.Color(204, 204, 204));
        btnMinimize.setText("-");
        btnMinimize.setToolTipText("Minimizar");
        btnMinimize.setBorder(null);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited(evt);
            }
        });
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });
        pnlMinimize.add(btnMinimize);
        btnMinimize.setBounds(0, 0, 50, 30);

        pnlMainToolbar.add(pnlMinimize, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 0, 50, 30));

        jLabel5.setBackground(new java.awt.Color(101, 0, 0));
        jLabel5.setFont(new java.awt.Font("Microsoft JhengHei Light", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Cryptomous");
        jLabel5.setToolTipText("");
        jLabel5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabel5.setMaximumSize(new java.awt.Dimension(58, 18));
        jLabel5.setMinimumSize(new java.awt.Dimension(58, 18));
        jLabel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jLabel5MouseDragged(evt);
            }
        });
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel5MousePressed(evt);
            }
        });
        pnlMainToolbar.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 820, 30));

        jPanel2.add(pnlMainToolbar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 870, -1));

        pnlEncode.setBackground(new java.awt.Color(104, 159, 56));

        btnEncode.setBackground(new java.awt.Color(255, 255, 255));
        btnEncode.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 1, 24)); // NOI18N
        btnEncode.setForeground(new java.awt.Color(255, 255, 255));
        btnEncode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_encrypt.png"))); // NOI18N
        btnEncode.setText("Cifrar");
        btnEncode.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnEncode.setContentAreaFilled(false);
        btnEncode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEncode.setIconTextGap(8);
        btnEncode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEncodeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnEncodeMouseExited(evt);
            }
        });
        btnEncode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEncodeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlEncodeLayout = new javax.swing.GroupLayout(pnlEncode);
        pnlEncode.setLayout(pnlEncodeLayout);
        pnlEncodeLayout.setHorizontalGroup(
            pnlEncodeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEncodeLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnEncode, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnlEncodeLayout.setVerticalGroup(
            pnlEncodeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEncodeLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnEncode, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.add(pnlEncode, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 310, 200, 80));

        pnlDecode.setBackground(new java.awt.Color(104, 159, 56));

        btnDecode.setBackground(new java.awt.Color(0, 39, 53));
        btnDecode.setFont(new java.awt.Font("Microsoft JhengHei Light", 1, 24)); // NOI18N
        btnDecode.setForeground(new java.awt.Color(255, 255, 255));
        btnDecode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_decrypt.png"))); // NOI18N
        btnDecode.setText("Descifrar");
        btnDecode.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(104, 159, 56)));
        btnDecode.setContentAreaFilled(false);
        btnDecode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDecode.setIconTextGap(8);
        btnDecode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDecodeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDecodeMouseExited(evt);
            }
        });
        btnDecode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDecodeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDecodeLayout = new javax.swing.GroupLayout(pnlDecode);
        pnlDecode.setLayout(pnlDecodeLayout);
        pnlDecodeLayout.setHorizontalGroup(
            pnlDecodeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDecodeLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnDecode, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnlDecodeLayout.setVerticalGroup(
            pnlDecodeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDecodeLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnDecode, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.add(pnlDecode, new org.netbeans.lib.awtextra.AbsoluteConstraints(475, 310, -1, 80));

        pnlCustomize.setBackground(new java.awt.Color(104, 159, 56));
        pnlCustomize.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnCustomize.setBackground(new java.awt.Color(139, 195, 74));
        btnCustomize.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        btnCustomize.setForeground(new java.awt.Color(255, 255, 255));
        btnCustomize.setText("Personalizar Sistema de Ciframiento");
        btnCustomize.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnCustomize.setContentAreaFilled(false);
        btnCustomize.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCustomize.setIconTextGap(8);
        btnCustomize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCustomizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCustomizeMouseExited(evt);
            }
        });
        btnCustomize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomizeActionPerformed(evt);
            }
        });
        pnlCustomize.add(btnCustomize, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 220, 40));

        jPanel2.add(pnlCustomize, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 410, 220, 40));

        lblMainTitle.setBackground(new java.awt.Color(104, 159, 56));
        lblMainTitle.setFont(new java.awt.Font("Microsoft JhengHei Light", 0, 60)); // NOI18N
        lblMainTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblMainTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMainTitle.setText("Cryptomous");
        lblMainTitle.setToolTipText("");
        jPanel2.add(lblMainTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 210, 870, 80));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 870, 460));

        setSize(new java.awt.Dimension(870, 458));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void showWelcomeMessage() {
        new WelcomeCryptomousHelper(this, true).setVisible(true);
    }

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void pnlMainToolbarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlMainToolbarMousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_pnlMainToolbarMousePressed

    private void pnlMainToolbarMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlMainToolbarMouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_pnlMainToolbarMouseDragged

    private void btnMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseEntered
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnMinimizeMouseEntered

    private void btnMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseExited
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);

    }//GEN-LAST:event_btnMinimizeMouseExited

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        pnlClose.setVisible(true);
        pnlClose.setBackground(new Color(230, 0, 0));
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        pnlClose.setBackground(null);
        btnClose.setVisible(true);
    }//GEN-LAST:event_btnCloseMouseExited

    private void pnlCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseExited

    }//GEN-LAST:event_pnlCloseMouseExited

    private void pnlCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseEntered

    }//GEN-LAST:event_pnlCloseMouseEntered

    private void btnEncodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEncodeActionPerformed
        EncodeView view = new EncodeView();
        view.setVisible(true);
        view.setLocationRelativeTo(null);
        view.setSize(996, 630);
        this.setVisible(false);
    }//GEN-LAST:event_btnEncodeActionPerformed

    private void btnCustomizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustomizeActionPerformed
        new LoginView().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnCustomizeActionPerformed

    private void btnDecodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDecodeActionPerformed
        DecodeView view = new DecodeView();
        view.setVisible(true);
        view.setLocationRelativeTo(null);
        view.setSize(996, 630);
        this.setVisible(false);
    }//GEN-LAST:event_btnDecodeActionPerformed

    private void btnEncodeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEncodeMouseEntered
        pnlEncode.setBackground(new Color(104, 159, 56));
        btnEncode.setForeground(Color.white);
    }//GEN-LAST:event_btnEncodeMouseEntered

    private void btnDecodeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDecodeMouseEntered
        pnlDecode.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnDecodeMouseEntered

    private void btnEncodeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEncodeMouseExited
        pnlEncode.setBackground(null);
    }//GEN-LAST:event_btnEncodeMouseExited

    private void btnDecodeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDecodeMouseExited
        pnlDecode.setBackground(null);
    }//GEN-LAST:event_btnDecodeMouseExited

    private void btnCustomizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCustomizeMouseEntered
        pnlCustomize.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnCustomizeMouseEntered

    private void btnCustomizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCustomizeMouseExited
        pnlCustomize.setBackground(null);
    }//GEN-LAST:event_btnCustomizeMouseExited

    private void jLabel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_jLabel5MousePressed

    private void jLabel5MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_jLabel5MouseDragged

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            try {
                new MainView().setVisible(true);
                oView.showWelcomeMessage();
            } catch (CryptoGlobalException ex) {
                Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnCustomize;
    private javax.swing.JButton btnDecode;
    private javax.swing.JButton btnEncode;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lblMainTitle;
    private javax.swing.JPanel pnlClose;
    private javax.swing.JPanel pnlCustomize;
    private javax.swing.JPanel pnlDecode;
    private javax.swing.JPanel pnlEncode;
    private javax.swing.JPanel pnlMainToolbar;
    private javax.swing.JPanel pnlMinimize;
    // End of variables declaration//GEN-END:variables
}
