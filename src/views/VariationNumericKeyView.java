/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package views;

import com.sun.awt.AWTUtilities;
import java.awt.Color;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;
import logic.entity.CryptoConstants;
import logic.entity.CryptoNumbersSort;
import logic.entity.CryptoNumbersSortStructure;
import org.apache.commons.lang3.math.NumberUtils;
import resources.alerts.OnlyNumbersAlert;

/**
 *
 * @author Gustavo Ramos M.
 */
public class VariationNumericKeyView extends javax.swing.JFrame {

    private int posX, posY;
    private String numericKey;
    private CryptoNumbersSort.TypeSort type;
    private final Object oView;
    private final Object oRestriction;

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public VariationNumericKeyView(Object view, Object restriction) {
        initComponents();
        @SuppressWarnings("OverridableMethodCallInConstructor")
        Shape shape = new RoundRectangle2D.Double(0, 0, this.getBounds().width, this.getBounds().height, 15, 15);
        AWTUtilities.setWindowShape(this, shape);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/images/crypto_logo.png"));
        setIconImage(icon);
        oView = view;
        oRestriction = restriction;
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setBackground(null);
        btnClose.setContentAreaFilled(false);
        pnlClose.setBackground(null);
        rbNormal.setSelected(true);
        rbEvenOrdering.setSelected(true);
        rbEvenAscendant.setSelected(true);
        rbOddAscendant.setSelected(true);
        rbNormalAscendant.setSelected(true);
        type = CryptoNumbersSort.TypeSort.NORMAL;
        jpEvenAndOdd.setVisible(false);
        jpNormal.setVisible(true);
        pnlSaveConfiguration.setBackground(null);
    }

    public static VariationNumericKeyView editConfigurationViewByParameters(Object view, Object restriction, CryptoNumbersSort variation) {
        VariationNumericKeyView vView = new VariationNumericKeyView(view, restriction);
        switch (variation.getType()) {
            case NORMAL:
                vView.rbNormal.setSelected(true);
                vView.rbEvenAndOdd.setSelected(false);
                vView.jpNormal.setVisible(true);
                vView.jpEvenAndOdd.setVisible(false);
                vView.type = CryptoNumbersSort.TypeSort.NORMAL;
                vView.txtNormalInitialNumber.setText((variation.getFirstStructure().getInitial() == null) ? "" : variation.getFirstStructure().getInitial());
                switch (variation.getFirstStructure().getDirection()) {
                    case ASCENDANT:
                        vView.rbNormalAscendant.setSelected(true);
                        vView.rbNormalDescendant.setSelected(false);
                        break;
                    case DESCENDANT:
                        vView.rbNormalAscendant.setSelected(false);
                        vView.rbNormalDescendant.setSelected(true);
                        break;
                }
                break;
            case COMPOUND:
                vView.rbNormal.setSelected(false);
                vView.rbEvenAndOdd.setSelected(true);
                vView.jpNormal.setVisible(false);
                vView.jpEvenAndOdd.setVisible(true);
                vView.type = CryptoNumbersSort.TypeSort.COMPOUND;
                switch (variation.getTypeNumber()) {
                    case EVEN:
                        vView.rbEvenOrdering.setSelected(true);
                        vView.rbOddOrdering.setSelected(false);
                        vView.txtEvenInitialNumber.setText((variation.getFirstStructure().getInitial() == null) ? "" : variation.getFirstStructure().getInitial());
                        switch (variation.getFirstStructure().getDirection()) {
                            case ASCENDANT:
                                vView.rbEvenAscendant.setSelected(true);
                                vView.rbEvenDescendant.setSelected(false);
                                break;
                            case DESCENDANT:
                                vView.rbEvenAscendant.setSelected(false);
                                vView.rbEvenDescendant.setSelected(true);
                                break;
                        }
                        vView.txtOddInitialNumber.setText((variation.getSecondStructure().getInitial() == null) ? "" : variation.getSecondStructure().getInitial());
                        switch (variation.getSecondStructure().getDirection()) {
                            case ASCENDANT:
                                vView.rbOddAscendant.setSelected(true);
                                vView.rbOddDescendant.setSelected(false);
                                break;
                            case DESCENDANT:
                                vView.rbOddAscendant.setSelected(false);
                                vView.rbOddDescendant.setSelected(true);
                                break;
                        }
                        break;
                    case ODD:
                        vView.rbEvenOrdering.setSelected(false);
                        vView.rbOddOrdering.setSelected(true);
                        vView.txtOddInitialNumber.setText((variation.getFirstStructure().getInitial() == null) ? "" : variation.getFirstStructure().getInitial());
                        switch (variation.getFirstStructure().getDirection()) {
                            case ASCENDANT:
                                vView.rbOddAscendant.setSelected(true);
                                vView.rbOddDescendant.setSelected(false);
                                break;
                            case DESCENDANT:
                                vView.rbOddAscendant.setSelected(false);
                                vView.rbOddDescendant.setSelected(true);
                                break;
                        }
                        vView.txtEvenInitialNumber.setText((variation.getSecondStructure().getInitial() == null) ? "" : variation.getSecondStructure().getInitial());
                        switch (variation.getSecondStructure().getDirection()) {
                            case ASCENDANT:
                                vView.rbEvenAscendant.setSelected(true);
                                vView.rbEvenDescendant.setSelected(false);
                                break;
                            case DESCENDANT:
                                vView.rbEvenAscendant.setSelected(false);
                                vView.rbEvenDescendant.setSelected(true);
                                break;
                        }
                        break;
                }
                break;
        }
        return vView;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        pnlClose = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        pnlMinimize = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnMinimize = new javax.swing.JButton();
        rbEvenAndOdd = new javax.swing.JRadioButton();
        jpNormal = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        rbNormalAscendant = new javax.swing.JRadioButton();
        rbNormalDescendant = new javax.swing.JRadioButton();
        jLabel18 = new javax.swing.JLabel();
        txtNormalInitialNumber = new javax.swing.JTextField();
        jpEvenAndOdd = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        rbEvenAscendant = new javax.swing.JRadioButton();
        rbEvenDescendant = new javax.swing.JRadioButton();
        jLabel15 = new javax.swing.JLabel();
        rbEvenOrdering = new javax.swing.JRadioButton();
        rbOddOrdering = new javax.swing.JRadioButton();
        jLabel13 = new javax.swing.JLabel();
        txtEvenInitialNumber = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        rbOddAscendant = new javax.swing.JRadioButton();
        rbOddDescendant = new javax.swing.JRadioButton();
        jLabel17 = new javax.swing.JLabel();
        txtOddInitialNumber = new javax.swing.JTextField();
        rbNormal = new javax.swing.JRadioButton();
        pnlSaveConfiguration = new javax.swing.JPanel();
        btnSaveConfiguration = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Personalizar");
        setBackground(new java.awt.Color(35, 86, 104));
        setName("Personalizar"); // NOI18N
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(139, 195, 74));
        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jPanel4.setForeground(new java.awt.Color(255, 173, 51));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(104, 159, 56));
        jPanel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel5MouseDragged(evt);
            }
        });
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel5MousePressed(evt);
            }
        });
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_icon_01.png"))); // NOI18N
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 0, 30, 30));

        jLabel9.setBackground(new java.awt.Color(104, 159, 56));
        jLabel9.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Generar Variación de Clave Numérica");
        jPanel5.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 460, 30));

        pnlClose.setBackground(new java.awt.Color(204, 0, 0));
        pnlClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pnlCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pnlCloseMouseExited(evt);
            }
        });
        pnlClose.setLayout(null);

        btnClose.setBackground(new java.awt.Color(104, 159, 56));
        btnClose.setFont(new java.awt.Font("Eras Light ITC", 1, 18)); // NOI18N
        btnClose.setForeground(new java.awt.Color(255, 255, 255));
        btnClose.setText("X");
        btnClose.setToolTipText("Cerrar");
        btnClose.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 39, 60), 0, true));
        btnClose.setName(""); // NOI18N
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        pnlClose.add(btnClose);
        btnClose.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 0, 50, 30));

        pnlMinimize.setBackground(new java.awt.Color(104, 159, 56));
        pnlMinimize.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(null);
        pnlMinimize.add(jPanel6);
        jPanel6.setBounds(660, 0, 50, 30);

        btnMinimize.setBackground(new java.awt.Color(104, 159, 56));
        btnMinimize.setFont(new java.awt.Font("Eras Light ITC", 0, 48)); // NOI18N
        btnMinimize.setForeground(new java.awt.Color(255, 255, 255));
        btnMinimize.setText("-");
        btnMinimize.setToolTipText("Minimizar");
        btnMinimize.setBorder(null);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited(evt);
            }
        });
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });
        pnlMinimize.add(btnMinimize);
        btnMinimize.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlMinimize, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 0, 50, 30));

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 540, 30));

        rbEvenAndOdd.setBackground(new java.awt.Color(242, 242, 242));
        rbEvenAndOdd.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        rbEvenAndOdd.setForeground(new java.awt.Color(242, 242, 242));
        rbEvenAndOdd.setText("Pares e impares");
        rbEvenAndOdd.setContentAreaFilled(false);
        rbEvenAndOdd.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbEvenAndOdd.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbEvenAndOdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbEvenAndOddActionPerformed(evt);
            }
        });
        jPanel4.add(rbEvenAndOdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 40, 160, 40));

        jpNormal.setBackground(new java.awt.Color(104, 159, 56));
        jpNormal.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jpNormal.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel14.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Ordenamiento");
        jpNormal.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 85, 110, 30));

        rbNormalAscendant.setBackground(new java.awt.Color(242, 242, 242));
        rbNormalAscendant.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        rbNormalAscendant.setForeground(new java.awt.Color(242, 242, 242));
        rbNormalAscendant.setText("Ascendente");
        rbNormalAscendant.setContentAreaFilled(false);
        rbNormalAscendant.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbNormalAscendant.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbNormalAscendant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbNormalAscendantActionPerformed(evt);
            }
        });
        jpNormal.add(rbNormalAscendant, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 80, 130, 40));

        rbNormalDescendant.setBackground(new java.awt.Color(242, 242, 242));
        rbNormalDescendant.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        rbNormalDescendant.setForeground(new java.awt.Color(242, 242, 242));
        rbNormalDescendant.setText("Descendente");
        rbNormalDescendant.setContentAreaFilled(false);
        rbNormalDescendant.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbNormalDescendant.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbNormalDescendant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbNormalDescendantActionPerformed(evt);
            }
        });
        jpNormal.add(rbNormalDescendant, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 80, 130, 40));

        jLabel18.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Número inicial");
        jpNormal.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 150, 110, 30));

        txtNormalInitialNumber.setBackground(new java.awt.Color(242, 242, 242));
        txtNormalInitialNumber.setColumns(1);
        txtNormalInitialNumber.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtNormalInitialNumber.setForeground(new java.awt.Color(13, 13, 13));
        txtNormalInitialNumber.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNormalInitialNumber.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtNormalInitialNumber.setCaretColor(new java.awt.Color(13, 13, 13));
        txtNormalInitialNumber.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtNormalInitialNumber.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtNormalInitialNumber.setSelectionStart(1);
        txtNormalInitialNumber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNormalInitialNumberFocusLost(evt);
            }
        });
        txtNormalInitialNumber.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtNormalInitialNumberInputMethodTextChanged(evt);
            }
        });
        txtNormalInitialNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNormalInitialNumberActionPerformed(evt);
            }
        });
        txtNormalInitialNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNormalInitialNumberKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNormalInitialNumberKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNormalInitialNumberKeyTyped(evt);
            }
        });
        jpNormal.add(txtNormalInitialNumber, new org.netbeans.lib.awtextra.AbsoluteConstraints(175, 150, 110, 30));

        jPanel4.add(jpNormal, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 480, 260));

        jpEvenAndOdd.setBackground(new java.awt.Color(104, 159, 56));
        jpEvenAndOdd.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jpEvenAndOdd.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Ordernamiento inicial");
        jpEvenAndOdd.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 140, 30));

        rbEvenAscendant.setBackground(new java.awt.Color(242, 242, 242));
        rbEvenAscendant.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        rbEvenAscendant.setForeground(new java.awt.Color(242, 242, 242));
        rbEvenAscendant.setText("Ascendente");
        rbEvenAscendant.setContentAreaFilled(false);
        rbEvenAscendant.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbEvenAscendant.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbEvenAscendant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbEvenAscendantActionPerformed(evt);
            }
        });
        jpEvenAndOdd.add(rbEvenAscendant, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 60, 130, 40));

        rbEvenDescendant.setBackground(new java.awt.Color(242, 242, 242));
        rbEvenDescendant.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        rbEvenDescendant.setForeground(new java.awt.Color(242, 242, 242));
        rbEvenDescendant.setText("Descendente");
        rbEvenDescendant.setContentAreaFilled(false);
        rbEvenDescendant.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbEvenDescendant.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbEvenDescendant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbEvenDescendantActionPerformed(evt);
            }
        });
        jpEvenAndOdd.add(rbEvenDescendant, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 60, 130, 40));

        jLabel15.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Impar ordenamiento");
        jpEvenAndOdd.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 140, 30));

        rbEvenOrdering.setBackground(new java.awt.Color(242, 242, 242));
        rbEvenOrdering.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        rbEvenOrdering.setForeground(new java.awt.Color(242, 242, 242));
        rbEvenOrdering.setText("Par");
        rbEvenOrdering.setContentAreaFilled(false);
        rbEvenOrdering.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbEvenOrdering.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbEvenOrdering.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbEvenOrderingActionPerformed(evt);
            }
        });
        jpEvenAndOdd.add(rbEvenOrdering, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 12, 130, 40));

        rbOddOrdering.setBackground(new java.awt.Color(242, 242, 242));
        rbOddOrdering.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        rbOddOrdering.setForeground(new java.awt.Color(242, 242, 242));
        rbOddOrdering.setText("Impar");
        rbOddOrdering.setContentAreaFilled(false);
        rbOddOrdering.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbOddOrdering.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbOddOrdering.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbOddOrderingActionPerformed(evt);
            }
        });
        jpEvenAndOdd.add(rbOddOrdering, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 12, 130, 40));

        jLabel13.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Impar número inicial");
        jpEvenAndOdd.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 150, 30));

        txtEvenInitialNumber.setBackground(new java.awt.Color(242, 242, 242));
        txtEvenInitialNumber.setColumns(1);
        txtEvenInitialNumber.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtEvenInitialNumber.setForeground(new java.awt.Color(13, 13, 13));
        txtEvenInitialNumber.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtEvenInitialNumber.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtEvenInitialNumber.setCaretColor(new java.awt.Color(13, 13, 13));
        txtEvenInitialNumber.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtEvenInitialNumber.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtEvenInitialNumber.setSelectionStart(1);
        txtEvenInitialNumber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtEvenInitialNumberFocusLost(evt);
            }
        });
        txtEvenInitialNumber.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtEvenInitialNumberInputMethodTextChanged(evt);
            }
        });
        txtEvenInitialNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEvenInitialNumberActionPerformed(evt);
            }
        });
        txtEvenInitialNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtEvenInitialNumberKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtEvenInitialNumberKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEvenInitialNumberKeyTyped(evt);
            }
        });
        jpEvenAndOdd.add(txtEvenInitialNumber, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 110, 110, 30));

        jLabel16.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Par ordenamiento");
        jpEvenAndOdd.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 65, 140, 30));

        rbOddAscendant.setBackground(new java.awt.Color(242, 242, 242));
        rbOddAscendant.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        rbOddAscendant.setForeground(new java.awt.Color(242, 242, 242));
        rbOddAscendant.setText("Ascendente");
        rbOddAscendant.setContentAreaFilled(false);
        rbOddAscendant.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbOddAscendant.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbOddAscendant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbOddAscendantActionPerformed(evt);
            }
        });
        jpEvenAndOdd.add(rbOddAscendant, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 155, 130, 40));

        rbOddDescendant.setBackground(new java.awt.Color(242, 242, 242));
        rbOddDescendant.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        rbOddDescendant.setForeground(new java.awt.Color(242, 242, 242));
        rbOddDescendant.setText("Descendente");
        rbOddDescendant.setContentAreaFilled(false);
        rbOddDescendant.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbOddDescendant.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbOddDescendant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbOddDescendantActionPerformed(evt);
            }
        });
        jpEvenAndOdd.add(rbOddDescendant, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 155, 130, 40));

        jLabel17.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Par número inicial");
        jpEvenAndOdd.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 150, 30));

        txtOddInitialNumber.setBackground(new java.awt.Color(242, 242, 242));
        txtOddInitialNumber.setColumns(1);
        txtOddInitialNumber.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtOddInitialNumber.setForeground(new java.awt.Color(13, 13, 13));
        txtOddInitialNumber.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtOddInitialNumber.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtOddInitialNumber.setCaretColor(new java.awt.Color(13, 13, 13));
        txtOddInitialNumber.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtOddInitialNumber.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtOddInitialNumber.setSelectionStart(1);
        txtOddInitialNumber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtOddInitialNumberFocusLost(evt);
            }
        });
        txtOddInitialNumber.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtOddInitialNumberInputMethodTextChanged(evt);
            }
        });
        txtOddInitialNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOddInitialNumberActionPerformed(evt);
            }
        });
        txtOddInitialNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtOddInitialNumberKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtOddInitialNumberKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOddInitialNumberKeyTyped(evt);
            }
        });
        jpEvenAndOdd.add(txtOddInitialNumber, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 210, 110, 30));

        jPanel4.add(jpEvenAndOdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 480, 260));

        rbNormal.setBackground(new java.awt.Color(242, 242, 242));
        rbNormal.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        rbNormal.setForeground(new java.awt.Color(242, 242, 242));
        rbNormal.setText("Normal");
        rbNormal.setContentAreaFilled(false);
        rbNormal.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbNormal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbNormal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbNormalActionPerformed(evt);
            }
        });
        jPanel4.add(rbNormal, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 40, 90, 40));

        pnlSaveConfiguration.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnSaveConfiguration.setBackground(new java.awt.Color(139, 195, 74));
        btnSaveConfiguration.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnSaveConfiguration.setForeground(new java.awt.Color(242, 242, 242));
        btnSaveConfiguration.setText("GENERAR");
        btnSaveConfiguration.setToolTipText("");
        btnSaveConfiguration.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnSaveConfiguration.setContentAreaFilled(false);
        btnSaveConfiguration.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSaveConfiguration.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseExited(evt);
            }
        });
        btnSaveConfiguration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveConfigurationActionPerformed(evt);
            }
        });
        pnlSaveConfiguration.add(btnSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 90, 40));

        jPanel4.add(pnlSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 385, 90, 40));

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 540, 450));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseEntered
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnMinimizeMouseEntered

    private void btnMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseExited
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);
    }//GEN-LAST:event_btnMinimizeMouseExited

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        pnlClose.setVisible(true);
        pnlClose.setBackground(new Color(230, 0, 0));
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        pnlClose.setBackground(null);
        btnClose.setVisible(true);
    }//GEN-LAST:event_btnCloseMouseExited

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void pnlCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseEntered

    }//GEN-LAST:event_pnlCloseMouseEntered

    private void pnlCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseExited

    }//GEN-LAST:event_pnlCloseMouseExited

    private void jPanel5MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_jPanel5MouseDragged

    private void jPanel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_jPanel5MousePressed

    private void btnSaveConfigurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveConfigurationActionPerformed
        CryptoNumbersSort obj = new CryptoNumbersSort();
        switch (type) {
            case NORMAL:
                CryptoNumbersSortStructure.DirectionSort normalOrdering = (rbNormalAscendant.isSelected()) ? CryptoNumbersSortStructure.DirectionSort.ASCENDANT
                        : CryptoNumbersSortStructure.DirectionSort.DESCENDANT;
                String normalInitialNumber = (txtNormalInitialNumber.getText().length() > 0) ? txtNormalInitialNumber.getText() : null;
                obj = new CryptoNumbersSort(type, new CryptoNumbersSortStructure(normalOrdering, normalInitialNumber), null);
                break;
            case COMPOUND:
                CryptoNumbersSort.TypeNumber typeOrdering = (rbEvenOrdering.isSelected()) ? CryptoNumbersSort.TypeNumber.EVEN : CryptoNumbersSort.TypeNumber.ODD;
                CryptoNumbersSortStructure.DirectionSort evenOrdering = (rbEvenAscendant.isSelected()) ? CryptoNumbersSortStructure.DirectionSort.ASCENDANT
                        : CryptoNumbersSortStructure.DirectionSort.DESCENDANT;
                CryptoNumbersSortStructure.DirectionSort oddOrdering = (rbOddAscendant.isSelected()) ? CryptoNumbersSortStructure.DirectionSort.ASCENDANT
                        : CryptoNumbersSortStructure.DirectionSort.DESCENDANT;
                String evenInitialNumber = (txtEvenInitialNumber.getText().length() > 0) ? txtEvenInitialNumber.getText() : null;
                String oddInitialNumber = (txtOddInitialNumber.getText().length() > 0) ? txtNormalInitialNumber.getText() : null;
                switch (typeOrdering) {
                    case EVEN:
                        obj = new CryptoNumbersSort(new CryptoNumbersSortStructure(evenOrdering, evenInitialNumber),
                                new CryptoNumbersSortStructure(oddOrdering, oddInitialNumber), typeOrdering);
                        break;
                    case ODD:
                        obj = new CryptoNumbersSort(new CryptoNumbersSortStructure(oddOrdering, oddInitialNumber),
                                new CryptoNumbersSortStructure(evenOrdering, evenInitialNumber), typeOrdering);
                        break;
                }
                break;
        }
        if (oView instanceof WeftView) {
            WeftView view = (WeftView) oView;
            view.generateVariationNumber((CryptoConstants.WeftRestrictionNumber) oRestriction, obj);
        } else if (oView instanceof TranspositionView) {
            TranspositionView view = (TranspositionView) oView;
            view.generateVariationNumber((CryptoConstants.TranspositionRestrictionNumber) oRestriction, obj);
        } else if (oView instanceof NormalDoubleAppView) {
            NormalDoubleAppView view = (NormalDoubleAppView) oView;
            view.generateVariationNumber((CryptoConstants.NormalDoubleAppRestrictionNumber) oRestriction, obj);
        } else if (oView instanceof GradualDoubleAppView) {
            GradualDoubleAppView view = (GradualDoubleAppView) oView;
            view.generateVariationNumber((CryptoConstants.GradualDoubleAppRestrictionNumber) oRestriction, obj);
        }
        dispose();
    }//GEN-LAST:event_btnSaveConfigurationActionPerformed

    private void rbEvenDescendantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbEvenDescendantActionPerformed
        rbEvenAscendant.setSelected(false);
        rbEvenDescendant.setSelected(true);
    }//GEN-LAST:event_rbEvenDescendantActionPerformed

    private void rbEvenAndOddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbEvenAndOddActionPerformed
        rbNormal.setSelected(false);
        rbEvenAndOdd.setSelected(true);
        jpEvenAndOdd.setVisible(true);
        jpNormal.setVisible(false);
        type = CryptoNumbersSort.TypeSort.COMPOUND;
    }//GEN-LAST:event_rbEvenAndOddActionPerformed

    private void txtNormalInitialNumberFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNormalInitialNumberFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNormalInitialNumberFocusLost

    private void txtNormalInitialNumberInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtNormalInitialNumberInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNormalInitialNumberInputMethodTextChanged

    private void txtNormalInitialNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNormalInitialNumberKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNormalInitialNumberKeyPressed

    private void txtNormalInitialNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNormalInitialNumberKeyReleased
        String input = txtNormalInitialNumber.getText();
        boolean isNumeric = NumberUtils.isParsable(input);
        if (!isNumeric && input.length() > 0) {
            new OnlyNumbersAlert(this, true).setVisible(true);
            txtNormalInitialNumber.requestFocus();
        }
    }//GEN-LAST:event_txtNormalInitialNumberKeyReleased

    private void txtNormalInitialNumberKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNormalInitialNumberKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNormalInitialNumberKeyTyped

    private void txtNormalInitialNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNormalInitialNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNormalInitialNumberActionPerformed

    private void btnSaveConfigurationMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseEntered
        pnlSaveConfiguration.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnSaveConfigurationMouseEntered

    private void btnSaveConfigurationMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseExited
        pnlSaveConfiguration.setBackground(null);
    }//GEN-LAST:event_btnSaveConfigurationMouseExited

    private void rbNormalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbNormalActionPerformed
        rbNormal.setSelected(true);
        rbEvenAndOdd.setSelected(false);
        jpEvenAndOdd.setVisible(false);
        jpNormal.setVisible(true);
        type = CryptoNumbersSort.TypeSort.NORMAL;
    }//GEN-LAST:event_rbNormalActionPerformed

    private void rbEvenAscendantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbEvenAscendantActionPerformed
        rbEvenAscendant.setSelected(true);
        rbEvenDescendant.setSelected(false);
    }//GEN-LAST:event_rbEvenAscendantActionPerformed

    private void rbNormalAscendantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbNormalAscendantActionPerformed
        rbNormalAscendant.setSelected(true);
        rbNormalDescendant.setSelected(false);
    }//GEN-LAST:event_rbNormalAscendantActionPerformed

    private void rbNormalDescendantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbNormalDescendantActionPerformed
        rbNormalAscendant.setSelected(false);
        rbNormalDescendant.setSelected(true);
    }//GEN-LAST:event_rbNormalDescendantActionPerformed

    private void rbEvenOrderingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbEvenOrderingActionPerformed
        rbEvenOrdering.setSelected(true);
        rbOddOrdering.setSelected(false);
    }//GEN-LAST:event_rbEvenOrderingActionPerformed

    private void rbOddOrderingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbOddOrderingActionPerformed
        rbEvenOrdering.setSelected(false);
        rbOddOrdering.setSelected(true);
    }//GEN-LAST:event_rbOddOrderingActionPerformed

    private void txtEvenInitialNumberFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtEvenInitialNumberFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEvenInitialNumberFocusLost

    private void txtEvenInitialNumberInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtEvenInitialNumberInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEvenInitialNumberInputMethodTextChanged

    private void txtEvenInitialNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEvenInitialNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEvenInitialNumberActionPerformed

    private void txtEvenInitialNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEvenInitialNumberKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEvenInitialNumberKeyPressed

    private void txtEvenInitialNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEvenInitialNumberKeyReleased
        String input = txtEvenInitialNumber.getText();
        boolean isNumeric = NumberUtils.isParsable(input);
        if (!isNumeric && input.length() > 0) {
            new OnlyNumbersAlert(this, true).setVisible(true);
            txtEvenInitialNumber.requestFocus();
        }
    }//GEN-LAST:event_txtEvenInitialNumberKeyReleased

    private void txtEvenInitialNumberKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEvenInitialNumberKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEvenInitialNumberKeyTyped

    private void rbOddAscendantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbOddAscendantActionPerformed
        rbOddAscendant.setSelected(true);
        rbOddDescendant.setSelected(false);
    }//GEN-LAST:event_rbOddAscendantActionPerformed

    private void rbOddDescendantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbOddDescendantActionPerformed
        rbOddAscendant.setSelected(false);
        rbOddDescendant.setSelected(true);
    }//GEN-LAST:event_rbOddDescendantActionPerformed

    private void txtOddInitialNumberFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtOddInitialNumberFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOddInitialNumberFocusLost

    private void txtOddInitialNumberInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtOddInitialNumberInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOddInitialNumberInputMethodTextChanged

    private void txtOddInitialNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOddInitialNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOddInitialNumberActionPerformed

    private void txtOddInitialNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOddInitialNumberKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOddInitialNumberKeyPressed

    private void txtOddInitialNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOddInitialNumberKeyReleased
        String input = txtOddInitialNumber.getText();
        boolean isNumeric = NumberUtils.isParsable(input);
        if (!isNumeric && input.length() > 0) {
            new OnlyNumbersAlert(this, true).setVisible(true);
            txtOddInitialNumber.requestFocus();
        }
    }//GEN-LAST:event_txtOddInitialNumberKeyReleased

    private void txtOddInitialNumberKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOddInitialNumberKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOddInitialNumberKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VariationNumericKeyView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new VariationNumericKeyView(null, null).setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JButton btnSaveConfiguration;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jpEvenAndOdd;
    private javax.swing.JPanel jpNormal;
    private javax.swing.JPanel pnlClose;
    private javax.swing.JPanel pnlMinimize;
    private javax.swing.JPanel pnlSaveConfiguration;
    private javax.swing.JRadioButton rbEvenAndOdd;
    private javax.swing.JRadioButton rbEvenAscendant;
    private javax.swing.JRadioButton rbEvenDescendant;
    private javax.swing.JRadioButton rbEvenOrdering;
    private javax.swing.JRadioButton rbNormal;
    private javax.swing.JRadioButton rbNormalAscendant;
    private javax.swing.JRadioButton rbNormalDescendant;
    private javax.swing.JRadioButton rbOddAscendant;
    private javax.swing.JRadioButton rbOddDescendant;
    private javax.swing.JRadioButton rbOddOrdering;
    private javax.swing.JTextField txtEvenInitialNumber;
    private javax.swing.JTextField txtNormalInitialNumber;
    private javax.swing.JTextField txtOddInitialNumber;
    // End of variables declaration//GEN-END:variables
}
