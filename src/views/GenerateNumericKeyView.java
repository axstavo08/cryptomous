/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package views;

import com.sun.awt.AWTUtilities;
import java.awt.Color;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import logic.entity.CryptoConstants.TypeGenerationNumericKey;
import logic.exception.CryptoGlobalException;
import logic.utils.CryptoNumbersUtils;
import org.apache.commons.lang3.math.NumberUtils;
import resources.alerts.DuplicateNumericKeyAlert;
import resources.alerts.LimitNumericKeysAlert;
import resources.alerts.MandatoryFieldAlert;
import resources.alerts.MinimumNumericFieldAlert;
import resources.alerts.OnlyNumbersAlert;
import resources.helpers.NumericKeyConfigHelper;

/**
 *
 * @author Gustavo Ramos M.
 */
public class GenerateNumericKeyView extends javax.swing.JFrame {

    private int posX, posY;
    private String numericKey;
    private TypeGenerationNumericKey type;
    private final Object oView;

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public GenerateNumericKeyView(Object view) {
        initComponents();
        @SuppressWarnings("OverridableMethodCallInConstructor")
        Shape shape = new RoundRectangle2D.Double(0, 0, this.getBounds().width, this.getBounds().height, 15, 15);
        AWTUtilities.setWindowShape(this, shape);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/images/crypto_logo.png"));
        setIconImage(icon);
        oView = view;
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setBackground(null);
        btnClose.setContentAreaFilled(false);
        pnlClose.setBackground(null);
        rbRandomNumericKey.setSelected(true);
        type = TypeGenerationNumericKey.RANDOM;
        jpRandomNumericKey.setVisible(true);
        jpPosPrimesNumbsNumericKey.setVisible(false);
        pnlSaveConfiguration.setBackground(null);
        txtMinValue.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        pnlClose = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        pnlMinimize = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnMinimize = new javax.swing.JButton();
        pnlSaveConfiguration = new javax.swing.JPanel();
        btnSaveConfiguration = new javax.swing.JButton();
        rbRandomNumericKey = new javax.swing.JRadioButton();
        rbPosPrimesNumsNumericKey = new javax.swing.JRadioButton();
        jpRandomNumericKey = new javax.swing.JPanel();
        txtQuantityNums = new javax.swing.JTextField();
        txtMaxValue = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtMinValue = new javax.swing.JTextField();
        jpPosPrimesNumbsNumericKey = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txtPosPrimesNums = new javax.swing.JTextField();
        numericKeyConfig01 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Personalizar");
        setBackground(new java.awt.Color(35, 86, 104));
        setName("Personalizar"); // NOI18N
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(139, 195, 74));
        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jPanel4.setForeground(new java.awt.Color(255, 173, 51));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(104, 159, 56));
        jPanel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel5MouseDragged(evt);
            }
        });
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel5MousePressed(evt);
            }
        });
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_icon_01.png"))); // NOI18N
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 0, 30, 30));

        jLabel9.setBackground(new java.awt.Color(104, 159, 56));
        jLabel9.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Generar Clave Numérica");
        jPanel5.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 350, 30));

        pnlClose.setBackground(new java.awt.Color(204, 0, 0));
        pnlClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pnlCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pnlCloseMouseExited(evt);
            }
        });
        pnlClose.setLayout(null);

        btnClose.setBackground(new java.awt.Color(104, 159, 56));
        btnClose.setFont(new java.awt.Font("Eras Light ITC", 1, 18)); // NOI18N
        btnClose.setForeground(new java.awt.Color(255, 255, 255));
        btnClose.setText("X");
        btnClose.setToolTipText("Cerrar");
        btnClose.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 39, 60), 0, true));
        btnClose.setName(""); // NOI18N
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        pnlClose.add(btnClose);
        btnClose.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 0, 50, 30));

        pnlMinimize.setBackground(new java.awt.Color(104, 159, 56));
        pnlMinimize.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(null);
        pnlMinimize.add(jPanel6);
        jPanel6.setBounds(660, 0, 50, 30);

        btnMinimize.setBackground(new java.awt.Color(104, 159, 56));
        btnMinimize.setFont(new java.awt.Font("Eras Light ITC", 0, 48)); // NOI18N
        btnMinimize.setForeground(new java.awt.Color(255, 255, 255));
        btnMinimize.setText("-");
        btnMinimize.setToolTipText("Minimizar");
        btnMinimize.setBorder(null);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited(evt);
            }
        });
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });
        pnlMinimize.add(btnMinimize);
        btnMinimize.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlMinimize, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 0, 50, 30));

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 430, 30));

        pnlSaveConfiguration.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnSaveConfiguration.setBackground(new java.awt.Color(139, 195, 74));
        btnSaveConfiguration.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnSaveConfiguration.setForeground(new java.awt.Color(242, 242, 242));
        btnSaveConfiguration.setText("GENERAR");
        btnSaveConfiguration.setToolTipText("");
        btnSaveConfiguration.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnSaveConfiguration.setContentAreaFilled(false);
        btnSaveConfiguration.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSaveConfiguration.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseExited(evt);
            }
        });
        btnSaveConfiguration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveConfigurationActionPerformed(evt);
            }
        });
        pnlSaveConfiguration.add(btnSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 90, 40));

        jPanel4.add(pnlSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 310, 90, 40));

        rbRandomNumericKey.setBackground(new java.awt.Color(242, 242, 242));
        rbRandomNumericKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        rbRandomNumericKey.setForeground(new java.awt.Color(242, 242, 242));
        rbRandomNumericKey.setText("Aleatorio");
        rbRandomNumericKey.setContentAreaFilled(false);
        rbRandomNumericKey.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbRandomNumericKey.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbRandomNumericKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbRandomNumericKeyActionPerformed(evt);
            }
        });
        jPanel4.add(rbRandomNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, 90, 40));

        rbPosPrimesNumsNumericKey.setBackground(new java.awt.Color(242, 242, 242));
        rbPosPrimesNumsNumericKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        rbPosPrimesNumsNumericKey.setForeground(new java.awt.Color(242, 242, 242));
        rbPosPrimesNumsNumericKey.setText("Posición de números primos");
        rbPosPrimesNumsNumericKey.setContentAreaFilled(false);
        rbPosPrimesNumsNumericKey.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbPosPrimesNumsNumericKey.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbPosPrimesNumsNumericKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbPosPrimesNumsNumericKeyActionPerformed(evt);
            }
        });
        jPanel4.add(rbPosPrimesNumsNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 40, 200, 40));

        jpRandomNumericKey.setBackground(new java.awt.Color(104, 159, 56));
        jpRandomNumericKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jpRandomNumericKey.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtQuantityNums.setBackground(new java.awt.Color(242, 242, 242));
        txtQuantityNums.setColumns(1);
        txtQuantityNums.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtQuantityNums.setForeground(new java.awt.Color(13, 13, 13));
        txtQuantityNums.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtQuantityNums.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtQuantityNums.setCaretColor(new java.awt.Color(13, 13, 13));
        txtQuantityNums.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtQuantityNums.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtQuantityNums.setSelectionStart(1);
        txtQuantityNums.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtQuantityNumsFocusLost(evt);
            }
        });
        txtQuantityNums.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtQuantityNumsInputMethodTextChanged(evt);
            }
        });
        txtQuantityNums.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQuantityNumsActionPerformed(evt);
            }
        });
        txtQuantityNums.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtQuantityNumsKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQuantityNumsKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtQuantityNumsKeyTyped(evt);
            }
        });
        jpRandomNumericKey.add(txtQuantityNums, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 125, 160, 30));

        txtMaxValue.setBackground(new java.awt.Color(242, 242, 242));
        txtMaxValue.setColumns(1);
        txtMaxValue.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtMaxValue.setForeground(new java.awt.Color(13, 13, 13));
        txtMaxValue.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMaxValue.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtMaxValue.setCaretColor(new java.awt.Color(13, 13, 13));
        txtMaxValue.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtMaxValue.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtMaxValue.setSelectionStart(1);
        txtMaxValue.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtMaxValueFocusLost(evt);
            }
        });
        txtMaxValue.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtMaxValueInputMethodTextChanged(evt);
            }
        });
        txtMaxValue.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMaxValueKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMaxValueKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMaxValueKeyTyped(evt);
            }
        });
        jpRandomNumericKey.add(txtMaxValue, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 75, 160, 30));

        jLabel11.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Valor mínimo");
        jpRandomNumericKey.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 25, 120, 30));

        jLabel12.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Valor máximo");
        jpRandomNumericKey.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 75, 120, 30));

        jLabel13.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Cantidad de números");
        jpRandomNumericKey.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 125, 150, 30));

        txtMinValue.setBackground(new java.awt.Color(242, 242, 242));
        txtMinValue.setColumns(1);
        txtMinValue.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtMinValue.setForeground(new java.awt.Color(13, 13, 13));
        txtMinValue.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMinValue.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtMinValue.setCaretColor(new java.awt.Color(13, 13, 13));
        txtMinValue.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtMinValue.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtMinValue.setSelectionStart(1);
        txtMinValue.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtMinValueFocusLost(evt);
            }
        });
        txtMinValue.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtMinValueInputMethodTextChanged(evt);
            }
        });
        txtMinValue.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMinValueKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMinValueKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMinValueKeyTyped(evt);
            }
        });
        jpRandomNumericKey.add(txtMinValue, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 25, 160, 30));

        jPanel4.add(jpRandomNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 370, 180));

        jpPosPrimesNumbsNumericKey.setBackground(new java.awt.Color(104, 159, 56));
        jpPosPrimesNumbsNumericKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jpPosPrimesNumbsNumericKey.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel14.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Posiciones");
        jpPosPrimesNumbsNumericKey.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 75, 80, 30));

        txtPosPrimesNums.setBackground(new java.awt.Color(242, 242, 242));
        txtPosPrimesNums.setColumns(1);
        txtPosPrimesNums.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtPosPrimesNums.setForeground(new java.awt.Color(13, 13, 13));
        txtPosPrimesNums.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPosPrimesNums.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtPosPrimesNums.setCaretColor(new java.awt.Color(13, 13, 13));
        txtPosPrimesNums.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtPosPrimesNums.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtPosPrimesNums.setSelectionStart(1);
        txtPosPrimesNums.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPosPrimesNumsFocusLost(evt);
            }
        });
        txtPosPrimesNums.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtPosPrimesNumsInputMethodTextChanged(evt);
            }
        });
        txtPosPrimesNums.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPosPrimesNumsKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPosPrimesNumsKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPosPrimesNumsKeyTyped(evt);
            }
        });
        jpPosPrimesNumbsNumericKey.add(txtPosPrimesNums, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 75, 240, 30));

        jPanel4.add(jpPosPrimesNumbsNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 370, 180));

        numericKeyConfig01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        numericKeyConfig01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        numericKeyConfig01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                numericKeyConfig01MouseClicked(evt);
            }
        });
        jPanel4.add(numericKeyConfig01, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 45, -1, 30));

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 430, 370));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseEntered
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnMinimizeMouseEntered

    private void btnMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseExited
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);
    }//GEN-LAST:event_btnMinimizeMouseExited

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        pnlClose.setVisible(true);
        pnlClose.setBackground(new Color(230, 0, 0));
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        pnlClose.setBackground(null);
        btnClose.setVisible(true);
    }//GEN-LAST:event_btnCloseMouseExited

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void pnlCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseEntered

    }//GEN-LAST:event_pnlCloseMouseEntered

    private void pnlCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseExited

    }//GEN-LAST:event_pnlCloseMouseExited

    private void jPanel5MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_jPanel5MouseDragged

    private void jPanel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_jPanel5MousePressed

    private void btnSaveConfigurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveConfigurationActionPerformed
        String field;
        switch (type) {
            case RANDOM:
                field = txtMinValue.getText();
                if (field.length() == 0) {
                    txtMinValue.setText("1");
                    /*new MandatoryFieldAlert(this, true, "mínimo").setVisible(true);
                    txtMinValue.requestFocus();
                    return;*/
                }
                field = txtMaxValue.getText();
                if (field.length() == 0) {
                    txtMaxValue.setText("1000");
                    /*new MandatoryFieldAlert(this, true, "máximo").setVisible(true);
                    txtMaxValue.requestFocus();
                    return;*/
                }
                field = txtQuantityNums.getText();
                if (field.length() == 0) {
                    txtQuantityNums.setText("20");
                   /* new MandatoryFieldAlert(this, true, "cantidad").setVisible(true);
                    txtQuantityNums.requestFocus();
                    return;*/
                }
                numericKey = CryptoNumbersUtils.buildKeysByArrayNumbers(CryptoNumbersUtils.buildRandomValuesByParameters(Integer.parseInt(txtMinValue.getText()),
                        Integer.parseInt(txtMaxValue.getText()), Integer.parseInt(txtQuantityNums.getText())));
                break;
            case POSITION_PRIME_NUMBERS:
                field = txtPosPrimesNums.getText();
                if (field.length() == 0) {
                    txtPosPrimesNums.setText("20,500,56,43,67,54,23,76,88,127");
                    /*new MandatoryFieldAlert(this, true, "posiciones").setVisible(true);
                    txtPosPrimesNums.requestFocus();
                    return;*/
                }
                String[] numbers = field.split(",");
                Set<String> setNumbers = new HashSet<>(Arrays.asList(numbers));
                if (numbers.length != setNumbers.size()) {
                    new DuplicateNumericKeyAlert(this, true).setVisible(true);
                    txtPosPrimesNums.requestFocus();
                    return;
                }
                try {
                    numericKey = CryptoNumbersUtils.buildKeysByArrayNumbers(CryptoNumbersUtils.findPrimeNumbersByKeyPositions(txtPosPrimesNums.getText()));
                } catch (CryptoGlobalException ex) {
                    Logger.getLogger(GenerateNumericKeyView.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
        if (oView instanceof CustomizeView) {
            CustomizeView view = (CustomizeView) oView;
            view.generateGlobalNumericKey(numericKey);
        } else if (oView instanceof SuccessiveBouncesView) {
            SuccessiveBouncesView view = (SuccessiveBouncesView) oView;
            view.generateNumericKey(numericKey);
        } else if (oView instanceof WeftView) {
            WeftView view = (WeftView) oView;
            view.generateNumericKey(numericKey);
        } else if (oView instanceof TranspositionView) {
            TranspositionView view = (TranspositionView) oView;
            view.generateNumericKey(numericKey);
        } else if (oView instanceof NormalDoubleAppView) {
            NormalDoubleAppView view = (NormalDoubleAppView) oView;
            view.generateNumericKey(numericKey);
        } else if (oView instanceof GradualDoubleAppView) {
            GradualDoubleAppView view = (GradualDoubleAppView) oView;
            view.generateNumericKey(numericKey);
        }
        dispose();
    }//GEN-LAST:event_btnSaveConfigurationActionPerformed

    private void rbRandomNumericKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbRandomNumericKeyActionPerformed
        rbRandomNumericKey.setSelected(true);
        rbPosPrimesNumsNumericKey.setSelected(false);
        type = TypeGenerationNumericKey.RANDOM;
        jpRandomNumericKey.setVisible(true);
        jpPosPrimesNumbsNumericKey.setVisible(false);
        txtPosPrimesNums.setText("");
        txtMinValue.requestFocus();
    }//GEN-LAST:event_rbRandomNumericKeyActionPerformed

    private void txtMaxValueFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMaxValueFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaxValueFocusLost

    private void txtMaxValueInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtMaxValueInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaxValueInputMethodTextChanged

    private void txtMaxValueKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMaxValueKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaxValueKeyPressed

    private void txtMaxValueKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMaxValueKeyReleased
        String input = txtMaxValue.getText();
        boolean isNumeric = NumberUtils.isParsable(input);
        if (!isNumeric && input.length() > 0) {
            //txtMaxValue.setText("1000");
            new OnlyNumbersAlert(this, true).setVisible(true);
            txtMaxValue.requestFocus();
            return;
        }
        if (input.length() > 5) {
            txtMaxValue.setText(input.substring(0, input.length() - 1));
        }
    }//GEN-LAST:event_txtMaxValueKeyReleased

    private void txtMaxValueKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMaxValueKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaxValueKeyTyped

    private void rbPosPrimesNumsNumericKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbPosPrimesNumsNumericKeyActionPerformed
        rbRandomNumericKey.setSelected(false);
        rbPosPrimesNumsNumericKey.setSelected(true);
        type = TypeGenerationNumericKey.POSITION_PRIME_NUMBERS;
        jpRandomNumericKey.setVisible(false);
        jpPosPrimesNumbsNumericKey.setVisible(true);
        txtMinValue.setText("");
        txtMaxValue.setText("");
        txtQuantityNums.setText("");
        txtPosPrimesNums.requestFocus();
    }//GEN-LAST:event_rbPosPrimesNumsNumericKeyActionPerformed

    private void txtQuantityNumsFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtQuantityNumsFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQuantityNumsFocusLost

    private void txtQuantityNumsInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtQuantityNumsInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQuantityNumsInputMethodTextChanged

    private void txtQuantityNumsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuantityNumsKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQuantityNumsKeyPressed

    private void txtQuantityNumsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuantityNumsKeyReleased
        String input = txtQuantityNums.getText();
        boolean isNumeric = NumberUtils.isParsable(input);
        if (!isNumeric && input.length() > 0) {
            new OnlyNumbersAlert(this, true).setVisible(true);
            txtQuantityNums.requestFocus();
            return;
        }
        if (input.length() > 0) {
            int value = Integer.parseInt(input);
            if (value > 60) {
                new LimitNumericKeysAlert(this, true).setVisible(true);
                txtQuantityNums.requestFocus();
            }
        }
    }//GEN-LAST:event_txtQuantityNumsKeyReleased

    private void txtQuantityNumsKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuantityNumsKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQuantityNumsKeyTyped

    private void txtQuantityNumsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQuantityNumsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQuantityNumsActionPerformed

    private void btnSaveConfigurationMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseEntered
        pnlSaveConfiguration.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnSaveConfigurationMouseEntered

    private void btnSaveConfigurationMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseExited
        pnlSaveConfiguration.setBackground(null);
    }//GEN-LAST:event_btnSaveConfigurationMouseExited

    private void txtMinValueFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMinValueFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMinValueFocusLost

    private void txtMinValueInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtMinValueInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMinValueInputMethodTextChanged

    private void txtMinValueKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMinValueKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMinValueKeyPressed

    private void txtMinValueKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMinValueKeyReleased
        String input = txtMinValue.getText();
        boolean isNumeric = NumberUtils.isParsable(input);
        if (!isNumeric && input.length() > 0) {
            //txtMinValue.setText("1");
            new OnlyNumbersAlert(this, true).setVisible(true);
            txtMinValue.requestFocus();
            return;
        }
        if (input.length() > 0) {
            int value = Integer.parseInt(input);
            if (value == 0) {
                new MinimumNumericFieldAlert(this, true).setVisible(true);
                txtMinValue.requestFocus();
                return;
            }
        }
        if (input.length() > 5) {
            txtMinValue.setText(input.substring(0, input.length() - 1));
        }
    }//GEN-LAST:event_txtMinValueKeyReleased

    private void txtMinValueKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMinValueKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMinValueKeyTyped

    private void txtPosPrimesNumsFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPosPrimesNumsFocusLost
        String input = txtPosPrimesNums.getText(), separator = ",", lastLetter;
        if (input.length() > 0) {
            lastLetter = input.substring(input.length() - 1, input.length());
            if (lastLetter.equals(separator)) {
                txtPosPrimesNums.setText(input.substring(0, input.length() - 1));
            }
        }
    }//GEN-LAST:event_txtPosPrimesNumsFocusLost

    private void txtPosPrimesNumsInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtPosPrimesNumsInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPosPrimesNumsInputMethodTextChanged

    private void txtPosPrimesNumsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPosPrimesNumsKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPosPrimesNumsKeyPressed

    private void txtPosPrimesNumsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPosPrimesNumsKeyReleased
        String input = txtPosPrimesNums.getText();
        if (input.length() > 0) {
            String[] numbers = input.split(",");
            boolean isNumeric;
            for (String number : numbers) {
                isNumeric = NumberUtils.isParsable(number);
                if (!isNumeric) {
                    new OnlyNumbersAlert(this, true).setVisible(true);
                    txtPosPrimesNums.requestFocus();
                    return;
                }
            }
        }
    }//GEN-LAST:event_txtPosPrimesNumsKeyReleased

    private void txtPosPrimesNumsKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPosPrimesNumsKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPosPrimesNumsKeyTyped

    private void numericKeyConfig01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_numericKeyConfig01MouseClicked
        new NumericKeyConfigHelper(this, true).setVisible(true);
    }//GEN-LAST:event_numericKeyConfig01MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GenerateNumericKeyView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new GenerateNumericKeyView(null).setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JButton btnSaveConfiguration;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jpPosPrimesNumbsNumericKey;
    private javax.swing.JPanel jpRandomNumericKey;
    private javax.swing.JLabel numericKeyConfig01;
    private javax.swing.JPanel pnlClose;
    private javax.swing.JPanel pnlMinimize;
    private javax.swing.JPanel pnlSaveConfiguration;
    private javax.swing.JRadioButton rbPosPrimesNumsNumericKey;
    private javax.swing.JRadioButton rbRandomNumericKey;
    private javax.swing.JTextField txtMaxValue;
    private javax.swing.JTextField txtMinValue;
    private javax.swing.JTextField txtPosPrimesNums;
    private javax.swing.JTextField txtQuantityNums;
    // End of variables declaration//GEN-END:variables
}
