/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package views;

import com.sun.awt.AWTUtilities;
import java.awt.Color;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;
import logic.entity.CryptoConstants;
import logic.entity.CryptoAlphabetConfiguration;
import org.apache.commons.lang3.math.NumberUtils;
import resources.alerts.MandatoryFieldAlert;
import resources.alerts.OnlyNumbersAlert;
import resources.helpers.InitialCharacterHelper;
import resources.helpers.LoopsAlphabetHelper;
import resources.helpers.OrderingAlphabetHelper;
import resources.helpers.UsePhraseKeyHelper;

/**
 *
 * @author Gustavo Ramos M.
 */
public class GenerateAlphabetView extends javax.swing.JFrame {

    private int posX, posY;
    private final CryptoAlphabetConfiguration configuration;
    private final CustomizeView oView;
    private final CryptoConstants.TypeCrypto oType;

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public GenerateAlphabetView(CustomizeView view, CryptoConstants.TypeCrypto type) {
        initComponents();
        @SuppressWarnings("OverridableMethodCallInConstructor")
        Shape shape = new RoundRectangle2D.Double(0, 0, this.getBounds().width, this.getBounds().height, 15, 15);
        AWTUtilities.setWindowShape(this, shape);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/images/crypto_logo.png"));
        setIconImage(icon);
        oView = view;
        oType = type;
        configuration = new CryptoAlphabetConfiguration();
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setBackground(null);
        btnClose.setContentAreaFilled(false);
        pnlClose.setBackground(null);
        rbArrangedAlphabet.setSelected(true);
        pnlSaveConfiguration.setBackground(null);
    }

    public static GenerateAlphabetView editConfigurationViewByParameters(CustomizeView view, CryptoConstants.TypeCrypto type, CryptoConstants.TypeArranged ordering, int loops,
            String initialCharacter, boolean usePhraseKey) {
        GenerateAlphabetView gView = new GenerateAlphabetView(view, type);
        gView.configuration.setOrdering(ordering);
        gView.rbArrangedAlphabet.setSelected(false);
        gView.rbReverseAlphabet.setSelected(false);
        gView.rbRandomAlphabet.setSelected(false);
        switch (ordering) {
            case ARRANGED:
                gView.rbArrangedAlphabet.setSelected(true);
                break;
            case REVERSE:
                gView.rbReverseAlphabet.setSelected(true);
                break;
            case RANDOM:
                gView.rbRandomAlphabet.setSelected(true);
                break;
        }
        gView.configuration.setLoops(loops);
        gView.txtLoopsAlphabet.setText(String.valueOf(loops));
        gView.configuration.setInitialCharacter(initialCharacter);
        gView.txtInitialCharacter.setText(initialCharacter);
        gView.configuration.setUsePhraseKey(usePhraseKey);
        gView.jcbUseKeyPhrase.setSelected(usePhraseKey);
        return gView;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        pnlClose = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        pnlMinimize = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnMinimize = new javax.swing.JButton();
        pnlSaveConfiguration = new javax.swing.JPanel();
        btnSaveConfiguration = new javax.swing.JButton();
        jpInitialGenAlphabet = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        rbArrangedAlphabet = new javax.swing.JRadioButton();
        rbRandomAlphabet = new javax.swing.JRadioButton();
        jLabel19 = new javax.swing.JLabel();
        rbReverseAlphabet = new javax.swing.JRadioButton();
        txtInitialCharacter = new javax.swing.JTextField();
        txtLoopsAlphabet = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jcbUseKeyPhrase = new javax.swing.JCheckBox();
        usePhraseKey01 = new javax.swing.JLabel();
        orderingAlphabet01 = new javax.swing.JLabel();
        loopsAlphabet01 = new javax.swing.JLabel();
        initialCharacter01 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Personalizar");
        setBackground(new java.awt.Color(35, 86, 104));
        setName("Personalizar"); // NOI18N
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(139, 195, 74));
        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jPanel4.setForeground(new java.awt.Color(255, 173, 51));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(104, 159, 56));
        jPanel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel5MouseDragged(evt);
            }
        });
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel5MousePressed(evt);
            }
        });
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_icon_01.png"))); // NOI18N
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 0, 30, 30));

        jLabel9.setBackground(new java.awt.Color(104, 159, 56));
        jLabel9.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Generar Alfabeto");
        jPanel5.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 510, 30));

        pnlClose.setBackground(new java.awt.Color(204, 0, 0));
        pnlClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pnlCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pnlCloseMouseExited(evt);
            }
        });
        pnlClose.setLayout(null);

        btnClose.setBackground(new java.awt.Color(104, 159, 56));
        btnClose.setFont(new java.awt.Font("Eras Light ITC", 1, 18)); // NOI18N
        btnClose.setForeground(new java.awt.Color(255, 255, 255));
        btnClose.setText("X");
        btnClose.setToolTipText("Cerrar");
        btnClose.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 39, 60), 0, true));
        btnClose.setName(""); // NOI18N
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        pnlClose.add(btnClose);
        btnClose.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 0, 50, 30));

        pnlMinimize.setBackground(new java.awt.Color(104, 159, 56));
        pnlMinimize.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(null);
        pnlMinimize.add(jPanel6);
        jPanel6.setBounds(660, 0, 50, 30);

        btnMinimize.setBackground(new java.awt.Color(104, 159, 56));
        btnMinimize.setFont(new java.awt.Font("Eras Light ITC", 0, 48)); // NOI18N
        btnMinimize.setForeground(new java.awt.Color(255, 255, 255));
        btnMinimize.setText("-");
        btnMinimize.setToolTipText("Minimizar");
        btnMinimize.setBorder(null);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited(evt);
            }
        });
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });
        pnlMinimize.add(btnMinimize);
        btnMinimize.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlMinimize, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 0, 50, 30));

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 590, 30));

        pnlSaveConfiguration.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnSaveConfiguration.setBackground(new java.awt.Color(139, 195, 74));
        btnSaveConfiguration.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnSaveConfiguration.setForeground(new java.awt.Color(242, 242, 242));
        btnSaveConfiguration.setText("GUARDAR CONFIGURACIÓN");
        btnSaveConfiguration.setToolTipText("");
        btnSaveConfiguration.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnSaveConfiguration.setContentAreaFilled(false);
        btnSaveConfiguration.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSaveConfiguration.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseExited(evt);
            }
        });
        btnSaveConfiguration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveConfigurationActionPerformed(evt);
            }
        });
        pnlSaveConfiguration.add(btnSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 180, 40));

        jPanel4.add(pnlSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 320, 180, 40));

        jpInitialGenAlphabet.setBackground(new java.awt.Color(104, 159, 56));
        jpInitialGenAlphabet.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jpInitialGenAlphabet.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel10.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Seleccionar ordenamiento");
        jpInitialGenAlphabet.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 25, 170, 30));

        rbArrangedAlphabet.setBackground(new java.awt.Color(242, 242, 242));
        rbArrangedAlphabet.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        rbArrangedAlphabet.setForeground(new java.awt.Color(242, 242, 242));
        rbArrangedAlphabet.setText("Ordenado");
        rbArrangedAlphabet.setContentAreaFilled(false);
        rbArrangedAlphabet.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbArrangedAlphabet.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbArrangedAlphabet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbArrangedAlphabetActionPerformed(evt);
            }
        });
        jpInitialGenAlphabet.add(rbArrangedAlphabet, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 20, 90, 40));

        rbRandomAlphabet.setBackground(new java.awt.Color(242, 242, 242));
        rbRandomAlphabet.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        rbRandomAlphabet.setForeground(new java.awt.Color(242, 242, 242));
        rbRandomAlphabet.setText("Aleatorio");
        rbRandomAlphabet.setContentAreaFilled(false);
        rbRandomAlphabet.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbRandomAlphabet.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbRandomAlphabet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbRandomAlphabetActionPerformed(evt);
            }
        });
        jpInitialGenAlphabet.add(rbRandomAlphabet, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 20, 80, 40));

        jLabel19.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Vueltas");
        jpInitialGenAlphabet.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(95, 70, 80, 30));

        rbReverseAlphabet.setBackground(new java.awt.Color(242, 242, 242));
        rbReverseAlphabet.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        rbReverseAlphabet.setForeground(new java.awt.Color(242, 242, 242));
        rbReverseAlphabet.setText("Inverso");
        rbReverseAlphabet.setContentAreaFilled(false);
        rbReverseAlphabet.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbReverseAlphabet.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbReverseAlphabet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbReverseAlphabetActionPerformed(evt);
            }
        });
        jpInitialGenAlphabet.add(rbReverseAlphabet, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 20, 80, 40));

        txtInitialCharacter.setBackground(new java.awt.Color(242, 242, 242));
        txtInitialCharacter.setColumns(1);
        txtInitialCharacter.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtInitialCharacter.setForeground(new java.awt.Color(13, 13, 13));
        txtInitialCharacter.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtInitialCharacter.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtInitialCharacter.setCaretColor(new java.awt.Color(13, 13, 13));
        txtInitialCharacter.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtInitialCharacter.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtInitialCharacter.setSelectionStart(1);
        txtInitialCharacter.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtInitialCharacterFocusLost(evt);
            }
        });
        txtInitialCharacter.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtInitialCharacterInputMethodTextChanged(evt);
            }
        });
        txtInitialCharacter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtInitialCharacterActionPerformed(evt);
            }
        });
        txtInitialCharacter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtInitialCharacterKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtInitialCharacterKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtInitialCharacterKeyTyped(evt);
            }
        });
        jpInitialGenAlphabet.add(txtInitialCharacter, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 120, 40, 30));

        txtLoopsAlphabet.setBackground(new java.awt.Color(242, 242, 242));
        txtLoopsAlphabet.setColumns(1);
        txtLoopsAlphabet.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtLoopsAlphabet.setForeground(new java.awt.Color(13, 13, 13));
        txtLoopsAlphabet.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtLoopsAlphabet.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtLoopsAlphabet.setCaretColor(new java.awt.Color(13, 13, 13));
        txtLoopsAlphabet.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtLoopsAlphabet.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtLoopsAlphabet.setSelectionStart(1);
        txtLoopsAlphabet.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtLoopsAlphabetFocusLost(evt);
            }
        });
        txtLoopsAlphabet.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtLoopsAlphabetInputMethodTextChanged(evt);
            }
        });
        txtLoopsAlphabet.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtLoopsAlphabetKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtLoopsAlphabetKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtLoopsAlphabetKeyTyped(evt);
            }
        });
        jpInitialGenAlphabet.add(txtLoopsAlphabet, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 70, 80, 30));

        jLabel21.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel21.setText("Caracter inicial");
        jpInitialGenAlphabet.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, 110, 30));

        jcbUseKeyPhrase.setBackground(new java.awt.Color(104, 159, 56));
        jcbUseKeyPhrase.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        jcbUseKeyPhrase.setForeground(new java.awt.Color(242, 242, 242));
        jcbUseKeyPhrase.setText("Usar frase clave");
        jcbUseKeyPhrase.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jcbUseKeyPhrase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbUseKeyPhraseActionPerformed(evt);
            }
        });
        jpInitialGenAlphabet.add(jcbUseKeyPhrase, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 170, -1, -1));

        usePhraseKey01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        usePhraseKey01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        usePhraseKey01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                usePhraseKey01MouseClicked(evt);
            }
        });
        jpInitialGenAlphabet.add(usePhraseKey01, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 170, -1, 30));

        orderingAlphabet01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        orderingAlphabet01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        orderingAlphabet01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                orderingAlphabet01MouseClicked(evt);
            }
        });
        jpInitialGenAlphabet.add(orderingAlphabet01, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 25, -1, 30));

        loopsAlphabet01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        loopsAlphabet01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        loopsAlphabet01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loopsAlphabet01MouseClicked(evt);
            }
        });
        jpInitialGenAlphabet.add(loopsAlphabet01, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 70, -1, 30));

        initialCharacter01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        initialCharacter01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        initialCharacter01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                initialCharacter01MouseClicked(evt);
            }
        });
        jpInitialGenAlphabet.add(initialCharacter01, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 120, -1, 30));

        jPanel4.add(jpInitialGenAlphabet, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, 510, 220));

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 590, 380));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseEntered
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnMinimizeMouseEntered

    private void btnMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseExited
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);
    }//GEN-LAST:event_btnMinimizeMouseExited

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        pnlClose.setVisible(true);
        pnlClose.setBackground(new Color(230, 0, 0));
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        pnlClose.setBackground(null);
        btnClose.setVisible(true);
    }//GEN-LAST:event_btnCloseMouseExited

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void pnlCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseEntered

    }//GEN-LAST:event_pnlCloseMouseEntered

    private void pnlCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseExited

    }//GEN-LAST:event_pnlCloseMouseExited

    private void jPanel5MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_jPanel5MouseDragged

    private void jPanel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_jPanel5MousePressed

    private void btnSaveConfigurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveConfigurationActionPerformed
        String field = txtLoopsAlphabet.getText();
        if (field.length() == 0) {
            txtLoopsAlphabet.setText("1");
            /*new MandatoryFieldAlert(this, true, "vueltas").setVisible(true);
            txtLoopsAlphabet.requestFocus();
            return;*/
        }
        //field = txtInitialCharacter.getText();
        /*if (field.length() == 0) {
            new MandatoryFieldAlert(this, true, "caracter").setVisible(true);
            txtInitialCharacter.requestFocus();
            return;
        }*/
        configuration.setLoops(Integer.parseInt(txtLoopsAlphabet.getText()));
        configuration.setInitialCharacter(txtInitialCharacter.getText());
        oView.setGenerateAlphabetConfiguration(oType, configuration.getOrdering(), configuration.getLoops(), configuration.getInitialCharacter(), configuration.isUsePhraseKey());
        dispose();
    }//GEN-LAST:event_btnSaveConfigurationActionPerformed

    private void rbArrangedAlphabetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbArrangedAlphabetActionPerformed
        rbArrangedAlphabet.setSelected(true);
        rbReverseAlphabet.setSelected(false);
        rbRandomAlphabet.setSelected(false);
        configuration.setOrdering(CryptoConstants.TypeArranged.ARRANGED);
    }//GEN-LAST:event_rbArrangedAlphabetActionPerformed

    private void rbRandomAlphabetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbRandomAlphabetActionPerformed
        rbArrangedAlphabet.setSelected(false);
        rbReverseAlphabet.setSelected(false);
        rbRandomAlphabet.setSelected(true);
        configuration.setOrdering(CryptoConstants.TypeArranged.RANDOM);
    }//GEN-LAST:event_rbRandomAlphabetActionPerformed

    private void txtLoopsAlphabetFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtLoopsAlphabetFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLoopsAlphabetFocusLost

    private void txtLoopsAlphabetInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtLoopsAlphabetInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLoopsAlphabetInputMethodTextChanged

    private void txtLoopsAlphabetKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLoopsAlphabetKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLoopsAlphabetKeyPressed

    private void txtLoopsAlphabetKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLoopsAlphabetKeyReleased
        String input = txtLoopsAlphabet.getText();
        boolean isNumeric = NumberUtils.isParsable(input);
        if (!isNumeric && input.length() > 0) {
            new OnlyNumbersAlert(this, true).setVisible(true);
            txtLoopsAlphabet.requestFocus();
            return;
        }
        if (input.length() > 3) {
            txtLoopsAlphabet.setText(input.substring(0, input.length() - 1));
        }
    }//GEN-LAST:event_txtLoopsAlphabetKeyReleased

    private void txtLoopsAlphabetKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLoopsAlphabetKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLoopsAlphabetKeyTyped

    private void rbReverseAlphabetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbReverseAlphabetActionPerformed
        rbArrangedAlphabet.setSelected(false);
        rbReverseAlphabet.setSelected(true);
        rbRandomAlphabet.setSelected(false);
        configuration.setOrdering(CryptoConstants.TypeArranged.REVERSE);
    }//GEN-LAST:event_rbReverseAlphabetActionPerformed

    private void txtInitialCharacterFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtInitialCharacterFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInitialCharacterFocusLost

    private void txtInitialCharacterInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtInitialCharacterInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInitialCharacterInputMethodTextChanged

    private void txtInitialCharacterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInitialCharacterKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInitialCharacterKeyPressed

    private void txtInitialCharacterKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInitialCharacterKeyReleased
        String input = txtInitialCharacter.getText();
        if (input.length() > 1) {
            txtInitialCharacter.setText(input.substring(0, input.length() - 1));
        }
    }//GEN-LAST:event_txtInitialCharacterKeyReleased

    private void txtInitialCharacterKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInitialCharacterKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInitialCharacterKeyTyped

    private void txtInitialCharacterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtInitialCharacterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInitialCharacterActionPerformed

    private void jcbUseKeyPhraseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbUseKeyPhraseActionPerformed
        if (jcbUseKeyPhrase.isSelected()) {
            configuration.setUsePhraseKey(true);
        } else {
            configuration.setUsePhraseKey(false);
        }
    }//GEN-LAST:event_jcbUseKeyPhraseActionPerformed

    private void btnSaveConfigurationMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseEntered
        pnlSaveConfiguration.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnSaveConfigurationMouseEntered

    private void btnSaveConfigurationMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseExited
        pnlSaveConfiguration.setBackground(null);
    }//GEN-LAST:event_btnSaveConfigurationMouseExited

    private void usePhraseKey01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_usePhraseKey01MouseClicked
        new UsePhraseKeyHelper(this, true).setVisible(true);
    }//GEN-LAST:event_usePhraseKey01MouseClicked

    private void orderingAlphabet01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_orderingAlphabet01MouseClicked
        new OrderingAlphabetHelper(this, true).setVisible(true);
    }//GEN-LAST:event_orderingAlphabet01MouseClicked

    private void loopsAlphabet01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loopsAlphabet01MouseClicked
        new LoopsAlphabetHelper(this, true).setVisible(true);
    }//GEN-LAST:event_loopsAlphabet01MouseClicked

    private void initialCharacter01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_initialCharacter01MouseClicked
        new InitialCharacterHelper(this, true).setVisible(true);
    }//GEN-LAST:event_initialCharacter01MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GenerateAlphabetView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new GenerateAlphabetView(null, null).setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JButton btnSaveConfiguration;
    private javax.swing.JLabel initialCharacter01;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JCheckBox jcbUseKeyPhrase;
    private javax.swing.JPanel jpInitialGenAlphabet;
    private javax.swing.JLabel loopsAlphabet01;
    private javax.swing.JLabel orderingAlphabet01;
    private javax.swing.JPanel pnlClose;
    private javax.swing.JPanel pnlMinimize;
    private javax.swing.JPanel pnlSaveConfiguration;
    private javax.swing.JRadioButton rbArrangedAlphabet;
    private javax.swing.JRadioButton rbRandomAlphabet;
    private javax.swing.JRadioButton rbReverseAlphabet;
    private javax.swing.JTextField txtInitialCharacter;
    private javax.swing.JTextField txtLoopsAlphabet;
    private javax.swing.JLabel usePhraseKey01;
    // End of variables declaration//GEN-END:variables
}
