/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package views;

import com.sun.awt.AWTUtilities;
import java.awt.Color;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import logic.entity.CryptoConstants;
import logic.entity.CryptoNumbersSort;
import logic.entity.CryptoNumbersSortStructure;
import logic.entity.CryptoWeftConfiguration;
import org.apache.commons.lang3.math.NumberUtils;
import resources.alerts.DuplicateNumericKeyAlert;
import resources.alerts.MandatoryFieldAlert;
import resources.alerts.MandatoryMethodsCompleteAlert;
import resources.alerts.OnlyNumbersAlert;
import resources.helpers.NumericKeyHelper;

/**
 *
 * @author Gustavo Ramos M.
 */
public class WeftView extends javax.swing.JFrame {

    private int posX, posY;
    private final CryptoWeftConfiguration configuration;
    private final CustomizeView oView;
    private final CryptoConstants.TypeCrypto oType;
    private final WeftView mainView;
    private final String keyMethod;
    private final Color incomplete = new Color(153, 38, 0), complete = new Color(204, 82, 0);

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public WeftView(CustomizeView view, CryptoConstants.TypeCrypto type, String key) {
        initComponents();
        @SuppressWarnings("OverridableMethodCallInConstructor")
        Shape shape = new RoundRectangle2D.Double(0, 0, this.getBounds().width, this.getBounds().height, 15, 15);
        AWTUtilities.setWindowShape(this, shape);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/images/crypto_logo.png"));
        setIconImage(icon);
        oView = view;
        mainView = this;
        oType = type;
        keyMethod = key;
        configuration = new CryptoWeftConfiguration();
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setBackground(null);
        btnClose.setContentAreaFilled(false);
        pnlClose.setBackground(null);
        pnlSaveConfiguration.setBackground(null);
    }

    public static WeftView editConfigurationViewByParameters(CustomizeView view, CryptoConstants.TypeCrypto type, String methodKey, String numericKey,
            CryptoNumbersSort assignRestriction, CryptoNumbersSort sortRestriction, boolean useGlobalNumericKey) {
        WeftView wView = new WeftView(view, type, methodKey);
        wView.configuration.setIsConfigured(true);
        wView.configuration.setNumericKey(numericKey);
        wView.txtNumericKey.setText(numericKey);
        wView.configuration.setAssignRestriction(assignRestriction);
        wView.jplAssignRestriction.setBackground(new Color(204, 82, 0));
        wView.configuration.setSortRestriction(sortRestriction);
        wView.jplSortRestriction.setBackground(new Color(204, 82, 0));
        wView.configuration.setUseGlobalNumericKey(useGlobalNumericKey);
        wView.jcbUseGlobalNumericKey.setSelected(useGlobalNumericKey);
        return wView;
    }

    public void generateNumericKey(String numericKey) {
        txtNumericKey.setText(numericKey);
        configuration.setNumericKey(numericKey);
    }

    public void generateVariationNumber(CryptoConstants.WeftRestrictionNumber restriction, CryptoNumbersSort variation) {
        switch (restriction) {
            case ASSIGN:
                configuration.setAssignRestriction(variation);
                jplAssignRestriction.setBackground(complete);
                break;
            case SORT:
                configuration.setSortRestriction(variation);
                jplSortRestriction.setBackground(complete);
                break;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        pnlClose = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        pnlMinimize = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnMinimize = new javax.swing.JButton();
        pnlSaveConfiguration = new javax.swing.JPanel();
        pnlSaveConfiguration1 = new javax.swing.JPanel();
        btnSaveConfiguration = new javax.swing.JButton();
        jpSuccessiveBounces = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtNumericKey = new javax.swing.JTextField();
        pnlGenerateNumericKey = new javax.swing.JPanel();
        pnlSaveConfiguration3 = new javax.swing.JPanel();
        btnGenerateNumericKey = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jplAssignRestriction = new javax.swing.JPanel();
        jplSortRestriction = new javax.swing.JPanel();
        jcbUseGlobalNumericKey = new javax.swing.JCheckBox();
        numericKey01 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Personalizar");
        setBackground(new java.awt.Color(35, 86, 104));
        setName("Personalizar"); // NOI18N
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(139, 195, 74));
        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jPanel4.setForeground(new java.awt.Color(255, 173, 51));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(104, 159, 56));
        jPanel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel5MouseDragged(evt);
            }
        });
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel5MousePressed(evt);
            }
        });
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_icon_01.png"))); // NOI18N
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 0, 30, 30));

        jLabel9.setBackground(new java.awt.Color(104, 159, 56));
        jLabel9.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Tramas");
        jPanel5.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 910, 30));

        pnlClose.setBackground(new java.awt.Color(204, 0, 0));
        pnlClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pnlCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pnlCloseMouseExited(evt);
            }
        });
        pnlClose.setLayout(null);

        btnClose.setBackground(new java.awt.Color(104, 159, 56));
        btnClose.setFont(new java.awt.Font("Eras Light ITC", 1, 18)); // NOI18N
        btnClose.setForeground(new java.awt.Color(255, 255, 255));
        btnClose.setText("X");
        btnClose.setToolTipText("Cerrar");
        btnClose.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 39, 60), 0, true));
        btnClose.setName(""); // NOI18N
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        pnlClose.add(btnClose);
        btnClose.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 0, 50, 30));

        pnlMinimize.setBackground(new java.awt.Color(104, 159, 56));
        pnlMinimize.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(null);
        pnlMinimize.add(jPanel6);
        jPanel6.setBounds(660, 0, 50, 30);

        btnMinimize.setBackground(new java.awt.Color(104, 159, 56));
        btnMinimize.setFont(new java.awt.Font("Eras Light ITC", 0, 48)); // NOI18N
        btnMinimize.setForeground(new java.awt.Color(255, 255, 255));
        btnMinimize.setText("-");
        btnMinimize.setToolTipText("Minimizar");
        btnMinimize.setBorder(null);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited(evt);
            }
        });
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });
        pnlMinimize.add(btnMinimize);
        btnMinimize.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlMinimize, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 0, 50, 30));

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 30));

        pnlSaveConfiguration.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlSaveConfiguration1.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlSaveConfiguration.add(pnlSaveConfiguration1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 400, 180, 40));

        btnSaveConfiguration.setBackground(new java.awt.Color(139, 195, 74));
        btnSaveConfiguration.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnSaveConfiguration.setForeground(new java.awt.Color(242, 242, 242));
        btnSaveConfiguration.setText("GUARDAR CONFIGURACIÓN");
        btnSaveConfiguration.setToolTipText("");
        btnSaveConfiguration.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnSaveConfiguration.setContentAreaFilled(false);
        btnSaveConfiguration.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSaveConfiguration.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseExited(evt);
            }
        });
        btnSaveConfiguration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveConfigurationActionPerformed(evt);
            }
        });
        pnlSaveConfiguration.add(btnSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 180, 40));

        jPanel4.add(pnlSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 370, 180, 40));

        jpSuccessiveBounces.setBackground(new java.awt.Color(104, 159, 56));
        jpSuccessiveBounces.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jpSuccessiveBounces.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Restricción para ordenación");
        jpSuccessiveBounces.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, 170, 40));

        txtNumericKey.setBackground(new java.awt.Color(242, 242, 242));
        txtNumericKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtNumericKey.setForeground(new java.awt.Color(13, 13, 13));
        txtNumericKey.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNumericKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtNumericKey.setCaretColor(new java.awt.Color(13, 13, 13));
        txtNumericKey.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtNumericKey.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtNumericKey.setSelectionStart(1);
        txtNumericKey.setVerifyInputWhenFocusTarget(false);
        txtNumericKey.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNumericKeyFocusLost(evt);
            }
        });
        txtNumericKey.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                txtNumericKeyCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtNumericKeyInputMethodTextChanged(evt);
            }
        });
        txtNumericKey.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumericKeyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNumericKeyKeyTyped(evt);
            }
        });
        jpSuccessiveBounces.add(txtNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 60, 560, 40));

        pnlGenerateNumericKey.setBackground(new java.awt.Color(62, 94, 33));
        pnlGenerateNumericKey.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlSaveConfiguration3.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlGenerateNumericKey.add(pnlSaveConfiguration3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 400, 180, 40));

        btnGenerateNumericKey.setBackground(new java.awt.Color(104, 159, 56));
        btnGenerateNumericKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnGenerateNumericKey.setForeground(new java.awt.Color(242, 242, 242));
        btnGenerateNumericKey.setText("GENERAR");
        btnGenerateNumericKey.setToolTipText("");
        btnGenerateNumericKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        btnGenerateNumericKey.setContentAreaFilled(false);
        btnGenerateNumericKey.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGenerateNumericKey.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGenerateNumericKeyMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGenerateNumericKeyMouseExited(evt);
            }
        });
        btnGenerateNumericKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateNumericKeyActionPerformed(evt);
            }
        });
        pnlGenerateNumericKey.add(btnGenerateNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 40));

        jpSuccessiveBounces.add(pnlGenerateNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 60, 110, 40));

        jLabel7.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Clave numérica");
        jpSuccessiveBounces.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 120, 40));

        jLabel8.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Restricción para asignación");
        jpSuccessiveBounces.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, 170, 40));

        jplAssignRestriction.setBackground(new java.awt.Color(153, 38, 0));
        jplAssignRestriction.setForeground(new java.awt.Color(242, 242, 242));
        jplAssignRestriction.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jplAssignRestriction.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jplAssignRestrictionMouseClicked(evt);
            }
        });
        jplAssignRestriction.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jpSuccessiveBounces.add(jplAssignRestriction, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 120, 560, 50));

        jplSortRestriction.setBackground(new java.awt.Color(153, 38, 0));
        jplSortRestriction.setForeground(new java.awt.Color(242, 242, 242));
        jplSortRestriction.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jplSortRestriction.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jplSortRestrictionMouseClicked(evt);
            }
        });
        jplSortRestriction.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jpSuccessiveBounces.add(jplSortRestriction, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 190, 560, 50));

        jcbUseGlobalNumericKey.setBackground(new java.awt.Color(104, 159, 56));
        jcbUseGlobalNumericKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        jcbUseGlobalNumericKey.setForeground(new java.awt.Color(242, 242, 242));
        jcbUseGlobalNumericKey.setText("Usar clave numérica global");
        jcbUseGlobalNumericKey.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jcbUseGlobalNumericKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbUseGlobalNumericKeyActionPerformed(evt);
            }
        });
        jpSuccessiveBounces.add(jcbUseGlobalNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 20, 240, -1));

        numericKey01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        numericKey01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        numericKey01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                numericKey01MouseClicked(evt);
            }
        });
        jpSuccessiveBounces.add(numericKey01, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 65, -1, 30));

        jPanel4.add(jpSuccessiveBounces, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, 920, 270));

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 440));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseEntered
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnMinimizeMouseEntered

    private void btnMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseExited
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);
    }//GEN-LAST:event_btnMinimizeMouseExited

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        pnlClose.setVisible(true);
        pnlClose.setBackground(new Color(230, 0, 0));
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        pnlClose.setBackground(null);
        btnClose.setVisible(true);
    }//GEN-LAST:event_btnCloseMouseExited

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void pnlCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseEntered

    }//GEN-LAST:event_pnlCloseMouseEntered

    private void pnlCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseExited

    }//GEN-LAST:event_pnlCloseMouseExited

    private void jPanel5MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_jPanel5MouseDragged

    private void jPanel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_jPanel5MousePressed

    private void btnSaveConfigurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveConfigurationActionPerformed
        String field = txtNumericKey.getText();
        if (field.length() == 0) {
            String globalNumericKey = oView.getGlobalNumericKey();
            if (globalNumericKey == null || globalNumericKey.length() == 0) {
                new MandatoryFieldAlert(this, true, "clave numérica").setVisible(true);
                txtNumericKey.requestFocus();
                return;
            } else {
                configuration.setNumericKey(globalNumericKey);
            }
        }
        if (!configuration.getAssignRestriction().isIsConfigured()) {
            configuration.setAssignRestriction(new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                    new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null));
            /*new MandatoryMethodsCompleteAlert(this, true).setVisible(true);
            jplAssignRestriction.setBackground(new Color(153, 38, 0));
            return;*/
        }
        if (!configuration.getSortRestriction().isIsConfigured()) {
            configuration.setSortRestriction(new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                    new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null));
            /*new MandatoryMethodsCompleteAlert(this, true).setVisible(true);
            jplSortRestriction.setBackground(new Color(153, 38, 0));
            return;*/
        }
        oView.setWeftConfiguration(oType, keyMethod, configuration.getNumericKey(), configuration.getAssignRestriction(), configuration.getSortRestriction(),
                configuration.isUseGlobalNumericKey());
        dispose();
    }//GEN-LAST:event_btnSaveConfigurationActionPerformed

    private void btnSaveConfigurationMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseEntered
        pnlSaveConfiguration.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnSaveConfigurationMouseEntered

    private void btnSaveConfigurationMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseExited
        pnlSaveConfiguration.setBackground(null);
    }//GEN-LAST:event_btnSaveConfigurationMouseExited

    private void txtNumericKeyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNumericKeyFocusLost
        String input = txtNumericKey.getText(), separator = ",", lastLetter;
        if (input.length() > 0) {
            lastLetter = input.substring(input.length() - 1, input.length());
            if (lastLetter.equals(separator)) {
                txtNumericKey.setText(input.substring(0, input.length() - 1));
            }
        }
        String[] numbers = input.split(",");
        Set<String> setNumbers = new HashSet<>(Arrays.asList(numbers));
        if (numbers.length != setNumbers.size()) {
            new DuplicateNumericKeyAlert(this, true).setVisible(true);
            txtNumericKey.requestFocus();
        }
    }//GEN-LAST:event_txtNumericKeyFocusLost

    private void txtNumericKeyCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtNumericKeyCaretPositionChanged

    }//GEN-LAST:event_txtNumericKeyCaretPositionChanged

    private void txtNumericKeyInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtNumericKeyInputMethodTextChanged

    }//GEN-LAST:event_txtNumericKeyInputMethodTextChanged

    private void txtNumericKeyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumericKeyKeyReleased
        String input = txtNumericKey.getText();
        if (input.length() > 0) {
            String[] numbers = input.split(",");
            boolean isNumeric;
            for (String number : numbers) {
                isNumeric = NumberUtils.isParsable(number);
                if (!isNumeric) {
                    new OnlyNumbersAlert(this, true).setVisible(true);
                    txtNumericKey.requestFocus();
                    return;
                }
            }
        }
    }//GEN-LAST:event_txtNumericKeyKeyReleased

    private void txtNumericKeyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumericKeyKeyTyped

    }//GEN-LAST:event_txtNumericKeyKeyTyped

    private void btnGenerateNumericKeyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeyMouseEntered
        pnlGenerateNumericKey.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnGenerateNumericKeyMouseEntered

    private void btnGenerateNumericKeyMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeyMouseExited
        pnlGenerateNumericKey.setBackground(null);
    }//GEN-LAST:event_btnGenerateNumericKeyMouseExited

    private void btnGenerateNumericKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeyActionPerformed
        new GenerateNumericKeyView(mainView).setVisible(true);
    }//GEN-LAST:event_btnGenerateNumericKeyActionPerformed

    private void jplAssignRestrictionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jplAssignRestrictionMouseClicked
        if (configuration.getAssignRestriction().isIsConfigured()) {
            VariationNumericKeyView.editConfigurationViewByParameters(mainView, CryptoConstants.WeftRestrictionNumber.ASSIGN, configuration.getAssignRestriction()).setVisible(true);
        } else {
            new VariationNumericKeyView(mainView, CryptoConstants.WeftRestrictionNumber.ASSIGN).setVisible(true);
        }
    }//GEN-LAST:event_jplAssignRestrictionMouseClicked

    private void jplSortRestrictionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jplSortRestrictionMouseClicked
        if (configuration.getSortRestriction().isIsConfigured()) {
            VariationNumericKeyView.editConfigurationViewByParameters(mainView, CryptoConstants.WeftRestrictionNumber.SORT, configuration.getSortRestriction()).setVisible(true);
        } else {
            new VariationNumericKeyView(mainView, CryptoConstants.WeftRestrictionNumber.SORT).setVisible(true);
        }
    }//GEN-LAST:event_jplSortRestrictionMouseClicked

    private void jcbUseGlobalNumericKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbUseGlobalNumericKeyActionPerformed
        if (jcbUseGlobalNumericKey.isSelected()) {
            String key = oView.getGlobalNumericKey();
            if (key == null) {
                new MandatoryFieldAlert(this, true, "clave global").setVisible(true);
                jcbUseGlobalNumericKey.setSelected(false);
                return;
            }
            configuration.setUseGlobalNumericKey(true);
            txtNumericKey.setText(key);
            configuration.setNumericKey(key);
        } else {
            configuration.setUseGlobalNumericKey(false);
        }
    }//GEN-LAST:event_jcbUseGlobalNumericKeyActionPerformed

    private void numericKey01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_numericKey01MouseClicked
        new NumericKeyHelper(this, true).setVisible(true);
    }//GEN-LAST:event_numericKey01MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WeftView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new WeftView(null, null, null).setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnGenerateNumericKey;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JButton btnSaveConfiguration;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JCheckBox jcbUseGlobalNumericKey;
    private javax.swing.JPanel jpSuccessiveBounces;
    private javax.swing.JPanel jplAssignRestriction;
    private javax.swing.JPanel jplSortRestriction;
    private javax.swing.JLabel numericKey01;
    private javax.swing.JPanel pnlClose;
    private javax.swing.JPanel pnlGenerateNumericKey;
    private javax.swing.JPanel pnlMinimize;
    private javax.swing.JPanel pnlSaveConfiguration;
    private javax.swing.JPanel pnlSaveConfiguration1;
    private javax.swing.JPanel pnlSaveConfiguration3;
    private javax.swing.JTextField txtNumericKey;
    // End of variables declaration//GEN-END:variables
}
