/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package views;

import com.sun.awt.AWTUtilities;
import java.awt.Color;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import logic.actions.CryptoGeneratorAction;
import logic.exception.CryptoGlobalException;
import resources.alerts.MandatoryFieldToDecryptAlert;

/**
 *
 * @author Gustavo Ramos M.
 */
public class DecodeView extends javax.swing.JFrame {

    private int posX, posY;

    public DecodeView() {
        initComponents();
        this.setLocationRelativeTo(null);
        Shape shape = new RoundRectangle2D.Double(0, 0, this.getBounds().width, this.getBounds().height, 15, 15);
        AWTUtilities.setWindowShape(this, shape);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/images/crypto_logo.png"));
        setIconImage(icon);
        txaDecode.requestFocus();
        btnMinimize.setContentAreaFilled(false);
        btnClose.setContentAreaFilled(false);
        btnGoToBack.setContentAreaFilled(false);
        btnDecode.setContentAreaFilled(false);
        btnClear.setContentAreaFilled(false);
        pnlClose.setVisible(true);
        pnlClose.setBackground(null);
        pnlMiniminze.setVisible(true);
        pnlMiniminze.setBackground(null);
        lblGoToEncode.setForeground(new Color(13, 13, 13));
        lblGoToBack.setForeground(new Color(13, 13, 13));
        pnlDecode.setBackground(null);
        pnlClear.setBackground(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        pnlClose = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        pnlMiniminze = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnMinimize = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txaDecode = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        txaDecoded = new javax.swing.JTextArea();
        btnGoToBack = new javax.swing.JButton();
        lblGoToBack = new javax.swing.JLabel();
        pnlDecode = new javax.swing.JPanel();
        btnDecode = new javax.swing.JButton();
        pnlClear = new javax.swing.JPanel();
        btnClear = new javax.swing.JButton();
        lblGoToEncode = new javax.swing.JLabel();
        btnGoToEncode = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(139, 195, 74));
        jPanel1.setForeground(new java.awt.Color(13, 13, 13));
        jPanel1.setLayout(null);

        jPanel3.setBackground(new java.awt.Color(104, 159, 56));
        jPanel3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel3MouseDragged(evt);
            }
        });
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel3MousePressed(evt);
            }
        });
        jPanel3.setLayout(null);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_icon_01.png"))); // NOI18N
        jPanel3.add(jLabel4);
        jLabel4.setBounds(5, 0, 30, 30);

        pnlClose.setBackground(new java.awt.Color(204, 0, 0));
        pnlClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pnlCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pnlCloseMouseExited(evt);
            }
        });
        pnlClose.setLayout(null);

        btnClose.setBackground(new java.awt.Color(104, 159, 56));
        btnClose.setFont(new java.awt.Font("Eras Light ITC", 1, 18)); // NOI18N
        btnClose.setForeground(new java.awt.Color(255, 255, 255));
        btnClose.setText("X");
        btnClose.setToolTipText("Cerrar");
        btnClose.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 39, 60), 0, true));
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        pnlClose.add(btnClose);
        btnClose.setBounds(0, 0, 50, 30);

        jPanel3.add(pnlClose);
        pnlClose.setBounds(950, 0, 50, 30);

        pnlMiniminze.setBackground(new java.awt.Color(204, 204, 204));
        pnlMiniminze.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(null);
        pnlMiniminze.add(jPanel6);
        jPanel6.setBounds(660, 0, 50, 30);

        btnMinimize.setBackground(new java.awt.Color(104, 159, 56));
        btnMinimize.setFont(new java.awt.Font("Eras Light ITC", 0, 48)); // NOI18N
        btnMinimize.setForeground(new java.awt.Color(255, 255, 255));
        btnMinimize.setText("-");
        btnMinimize.setToolTipText("Minimizar");
        btnMinimize.setBorder(null);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited(evt);
            }
        });
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });
        pnlMiniminze.add(btnMinimize);
        btnMinimize.setBounds(0, 0, 50, 30);

        jPanel3.add(pnlMiniminze);
        pnlMiniminze.setBounds(900, 0, 50, 30);

        jLabel5.setBackground(new java.awt.Color(104, 159, 56));
        jLabel5.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Descifrar");
        jPanel3.add(jLabel5);
        jLabel5.setBounds(0, 0, 1000, 30);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(0, 0, 1000, 30);

        jLabel2.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 20)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Ingrese el mensaje que desea descifrar");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(90, 90, 370, 40);

        jLabel1.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 20)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Este es el mensaje descifrado");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(90, 350, 340, 40);

        txaDecode.setBackground(new java.awt.Color(242, 242, 242));
        txaDecode.setColumns(20);
        txaDecode.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 18)); // NOI18N
        txaDecode.setForeground(new java.awt.Color(13, 13, 13));
        txaDecode.setLineWrap(true);
        txaDecode.setRows(5);
        txaDecode.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(104, 159, 56), new java.awt.Color(104, 159, 56), new java.awt.Color(104, 159, 56), new java.awt.Color(104, 159, 56)));
        txaDecode.setCaretColor(new java.awt.Color(13, 13, 13));
        txaDecode.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txaDecode.setSelectionColor(new java.awt.Color(139, 195, 74));
        jScrollPane2.setViewportView(txaDecode);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(90, 130, 830, 140);

        txaDecoded.setEditable(false);
        txaDecoded.setBackground(new java.awt.Color(204, 204, 204));
        txaDecoded.setColumns(20);
        txaDecoded.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 18)); // NOI18N
        txaDecoded.setForeground(new java.awt.Color(13, 13, 13));
        txaDecoded.setLineWrap(true);
        txaDecoded.setRows(5);
        txaDecoded.setBorder(null);
        txaDecoded.setCaretColor(new java.awt.Color(13, 13, 13));
        txaDecoded.setSelectionColor(new java.awt.Color(139, 195, 74));
        jScrollPane1.setViewportView(txaDecoded);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(90, 390, 820, 150);

        btnGoToBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/if_Left_arrow_2202280.png"))); // NOI18N
        btnGoToBack.setBorder(null);
        btnGoToBack.setContentAreaFilled(false);
        btnGoToBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGoToBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGoToBackMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGoToBackMouseExited(evt);
            }
        });
        btnGoToBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoToBackActionPerformed(evt);
            }
        });
        jPanel1.add(btnGoToBack);
        btnGoToBack.setBounds(10, 40, 50, 30);

        lblGoToBack.setBackground(new java.awt.Color(13, 13, 13));
        lblGoToBack.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 16)); // NOI18N
        lblGoToBack.setForeground(new java.awt.Color(255, 255, 255));
        lblGoToBack.setText("Ir a Inicio");
        lblGoToBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblGoToBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblGoToBackMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblGoToBackMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblGoToBackMouseReleased(evt);
            }
        });
        jPanel1.add(lblGoToBack);
        lblGoToBack.setBounds(40, 40, 90, 30);

        pnlDecode.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnDecode.setBackground(new java.awt.Color(139, 195, 74));
        btnDecode.setFont(new java.awt.Font("Microsoft JhengHei Light", 0, 18)); // NOI18N
        btnDecode.setForeground(new java.awt.Color(255, 255, 255));
        btnDecode.setText("Descifrar Mensaje");
        btnDecode.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnDecode.setContentAreaFilled(false);
        btnDecode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDecode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDecodeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDecodeMouseExited(evt);
            }
        });
        btnDecode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDecodeActionPerformed(evt);
            }
        });
        pnlDecode.add(btnDecode, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 170, 40));

        jPanel1.add(pnlDecode);
        pnlDecode.setBounds(420, 290, 170, 40);

        btnClear.setBackground(new java.awt.Color(139, 195, 74));
        btnClear.setFont(new java.awt.Font("Microsoft JhengHei Light", 0, 18)); // NOI18N
        btnClear.setForeground(new java.awt.Color(255, 255, 255));
        btnClear.setText("Limpiar Campos");
        btnClear.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnClear.setContentAreaFilled(false);
        btnClear.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnClear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnClearMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnClearMouseExited(evt);
            }
        });
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlClearLayout = new javax.swing.GroupLayout(pnlClear);
        pnlClear.setLayout(pnlClearLayout);
        pnlClearLayout.setHorizontalGroup(
            pnlClearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlClearLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnlClearLayout.setVerticalGroup(
            pnlClearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlClearLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(pnlClear);
        pnlClear.setBounds(430, 560, 170, 40);

        lblGoToEncode.setBackground(new java.awt.Color(13, 13, 13));
        lblGoToEncode.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 16)); // NOI18N
        lblGoToEncode.setForeground(new java.awt.Color(255, 255, 255));
        lblGoToEncode.setText("Ir a cifrar");
        lblGoToEncode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblGoToEncode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblGoToEncodeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblGoToEncodeMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblGoToEncodeMouseReleased(evt);
            }
        });
        jPanel1.add(lblGoToEncode);
        lblGoToEncode.setBounds(880, 40, 80, 30);

        btnGoToEncode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/if_Right_arrow_2202241.png"))); // NOI18N
        btnGoToEncode.setBorder(null);
        btnGoToEncode.setContentAreaFilled(false);
        btnGoToEncode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGoToEncode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoToEncodeActionPerformed(evt);
            }
        });
        jPanel1.add(btnGoToEncode);
        btnGoToEncode.setBounds(940, 40, 50, 30);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 996, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 630, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        pnlClose.setVisible(true);
        pnlClose.setBackground(new Color(230, 0, 0));
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        pnlClose.setBackground(null);
        btnClose.setVisible(true);
    }//GEN-LAST:event_btnCloseMouseExited

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnCloseActionPerformed

    private void pnlCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseEntered

    }//GEN-LAST:event_pnlCloseMouseEntered

    private void pnlCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseExited

    }//GEN-LAST:event_pnlCloseMouseExited

    private void btnMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseEntered
        btnMinimize.setContentAreaFilled(false);
        pnlMiniminze.setVisible(true);
        pnlMiniminze.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnMinimizeMouseEntered

    private void btnMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseExited
        btnMinimize.setContentAreaFilled(false);
        pnlMiniminze.setVisible(true);
        pnlMiniminze.setBackground(null);
    }//GEN-LAST:event_btnMinimizeMouseExited

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void jPanel3MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_jPanel3MouseDragged

    private void jPanel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_jPanel3MousePressed

    private void btnGoToBackMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToBackMouseEntered
        lblGoToBack.setForeground(new Color(242, 242, 242));
    }//GEN-LAST:event_btnGoToBackMouseEntered

    private void btnGoToBackMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToBackMouseExited
        lblGoToBack.setForeground(new Color(13, 13, 13));
    }//GEN-LAST:event_btnGoToBackMouseExited

    private void btnGoToBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoToBackActionPerformed
        try {
            new MainView().setVisible(true);
        } catch (CryptoGlobalException ex) {
            Logger.getLogger(DecodeView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_btnGoToBackActionPerformed

    private void lblGoToBackMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToBackMouseReleased
        try {
            new MainView().setVisible(true);
        } catch (CryptoGlobalException ex) {
            Logger.getLogger(DecodeView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_lblGoToBackMouseReleased

    private void lblGoToBackMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToBackMouseEntered
        lblGoToBack.setForeground(new Color(242, 242, 242));
    }//GEN-LAST:event_lblGoToBackMouseEntered

    private void lblGoToBackMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToBackMouseExited
        lblGoToBack.setForeground(new Color(13, 13, 13));
    }//GEN-LAST:event_lblGoToBackMouseExited

    private void btnDecodeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDecodeMouseEntered
        pnlDecode.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnDecodeMouseEntered

    private void btnClearMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnClearMouseEntered
        pnlClear.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnClearMouseEntered

    private void btnDecodeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDecodeMouseExited
        btnDecode.setForeground(new Color(242, 242, 242));
        pnlDecode.setBackground(null);
    }//GEN-LAST:event_btnDecodeMouseExited

    private void btnClearMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnClearMouseExited
        pnlClear.setBackground(null);
    }//GEN-LAST:event_btnClearMouseExited

    private void lblGoToEncodeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToEncodeMouseEntered
        lblGoToEncode.setForeground(new Color(242, 242, 242));
    }//GEN-LAST:event_lblGoToEncodeMouseEntered

    private void lblGoToEncodeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToEncodeMouseExited
        lblGoToEncode.setForeground(new Color(13, 13, 13));
    }//GEN-LAST:event_lblGoToEncodeMouseExited

    private void lblGoToEncodeMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToEncodeMouseReleased
        new EncodeView().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_lblGoToEncodeMouseReleased

    private void btnGoToEncodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoToEncodeActionPerformed
        new EncodeView().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnGoToEncodeActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        txaDecoded.setText("");
        txaDecode.setText("");
        txaDecode.requestFocus();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnDecodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDecodeActionPerformed
        String input = txaDecode.getText();
        if(input.length() > 0){
           String dencrypted = CryptoGeneratorAction.decryptText(input);
           txaDecoded.setText(dencrypted);
        } else {
            new MandatoryFieldToDecryptAlert(this, true).setVisible(true);
            txaDecode.requestFocus();
        }
    }//GEN-LAST:event_btnDecodeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DecodeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new DecodeView().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnDecode;
    private javax.swing.JButton btnGoToBack;
    private javax.swing.JButton btnGoToEncode;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblGoToBack;
    private javax.swing.JLabel lblGoToEncode;
    private javax.swing.JPanel pnlClear;
    private javax.swing.JPanel pnlClose;
    private javax.swing.JPanel pnlDecode;
    private javax.swing.JPanel pnlMiniminze;
    private javax.swing.JTextArea txaDecode;
    private javax.swing.JTextArea txaDecoded;
    // End of variables declaration//GEN-END:variables
}
