/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package views;

import com.sun.awt.AWTUtilities;
import java.awt.Color;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import logic.entity.CryptoConstants;
import logic.entity.CryptoSucBouncesConfiguration;
import org.apache.commons.lang3.math.NumberUtils;
import resources.alerts.DuplicateNumericKeyAlert;
import resources.alerts.MandatoryFieldAlert;
import resources.alerts.OnlyNumbersAlert;
import resources.helpers.DirectionBouncesConfigHelper;
import resources.helpers.DirectionBouncesHelper;
import resources.helpers.NumericKeyHelper;

/**
 *
 * @author Gustavo Ramos M.
 */
public class SuccessiveBouncesView extends javax.swing.JFrame {
    
    private int posX, posY;
    private final CryptoSucBouncesConfiguration configuration;
    private final CustomizeView oView;
    private final CryptoConstants.TypeCrypto oType;
    private final SuccessiveBouncesView mainView;
    private final String keyMethod;
    
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public SuccessiveBouncesView(CustomizeView view, CryptoConstants.TypeCrypto type, String key) {
        initComponents();
        @SuppressWarnings("OverridableMethodCallInConstructor")
        Shape shape = new RoundRectangle2D.Double(0, 0, this.getBounds().width, this.getBounds().height, 15, 15);
        AWTUtilities.setWindowShape(this, shape);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/images/crypto_logo.png"));
        setIconImage(icon);
        oView = view;
        mainView = this;
        oType = type;
        keyMethod = key;
        configuration = new CryptoSucBouncesConfiguration();
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setBackground(null);
        btnClose.setContentAreaFilled(false);
        pnlClose.setBackground(null);
        pnlSaveConfiguration.setBackground(null);
    }
    
    public static SuccessiveBouncesView editConfigurationViewByParameters(CustomizeView view, CryptoConstants.TypeCrypto type, String methodKey, String numericKey, String directionKey,
            String initialCharacter, boolean initial, boolean useGlobalNumericKey) {
        SuccessiveBouncesView sbView = new SuccessiveBouncesView(view, type, methodKey);
        sbView.configuration.setNumericKey(numericKey);
        sbView.txtNumericKey.setText(numericKey);
        sbView.configuration.setDirectionKey(directionKey);
        sbView.txtDirectionKey.setText(directionKey);
        sbView.configuration.setInitialCharacter(initialCharacter);
        sbView.txtInitialCharacter.setText((initialCharacter == null) ? "" : initialCharacter);
        sbView.configuration.setInitial(initial);
        sbView.jcbRestrictionStart.setSelected(initial);
        sbView.configuration.setUseGlobalNumericKey(useGlobalNumericKey);
        sbView.jcbUseGlobalNumericKey.setSelected(useGlobalNumericKey);
        return sbView;
    }
    
    public void generateNumericKey(String numericKey) {
        txtNumericKey.setText(numericKey);
        configuration.setNumericKey(numericKey);
    }
    
    public void generateDirectionKey(String directionKey) {
        txtDirectionKey.setText(directionKey);
        configuration.setDirectionKey(directionKey);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        pnlClose = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        pnlMinimize = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnMinimize = new javax.swing.JButton();
        pnlSaveConfiguration = new javax.swing.JPanel();
        pnlSaveConfiguration1 = new javax.swing.JPanel();
        btnSaveConfiguration = new javax.swing.JButton();
        jpSuccessiveBounces = new javax.swing.JPanel();
        txtInitialCharacter = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jcbRestrictionStart = new javax.swing.JCheckBox();
        jLabel5 = new javax.swing.JLabel();
        txtNumericKey = new javax.swing.JTextField();
        pnlGenerateNumericKey = new javax.swing.JPanel();
        pnlSaveConfiguration3 = new javax.swing.JPanel();
        btnGenerateNumericKey = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtDirectionKey = new javax.swing.JTextField();
        pnlGenerateDirectionKey = new javax.swing.JPanel();
        pnlSaveConfiguration4 = new javax.swing.JPanel();
        btnGenerateDirectionKey = new javax.swing.JButton();
        jcbUseGlobalNumericKey = new javax.swing.JCheckBox();
        numericKey01 = new javax.swing.JLabel();
        directionBounces01 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Personalizar");
        setBackground(new java.awt.Color(35, 86, 104));
        setName("Personalizar"); // NOI18N
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(139, 195, 74));
        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jPanel4.setForeground(new java.awt.Color(255, 173, 51));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(104, 159, 56));
        jPanel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel5MouseDragged(evt);
            }
        });
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel5MousePressed(evt);
            }
        });
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_icon_01.png"))); // NOI18N
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 0, 30, 30));

        jLabel9.setBackground(new java.awt.Color(104, 159, 56));
        jLabel9.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Saltos Sucesivos");
        jPanel5.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 760, 30));

        pnlClose.setBackground(new java.awt.Color(204, 0, 0));
        pnlClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pnlCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pnlCloseMouseExited(evt);
            }
        });
        pnlClose.setLayout(null);

        btnClose.setBackground(new java.awt.Color(104, 159, 56));
        btnClose.setFont(new java.awt.Font("Eras Light ITC", 1, 18)); // NOI18N
        btnClose.setForeground(new java.awt.Color(255, 255, 255));
        btnClose.setText("X");
        btnClose.setToolTipText("Cerrar");
        btnClose.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 39, 60), 0, true));
        btnClose.setName(""); // NOI18N
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        pnlClose.add(btnClose);
        btnClose.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 0, 50, 30));

        pnlMinimize.setBackground(new java.awt.Color(104, 159, 56));
        pnlMinimize.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(null);
        pnlMinimize.add(jPanel6);
        jPanel6.setBounds(660, 0, 50, 30);

        btnMinimize.setBackground(new java.awt.Color(104, 159, 56));
        btnMinimize.setFont(new java.awt.Font("Eras Light ITC", 0, 48)); // NOI18N
        btnMinimize.setForeground(new java.awt.Color(255, 255, 255));
        btnMinimize.setText("-");
        btnMinimize.setToolTipText("Minimizar");
        btnMinimize.setBorder(null);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited(evt);
            }
        });
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });
        pnlMinimize.add(btnMinimize);
        btnMinimize.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlMinimize, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 0, 50, 30));

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 850, 30));

        pnlSaveConfiguration.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlSaveConfiguration1.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlSaveConfiguration.add(pnlSaveConfiguration1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 400, 180, 40));

        btnSaveConfiguration.setBackground(new java.awt.Color(139, 195, 74));
        btnSaveConfiguration.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnSaveConfiguration.setForeground(new java.awt.Color(242, 242, 242));
        btnSaveConfiguration.setText("GUARDAR CONFIGURACIÓN");
        btnSaveConfiguration.setToolTipText("");
        btnSaveConfiguration.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnSaveConfiguration.setContentAreaFilled(false);
        btnSaveConfiguration.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSaveConfiguration.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseExited(evt);
            }
        });
        btnSaveConfiguration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveConfigurationActionPerformed(evt);
            }
        });
        pnlSaveConfiguration.add(btnSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 180, 40));

        jPanel4.add(pnlSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 310, 180, 40));

        jpSuccessiveBounces.setBackground(new java.awt.Color(104, 159, 56));
        jpSuccessiveBounces.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jpSuccessiveBounces.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtInitialCharacter.setBackground(new java.awt.Color(242, 242, 242));
        txtInitialCharacter.setColumns(1);
        txtInitialCharacter.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtInitialCharacter.setForeground(new java.awt.Color(13, 13, 13));
        txtInitialCharacter.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtInitialCharacter.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtInitialCharacter.setCaretColor(new java.awt.Color(13, 13, 13));
        txtInitialCharacter.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtInitialCharacter.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtInitialCharacter.setSelectionStart(1);
        txtInitialCharacter.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtInitialCharacterFocusLost(evt);
            }
        });
        txtInitialCharacter.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtInitialCharacterInputMethodTextChanged(evt);
            }
        });
        txtInitialCharacter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtInitialCharacterActionPerformed(evt);
            }
        });
        txtInitialCharacter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtInitialCharacterKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtInitialCharacterKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtInitialCharacterKeyTyped(evt);
            }
        });
        jpSuccessiveBounces.add(txtInitialCharacter, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 30, 40, 30));

        jLabel21.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel21.setText("Caracter inicial");
        jpSuccessiveBounces.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 30, 100, 30));

        jcbRestrictionStart.setBackground(new java.awt.Color(104, 159, 56));
        jcbRestrictionStart.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        jcbRestrictionStart.setForeground(new java.awt.Color(242, 242, 242));
        jcbRestrictionStart.setText("Inicia");
        jcbRestrictionStart.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jcbRestrictionStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbRestrictionStartActionPerformed(evt);
            }
        });
        jpSuccessiveBounces.add(jcbRestrictionStart, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 33, 110, -1));

        jLabel5.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Dirección");
        jpSuccessiveBounces.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 60, 40));

        txtNumericKey.setBackground(new java.awt.Color(242, 242, 242));
        txtNumericKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtNumericKey.setForeground(new java.awt.Color(13, 13, 13));
        txtNumericKey.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNumericKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtNumericKey.setCaretColor(new java.awt.Color(13, 13, 13));
        txtNumericKey.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtNumericKey.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtNumericKey.setSelectionStart(1);
        txtNumericKey.setVerifyInputWhenFocusTarget(false);
        txtNumericKey.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNumericKeyFocusLost(evt);
            }
        });
        txtNumericKey.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                txtNumericKeyCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtNumericKeyInputMethodTextChanged(evt);
            }
        });
        txtNumericKey.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumericKeyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNumericKeyKeyTyped(evt);
            }
        });
        jpSuccessiveBounces.add(txtNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 80, 450, 40));

        pnlGenerateNumericKey.setBackground(new java.awt.Color(62, 94, 33));
        pnlGenerateNumericKey.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlSaveConfiguration3.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlGenerateNumericKey.add(pnlSaveConfiguration3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 400, 180, 40));

        btnGenerateNumericKey.setBackground(new java.awt.Color(104, 159, 56));
        btnGenerateNumericKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnGenerateNumericKey.setForeground(new java.awt.Color(242, 242, 242));
        btnGenerateNumericKey.setText("GENERAR");
        btnGenerateNumericKey.setToolTipText("");
        btnGenerateNumericKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        btnGenerateNumericKey.setContentAreaFilled(false);
        btnGenerateNumericKey.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGenerateNumericKey.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGenerateNumericKeyMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGenerateNumericKeyMouseExited(evt);
            }
        });
        btnGenerateNumericKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateNumericKeyActionPerformed(evt);
            }
        });
        pnlGenerateNumericKey.add(btnGenerateNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 40));

        jpSuccessiveBounces.add(pnlGenerateNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 80, 110, 40));

        jLabel7.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Clave numérica");
        jpSuccessiveBounces.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 100, 40));

        txtDirectionKey.setBackground(new java.awt.Color(242, 242, 242));
        txtDirectionKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtDirectionKey.setForeground(new java.awt.Color(13, 13, 13));
        txtDirectionKey.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDirectionKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtDirectionKey.setCaretColor(new java.awt.Color(13, 13, 13));
        txtDirectionKey.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtDirectionKey.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtDirectionKey.setSelectionStart(1);
        txtDirectionKey.setVerifyInputWhenFocusTarget(false);
        txtDirectionKey.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDirectionKeyFocusLost(evt);
            }
        });
        txtDirectionKey.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                txtDirectionKeyCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtDirectionKeyInputMethodTextChanged(evt);
            }
        });
        txtDirectionKey.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDirectionKeyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDirectionKeyKeyTyped(evt);
            }
        });
        jpSuccessiveBounces.add(txtDirectionKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 140, 450, 40));

        pnlGenerateDirectionKey.setBackground(new java.awt.Color(62, 94, 33));
        pnlGenerateDirectionKey.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlSaveConfiguration4.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlGenerateDirectionKey.add(pnlSaveConfiguration4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 400, 180, 40));

        btnGenerateDirectionKey.setBackground(new java.awt.Color(104, 159, 56));
        btnGenerateDirectionKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnGenerateDirectionKey.setForeground(new java.awt.Color(242, 242, 242));
        btnGenerateDirectionKey.setText("GENERAR");
        btnGenerateDirectionKey.setToolTipText("");
        btnGenerateDirectionKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        btnGenerateDirectionKey.setContentAreaFilled(false);
        btnGenerateDirectionKey.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGenerateDirectionKey.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGenerateDirectionKeyMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGenerateDirectionKeyMouseExited(evt);
            }
        });
        btnGenerateDirectionKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateDirectionKeyActionPerformed(evt);
            }
        });
        pnlGenerateDirectionKey.add(btnGenerateDirectionKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 40));

        jpSuccessiveBounces.add(pnlGenerateDirectionKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 140, 110, 40));

        jcbUseGlobalNumericKey.setBackground(new java.awt.Color(104, 159, 56));
        jcbUseGlobalNumericKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        jcbUseGlobalNumericKey.setForeground(new java.awt.Color(242, 242, 242));
        jcbUseGlobalNumericKey.setText("Usar clave numérica global");
        jcbUseGlobalNumericKey.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jcbUseGlobalNumericKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbUseGlobalNumericKeyActionPerformed(evt);
            }
        });
        jpSuccessiveBounces.add(jcbUseGlobalNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 33, 230, -1));

        numericKey01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        numericKey01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        numericKey01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                numericKey01MouseClicked(evt);
            }
        });
        jpSuccessiveBounces.add(numericKey01, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 85, -1, 30));

        directionBounces01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        directionBounces01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        directionBounces01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                directionBounces01MouseClicked(evt);
            }
        });
        jpSuccessiveBounces.add(directionBounces01, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 145, -1, 30));

        jPanel4.add(jpSuccessiveBounces, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, 770, 210));

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 850, 380));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseEntered
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnMinimizeMouseEntered

    private void btnMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseExited
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);
    }//GEN-LAST:event_btnMinimizeMouseExited

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        pnlClose.setVisible(true);
        pnlClose.setBackground(new Color(230, 0, 0));
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        pnlClose.setBackground(null);
        btnClose.setVisible(true);
    }//GEN-LAST:event_btnCloseMouseExited

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void pnlCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseEntered

    }//GEN-LAST:event_pnlCloseMouseEntered

    private void pnlCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseExited

    }//GEN-LAST:event_pnlCloseMouseExited

    private void jPanel5MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_jPanel5MouseDragged

    private void jPanel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_jPanel5MousePressed

    private void btnSaveConfigurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveConfigurationActionPerformed
        String field = txtNumericKey.getText();
        if (field.length() == 0) {
            String globalNumericKey = oView.getGlobalNumericKey();
            if (globalNumericKey == null || globalNumericKey.length() == 0) {
                new MandatoryFieldAlert(this, true, "clave numérica").setVisible(true);
                txtNumericKey.requestFocus();
                return;
            } else {
                configuration.setNumericKey(globalNumericKey);
            }
        }
        field = txtDirectionKey.getText();
        if (field.length() == 0) {
            txtDirectionKey.setText("I,D,I,I,D");
            configuration.setDirectionKey(txtDirectionKey.getText());
            /*new MandatoryFieldAlert(this, true, "dirección").setVisible(true);
            txtDirectionKey.requestFocus();
            return;*/
        }
        configuration.setInitialCharacter((txtInitialCharacter.getText().length() == 0) ? null : txtInitialCharacter.getText());
        oView.setSucBouncesConfiguration(oType, keyMethod, configuration.getNumericKey(), configuration.getDirectionKey(), configuration.getInitialCharacter(),
                configuration.isInitial(), configuration.isUseGlobalNumericKey());
        dispose();
    }//GEN-LAST:event_btnSaveConfigurationActionPerformed

    private void txtInitialCharacterFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtInitialCharacterFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInitialCharacterFocusLost

    private void txtInitialCharacterInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtInitialCharacterInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInitialCharacterInputMethodTextChanged

    private void txtInitialCharacterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInitialCharacterKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInitialCharacterKeyPressed

    private void txtInitialCharacterKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInitialCharacterKeyReleased
        String input = txtInitialCharacter.getText();
        if (input.length() > 1) {
            txtInitialCharacter.setText(input.substring(0, input.length() - 1));
        }
    }//GEN-LAST:event_txtInitialCharacterKeyReleased

    private void txtInitialCharacterKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInitialCharacterKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInitialCharacterKeyTyped

    private void txtInitialCharacterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtInitialCharacterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInitialCharacterActionPerformed

    private void jcbRestrictionStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbRestrictionStartActionPerformed
        if (jcbRestrictionStart.isSelected()) {
            configuration.setInitial(true);
        } else {
            configuration.setInitial(false);
        }
    }//GEN-LAST:event_jcbRestrictionStartActionPerformed

    private void btnSaveConfigurationMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseEntered
        pnlSaveConfiguration.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnSaveConfigurationMouseEntered

    private void btnSaveConfigurationMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseExited
        pnlSaveConfiguration.setBackground(null);
    }//GEN-LAST:event_btnSaveConfigurationMouseExited

    private void txtNumericKeyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNumericKeyFocusLost
        String input = txtNumericKey.getText(), separator = ",", lastLetter;
        if (input.length() > 0) {
            lastLetter = input.substring(input.length() - 1, input.length());
            if (lastLetter.equals(separator)) {
                txtNumericKey.setText(input.substring(0, input.length() - 1));
            }
        }
        String[] numbers = input.split(",");
        Set<String> setNumbers = new HashSet<>(Arrays.asList(numbers));
        if (numbers.length != setNumbers.size()) {
            new DuplicateNumericKeyAlert(this, true).setVisible(true);
            txtNumericKey.requestFocus();
        }
    }//GEN-LAST:event_txtNumericKeyFocusLost

    private void txtNumericKeyCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtNumericKeyCaretPositionChanged

    }//GEN-LAST:event_txtNumericKeyCaretPositionChanged

    private void txtNumericKeyInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtNumericKeyInputMethodTextChanged

    }//GEN-LAST:event_txtNumericKeyInputMethodTextChanged

    private void txtNumericKeyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumericKeyKeyReleased
        String input = txtNumericKey.getText();
        if (input.length() > 0) {
            String[] numbers = input.split(",");
            boolean isNumeric;
            for (String number : numbers) {
                isNumeric = NumberUtils.isParsable(number);
                if (!isNumeric) {
                    new OnlyNumbersAlert(this, true).setVisible(true);
                    txtNumericKey.requestFocus();
                    return;
                }
            }
        }
    }//GEN-LAST:event_txtNumericKeyKeyReleased

    private void txtNumericKeyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumericKeyKeyTyped

    }//GEN-LAST:event_txtNumericKeyKeyTyped

    private void btnGenerateNumericKeyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeyMouseEntered
        pnlGenerateNumericKey.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnGenerateNumericKeyMouseEntered

    private void btnGenerateNumericKeyMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeyMouseExited
        pnlGenerateNumericKey.setBackground(null);
    }//GEN-LAST:event_btnGenerateNumericKeyMouseExited

    private void btnGenerateNumericKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeyActionPerformed
        new GenerateNumericKeyView(mainView).setVisible(true);
    }//GEN-LAST:event_btnGenerateNumericKeyActionPerformed

    private void txtDirectionKeyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDirectionKeyFocusLost
        String input = txtDirectionKey.getText(), separator = ",", lastLetter;
        if (input.length() > 0) {
            lastLetter = input.substring(input.length() - 1, input.length());
            if (lastLetter.equals(separator)) {
                txtNumericKey.setText(input.substring(0, input.length() - 1));
            }
        }
    }//GEN-LAST:event_txtDirectionKeyFocusLost

    private void txtDirectionKeyCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtDirectionKeyCaretPositionChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDirectionKeyCaretPositionChanged

    private void txtDirectionKeyInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtDirectionKeyInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDirectionKeyInputMethodTextChanged

    private void txtDirectionKeyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDirectionKeyKeyReleased

    }//GEN-LAST:event_txtDirectionKeyKeyReleased

    private void txtDirectionKeyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDirectionKeyKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDirectionKeyKeyTyped

    private void btnGenerateDirectionKeyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateDirectionKeyMouseEntered
        pnlGenerateDirectionKey.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnGenerateDirectionKeyMouseEntered

    private void btnGenerateDirectionKeyMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateDirectionKeyMouseExited
        pnlGenerateDirectionKey.setBackground(null);
    }//GEN-LAST:event_btnGenerateDirectionKeyMouseExited

    private void btnGenerateDirectionKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateDirectionKeyActionPerformed
        new GenerateDirectionKeyView(mainView).setVisible(true);
    }//GEN-LAST:event_btnGenerateDirectionKeyActionPerformed

    private void jcbUseGlobalNumericKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbUseGlobalNumericKeyActionPerformed
        if (jcbUseGlobalNumericKey.isSelected()) {
            String key = oView.getGlobalNumericKey();
            if (key == null) {
                new MandatoryFieldAlert(this, true, "clave global").setVisible(true);
                jcbUseGlobalNumericKey.setSelected(false);
                return;
            }
            configuration.setUseGlobalNumericKey(true);
            txtNumericKey.setText(key);
            configuration.setNumericKey(key);
        } else {
            configuration.setUseGlobalNumericKey(false);
        }
    }//GEN-LAST:event_jcbUseGlobalNumericKeyActionPerformed

    private void numericKey01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_numericKey01MouseClicked
        new NumericKeyHelper(this, true).setVisible(true);
    }//GEN-LAST:event_numericKey01MouseClicked

    private void directionBounces01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_directionBounces01MouseClicked
        new DirectionBouncesConfigHelper(this, true).setVisible(true);
    }//GEN-LAST:event_directionBounces01MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SuccessiveBouncesView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new SuccessiveBouncesView(null, null, null).setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnGenerateDirectionKey;
    private javax.swing.JButton btnGenerateNumericKey;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JButton btnSaveConfiguration;
    private javax.swing.JLabel directionBounces01;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JCheckBox jcbRestrictionStart;
    private javax.swing.JCheckBox jcbUseGlobalNumericKey;
    private javax.swing.JPanel jpSuccessiveBounces;
    private javax.swing.JLabel numericKey01;
    private javax.swing.JPanel pnlClose;
    private javax.swing.JPanel pnlGenerateDirectionKey;
    private javax.swing.JPanel pnlGenerateNumericKey;
    private javax.swing.JPanel pnlMinimize;
    private javax.swing.JPanel pnlSaveConfiguration;
    private javax.swing.JPanel pnlSaveConfiguration1;
    private javax.swing.JPanel pnlSaveConfiguration3;
    private javax.swing.JPanel pnlSaveConfiguration4;
    private javax.swing.JTextField txtDirectionKey;
    private javax.swing.JTextField txtInitialCharacter;
    private javax.swing.JTextField txtNumericKey;
    // End of variables declaration//GEN-END:variables
}
