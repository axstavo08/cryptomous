/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package views;

import com.sun.awt.AWTUtilities;
import java.awt.Color;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JList;
import logic.actions.CryptoGeneratorAction;
import logic.entity.CryptoAlphabetToAction;
import logic.entity.CryptoCharactersVariationConfiguration;
import logic.entity.CryptoConstants;
import logic.entity.CryptoTypeConfiguration;
import logic.entity.CryptoGlobalVariablesConfiguration;
import logic.entity.CryptoGradualDoubleAppConfiguration;
import logic.entity.CryptoNormalDoubleAppConfiguration;
import logic.entity.CryptoNumbersSort;
import logic.entity.CryptoSucBouncesConfiguration;
import logic.entity.CryptoTranspositionConfiguration;
import logic.entity.CryptoWeftConfiguration;
import logic.events.ExportTransferHandler;
import logic.events.ImportTransferHandler;
import logic.events.ItemsListListener;
import logic.exception.CryptoGlobalException;
import org.apache.commons.lang3.math.NumberUtils;
import resources.alerts.CleanAllOpionsAlert;
import resources.alerts.DuplicateNumericKeyAlert;
import resources.alerts.GenerateAlphabetEmptyAlert;
import resources.alerts.MandatoryCriptoFlatDesignAlert;
import resources.alerts.MandatoryFieldAlert;
import resources.alerts.MandatoryMethodsCompleteAlert;
import resources.alerts.OnlyNumbersAlert;
import resources.alerts.SystemConfigurationAlert;
import resources.helpers.AssignConfiigurationHelper;
import resources.helpers.KeyDesignHelper;
import resources.helpers.NumericKeyHelper;
import resources.helpers.PhraseDirectionHelper;
import resources.helpers.PhraseKeyHelper;
import resources.helpers.TypeDesignHelper;
import resources.helpers.WelcomeDesignToolHelper;

/**
 *
 * @author Gustavo Ramos M.
 */
public class CustomizeView extends javax.swing.JFrame {

    private int posX, posY;
    private final CustomizeView view;
    private CryptoGlobalVariablesConfiguration globalVariablesConfiguration;
    private CryptoTypeConfiguration criptoConfiguration;
    private CryptoTypeConfiguration flatConfiguration;
    private final DefaultListModel modelListCripto = new DefaultListModel<>(), modelListFlat = new DefaultListModel<>();
    private final Color incomplete = new Color(153, 38, 0), complete = new Color(204, 82, 0);
    //private static CustomizeView oView;

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public CustomizeView() {
        initComponents();
        @SuppressWarnings("OverridableMethodCallInConstructor")
        Shape shape = new RoundRectangle2D.Double(0, 0, this.getBounds().width, this.getBounds().height, 15, 15);
        AWTUtilities.setWindowShape(this, shape);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/images/crypto_logo.png"));
        setIconImage(icon);
        view = this;
        //oView = this;
        criptoConfiguration = new CryptoTypeConfiguration();
        flatConfiguration = new CryptoTypeConfiguration();
        globalVariablesConfiguration = new CryptoGlobalVariablesConfiguration();
        lblGoToHome.setForeground(new Color(13, 13, 13));
        btnGoToHome.setContentAreaFilled(false);
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setBackground(null);
        btnClose.setContentAreaFilled(false);
        pnlClose.setBackground(null);
        rbGlobalPhraseFromStart.setSelected(true);
        globalVariablesConfiguration.setDirectionPhraseKey(CryptoConstants.FromDirection.START);
        txaCritpoGenerated.setEditable(false);
        txaFlatGenerated.setEditable(false);
        pnlGenerateGlobalNumericKey.setBackground(null);
        pnlGenerateCripto.setBackground(null);
        pnlGenerateFlat.setBackground(null);
        jlMethodsTypes.setDragEnabled(true);
        jlMethodsCripto.setModel(modelListCripto);
        jlMethodsFlat.setModel(modelListFlat);
        jlMethodsCripto.setDropMode(DropMode.ON_OR_INSERT);
        jlMethodsFlat.setDropMode(DropMode.ON_OR_INSERT);
        jlMethodsTypes.setTransferHandler(new ExportTransferHandler(jlMethodsTypes));
        jlMethodsCripto.setTransferHandler(new ImportTransferHandler(view, CryptoConstants.TypeCrypto.CRIPTO));
        jlMethodsFlat.setTransferHandler(new ImportTransferHandler(view, CryptoConstants.TypeCrypto.FLAT));
        MouseAdapter listenerCripto = new ItemsListListener(jlMethodsCripto, view, CryptoConstants.TypeCrypto.CRIPTO);
        MouseAdapter listenerFlat = new ItemsListListener(jlMethodsFlat, view, CryptoConstants.TypeCrypto.FLAT);
        jlMethodsCripto.addMouseListener(listenerCripto);
        jlMethodsCripto.addMouseMotionListener(listenerCripto);
        jlMethodsFlat.addMouseListener(listenerFlat);
        jlMethodsCripto.addMouseMotionListener(listenerFlat);
    }

    /*private void showWelcomeMessage() {
        new WelcomeDesignToolHelper(this, true).setVisible(true);
    }*/
    public void generateGlobalNumericKey(String numericKey) {
        txtGlobalNumericKey.setText(numericKey);
        globalVariablesConfiguration.setNumericKey(numericKey);
    }

    public String getGlobalNumericKey() {
        return (txtGlobalNumericKey.getText().length() > 0) ? txtGlobalNumericKey.getText() : null;
    }

    public String getGlobalPhraseKey() {
        return (txtGlobalPhraseKey.getText().length() > 0) ? txtGlobalPhraseKey.getText() : null;
    }

    public void cleanAllOptions() {
        txtGlobalNumericKey.setText("");
        txtGlobalPhraseKey.setText("");
        /*DefaultListModel model = new DefaultListModel();
        model.clear();*/
        modelListCripto.clear();
        jlMethodsCripto.setModel(modelListCripto);
        modelListFlat.clear();
        jlMethodsFlat.setModel(modelListFlat);
        txaCritpoGenerated.setText("");
        txaFlatGenerated.setText("");
        criptoConfiguration = new CryptoTypeConfiguration();
        flatConfiguration = new CryptoTypeConfiguration();
        globalVariablesConfiguration = new CryptoGlobalVariablesConfiguration();
        rbGlobalPhraseFromStart.setSelected(true);
        globalVariablesConfiguration.setDirectionPhraseKey(CryptoConstants.FromDirection.START);
        jplGenAlphCripto.setBackground(new Color(153, 38, 0));
        jplGenAlphFlat.setBackground(new Color(153, 38, 0));
    }

    public void setGenerateAlphabetConfiguration(CryptoConstants.TypeCrypto type, CryptoConstants.TypeArranged ordering, int loops, String initialCharacter, boolean usePhraseKey) {
        Color colorContainer;
        switch (type) {
            case CRIPTO:
                criptoConfiguration.getCharactersConfiguration().setOrdering(ordering);
                criptoConfiguration.getCharactersConfiguration().setLoops(loops);
                criptoConfiguration.getCharactersConfiguration().setInitialCharacter(initialCharacter);
                criptoConfiguration.getCharactersConfiguration().setUsePhraseKey(usePhraseKey);
                criptoConfiguration.getCharactersConfiguration().setIsConfigured(true);
                colorContainer = jplGenAlphCripto.getBackground();
                if (colorContainer.getRGB() == incomplete.getRGB()) {
                    jplGenAlphCripto.setBackground(complete);
                }
                break;
            case FLAT:
                flatConfiguration.getCharactersConfiguration().setOrdering(ordering);
                flatConfiguration.getCharactersConfiguration().setLoops(loops);
                flatConfiguration.getCharactersConfiguration().setInitialCharacter(initialCharacter);
                flatConfiguration.getCharactersConfiguration().setUsePhraseKey(usePhraseKey);
                flatConfiguration.getCharactersConfiguration().setIsConfigured(true);
                colorContainer = jplGenAlphFlat.getBackground();
                if (colorContainer.getRGB() == incomplete.getRGB()) {
                    jplGenAlphFlat.setBackground(complete);
                }
                break;
        }
    }

    public void setSucBouncesConfiguration(CryptoConstants.TypeCrypto type, String methodKey, String numericKey, String directionKey, String initialCharacter,
            boolean initial, boolean useGlobalNumericKey) {
        switch (type) {
            case CRIPTO:
                criptoConfiguration.getCharactersMethods().put(methodKey, new CryptoSucBouncesConfiguration(numericKey, directionKey, initialCharacter, initial, useGlobalNumericKey));
                jlMethodsCripto.setSelectionBackground(complete);
                break;
            case FLAT:
                flatConfiguration.getCharactersMethods().put(methodKey, new CryptoSucBouncesConfiguration(numericKey, directionKey, initialCharacter, initial, useGlobalNumericKey));
                jlMethodsFlat.setSelectionBackground(complete);
                break;
        }
    }

    public void setWeftConfiguration(CryptoConstants.TypeCrypto type, String methodKey, String numericKey, CryptoNumbersSort assignRestriction,
            CryptoNumbersSort sortRestriction, boolean useGlobalNumericKey) {
        switch (type) {
            case CRIPTO:
                criptoConfiguration.getCharactersMethods().put(methodKey, new CryptoWeftConfiguration(numericKey, assignRestriction, sortRestriction, useGlobalNumericKey));
                jlMethodsCripto.setSelectionBackground(complete);
                break;
            case FLAT:
                flatConfiguration.getCharactersMethods().put(methodKey, new CryptoWeftConfiguration(numericKey, assignRestriction, sortRestriction, useGlobalNumericKey));
                jlMethodsFlat.setSelectionBackground(complete);
                break;
        }
    }

    public void setTranspositionConfiguration(CryptoConstants.TypeCrypto type, String methodKey, String numericKey, CryptoNumbersSort firstRestriction,
            CryptoNumbersSort secondRestriction, CryptoNumbersSort thirdRestriction, CryptoNumbersSort fourthRestriction, CryptoNumbersSort fifthRestriction,
            CryptoNumbersSort sixthRestriction, boolean useGlobalNumericKey) {
        switch (type) {
            case CRIPTO:
                criptoConfiguration.getCharactersMethods().put(methodKey, new CryptoTranspositionConfiguration(numericKey, firstRestriction, secondRestriction,
                        thirdRestriction, fourthRestriction, fifthRestriction, sixthRestriction, useGlobalNumericKey));
                jlMethodsCripto.setSelectionBackground(complete);
                break;
            case FLAT:
                flatConfiguration.getCharactersMethods().put(methodKey, new CryptoTranspositionConfiguration(numericKey, firstRestriction, secondRestriction,
                        thirdRestriction, fourthRestriction, fifthRestriction, sixthRestriction, useGlobalNumericKey));
                jlMethodsFlat.setSelectionBackground(complete);
                break;
        }
    }

    public void setNormalDoubleAppConfiguration(CryptoConstants.TypeCrypto type, String methodKey, String numericKeyFirstApp,
            String numericKeySecondApp, CryptoNumbersSort orderingRestrictionFirstApp, CryptoNumbersSort extractingRestrictionFirstApp, CryptoNumbersSort orderingRestrictionSecondApp,
            CryptoNumbersSort extractingRestrictionSecondApp, boolean useGlobalNumericKeyFirstApp, boolean useGlobalNumericKeySecondApp) {
        switch (type) {
            case CRIPTO:
                criptoConfiguration.getCharactersMethods().put(methodKey, new CryptoNormalDoubleAppConfiguration(numericKeyFirstApp, orderingRestrictionFirstApp, extractingRestrictionFirstApp,
                        useGlobalNumericKeyFirstApp, numericKeySecondApp, orderingRestrictionSecondApp, extractingRestrictionSecondApp, useGlobalNumericKeySecondApp));
                jlMethodsCripto.setSelectionBackground(complete);
                break;
            case FLAT:
                flatConfiguration.getCharactersMethods().put(methodKey, new CryptoNormalDoubleAppConfiguration(numericKeyFirstApp, orderingRestrictionFirstApp, extractingRestrictionFirstApp,
                        useGlobalNumericKeyFirstApp, numericKeySecondApp, orderingRestrictionSecondApp, extractingRestrictionSecondApp, useGlobalNumericKeySecondApp));
                jlMethodsFlat.setSelectionBackground(complete);
                break;
        }
    }

    public void setGradualDoubleAppConfiguration(CryptoConstants.TypeCrypto type, String methodKey, String numericKeyFirstApp,
            String numericKeySecondApp, CryptoNumbersSort orderingRestrictionFirstApp, CryptoNumbersSort extractingRestrictionFirstApp, CryptoNumbersSort orderingRestrictionSecondApp,
            CryptoNumbersSort extractingRestrictionSecondApp, boolean useGlobalNumericKeyFirstApp, boolean useGlobalNumericKeySecondApp) {
        switch (type) {
            case CRIPTO:
                criptoConfiguration.getCharactersMethods().put(methodKey, new CryptoGradualDoubleAppConfiguration(numericKeyFirstApp, orderingRestrictionFirstApp, extractingRestrictionFirstApp,
                        useGlobalNumericKeyFirstApp, numericKeySecondApp, orderingRestrictionSecondApp, extractingRestrictionSecondApp, useGlobalNumericKeySecondApp));
                jlMethodsCripto.setSelectionBackground(complete);
                break;
            case FLAT:
                flatConfiguration.getCharactersMethods().put(methodKey, new CryptoGradualDoubleAppConfiguration(numericKeyFirstApp, orderingRestrictionFirstApp, extractingRestrictionFirstApp,
                        useGlobalNumericKeyFirstApp, numericKeySecondApp, orderingRestrictionSecondApp, extractingRestrictionSecondApp, useGlobalNumericKeySecondApp));
                jlMethodsFlat.setSelectionBackground(complete);
                break;
        }
    }

    public void setCharactersVariationConfiguration(CryptoConstants.TypeCrypto type, String methodKey, CryptoConstants.TypeArranged ordering, int loops,
            String initialCharacter, String phraseKey, boolean useGlobalPhraseKey) {
        switch (type) {
            case CRIPTO:
                criptoConfiguration.getCharactersMethods().put(methodKey, new CryptoCharactersVariationConfiguration(ordering, loops, initialCharacter, phraseKey, useGlobalPhraseKey));
                jlMethodsCripto.setSelectionBackground(complete);
                break;
            case FLAT:
                flatConfiguration.getCharactersMethods().put(methodKey, new CryptoCharactersVariationConfiguration(ordering, loops, initialCharacter, phraseKey, useGlobalPhraseKey));
                jlMethodsFlat.setSelectionBackground(complete);
                break;
        }
    }

    public void enableRemoveOptionItemMethod(CryptoConstants.TypeCrypto type) {
        String selectedValue, num, name;
        CryptoConstants.Methods method;
        int selectedIndex;
        String[] key;
        switch (type) {
            case CRIPTO:
                selectedIndex = jlMethodsCripto.getSelectedIndex();
                if (selectedIndex != -1) {
                    selectedValue = jlMethodsCripto.getSelectedValue();
                    key = selectedValue.split(" ");
                    num = key[key.length - 1];
                    name = selectedValue.substring(0, selectedValue.indexOf(num) - 1);
                    method = CryptoConstants.Methods.findMethodByCoincidenceLabel(name);
                    jlMethodsCripto.setSelectionBackground((criptoConfiguration.getCharactersMethods().get(method.getKey().concat(num)) != null) ? complete : incomplete);
                    btnListRemoveItemCripto.setEnabled(true);
                } else {
                    btnListRemoveItemCripto.setEnabled(false);
                }
                break;
            case FLAT:
                selectedIndex = jlMethodsFlat.getSelectedIndex();
                if (selectedIndex != -1) {
                    selectedValue = jlMethodsFlat.getSelectedValue();
                    key = selectedValue.split(" ");
                    num = key[key.length - 1];
                    name = selectedValue.substring(0, selectedValue.indexOf(num) - 1);
                    method = CryptoConstants.Methods.findMethodByCoincidenceLabel(name);
                    jlMethodsFlat.setSelectionBackground((flatConfiguration.getCharactersMethods().get(method.getKey().concat(num)) != null) ? complete : incomplete);
                    btnListRemoveItemFlat.setEnabled(true);
                } else {
                    btnListRemoveItemFlat.setEnabled(false);
                }
                break;
        }
    }

    public void showMethodConfiguration(CryptoConstants.TypeCrypto type, String item) {
        String[] key = item.split(" ");
        String num = key[key.length - 1], name = item.substring(0, item.indexOf(num) - 1), methodKey;
        CryptoConstants.Methods method = CryptoConstants.Methods.findMethodByCoincidenceLabel(name);
        methodKey = method.getKey().concat(num);
        switch (method) {
            case CHARACTERS_VARIATION:
                CryptoCharactersVariationConfiguration cvConfig;
                switch (type) {
                    case CRIPTO:
                        cvConfig = (CryptoCharactersVariationConfiguration) criptoConfiguration.getCharactersMethods().get(methodKey);
                        if (cvConfig != null) {
                            VariationCharactersView.editConfigurationViewByParameters(view, type, methodKey, cvConfig.getOrdering(), cvConfig.getLoops(),
                                    cvConfig.getInitialCharacter(), cvConfig.getPhraseKey(), cvConfig.isUseGlobalPhraseKey()).setVisible(true);
                        } else {
                            new VariationCharactersView(view, type, methodKey).setVisible(true);
                        }
                        break;
                    case FLAT:
                        cvConfig = (CryptoCharactersVariationConfiguration) flatConfiguration.getCharactersMethods().get(methodKey);
                        if (cvConfig != null) {
                            VariationCharactersView.editConfigurationViewByParameters(view, type, methodKey, cvConfig.getOrdering(), cvConfig.getLoops(),
                                    cvConfig.getInitialCharacter(), cvConfig.getPhraseKey(), cvConfig.isUseGlobalPhraseKey()).setVisible(true);
                        } else {
                            new VariationCharactersView(view, type, methodKey).setVisible(true);
                        }
                        break;
                }
                break;
            case SUCCESSIVE_BOUNCES:
                //System.out.println(methodKey);
                CryptoSucBouncesConfiguration sucBouncesConfig;
                switch (type) {
                    case CRIPTO:
                        sucBouncesConfig = (CryptoSucBouncesConfiguration) criptoConfiguration.getCharactersMethods().get(methodKey);
                        if (sucBouncesConfig != null) {
                            SuccessiveBouncesView.editConfigurationViewByParameters(view, type, methodKey, sucBouncesConfig.getNumericKey(), sucBouncesConfig.getDirectionKey(),
                                    sucBouncesConfig.getInitialCharacter(), sucBouncesConfig.isInitial(), sucBouncesConfig.isUseGlobalNumericKey()).setVisible(true);
                        } else {
                            new SuccessiveBouncesView(view, type, methodKey).setVisible(true);
                        }
                        break;
                    case FLAT:
                        sucBouncesConfig = (CryptoSucBouncesConfiguration) flatConfiguration.getCharactersMethods().get(methodKey);
                        if (sucBouncesConfig != null) {
                            SuccessiveBouncesView.editConfigurationViewByParameters(view, type, methodKey, sucBouncesConfig.getNumericKey(), sucBouncesConfig.getDirectionKey(),
                                    sucBouncesConfig.getInitialCharacter(), sucBouncesConfig.isInitial(), sucBouncesConfig.isUseGlobalNumericKey()).setVisible(true);
                        } else {
                            new SuccessiveBouncesView(view, type, methodKey).setVisible(true);
                        }
                        break;
                }
                break;
            case TRANSPOSITION:
                CryptoTranspositionConfiguration transpositionConfig;
                switch (type) {
                    case CRIPTO:
                        transpositionConfig = (CryptoTranspositionConfiguration) criptoConfiguration.getCharactersMethods().get(methodKey);
                        if (transpositionConfig != null) {
                            TranspositionView.editConfigurationViewByParameters(view, type, methodKey, transpositionConfig.getNumericKey(), transpositionConfig.getFirstRestriction(),
                                    transpositionConfig.getSecondRestriction(), transpositionConfig.getThirdRestriction(), transpositionConfig.getFourthRestriction(),
                                    transpositionConfig.getFifthRestriction(), transpositionConfig.getSixthRestriction(), transpositionConfig.isUseGlobalNumericKey()).setVisible(true);
                        } else {
                            new TranspositionView(view, type, methodKey).setVisible(true);
                        }
                        break;
                    case FLAT:
                        transpositionConfig = (CryptoTranspositionConfiguration) flatConfiguration.getCharactersMethods().get(methodKey);
                        if (transpositionConfig != null) {
                            TranspositionView.editConfigurationViewByParameters(view, type, methodKey, transpositionConfig.getNumericKey(), transpositionConfig.getFirstRestriction(),
                                    transpositionConfig.getSecondRestriction(), transpositionConfig.getThirdRestriction(), transpositionConfig.getFourthRestriction(),
                                    transpositionConfig.getFifthRestriction(), transpositionConfig.getSixthRestriction(), transpositionConfig.isUseGlobalNumericKey()).setVisible(true);
                        } else {
                            new TranspositionView(view, type, methodKey).setVisible(true);
                        }
                        break;
                }
                break;
            case WEFT:
                CryptoWeftConfiguration weftConfig;
                switch (type) {
                    case CRIPTO:
                        weftConfig = (CryptoWeftConfiguration) criptoConfiguration.getCharactersMethods().get(methodKey);
                        if (weftConfig != null) {
                            WeftView.editConfigurationViewByParameters(view, type, methodKey, weftConfig.getNumericKey(), weftConfig.getAssignRestriction(),
                                    weftConfig.getSortRestriction(), weftConfig.isUseGlobalNumericKey()).setVisible(true);
                        } else {
                            new WeftView(view, type, methodKey).setVisible(true);
                        }
                        break;
                    case FLAT:
                        weftConfig = (CryptoWeftConfiguration) flatConfiguration.getCharactersMethods().get(methodKey);
                        if (weftConfig != null) {
                            WeftView.editConfigurationViewByParameters(view, type, methodKey, weftConfig.getNumericKey(), weftConfig.getAssignRestriction(),
                                    weftConfig.getSortRestriction(), weftConfig.isUseGlobalNumericKey()).setVisible(true);
                        } else {
                            new WeftView(view, type, methodKey).setVisible(true);
                        }
                        break;
                }
                break;
            case CHARACTERS_DIVISION:
                break;
            case NORMAL_DOUBLE_APP:
                CryptoNormalDoubleAppConfiguration ndpConfig;
                switch (type) {
                    case CRIPTO:
                        ndpConfig = (CryptoNormalDoubleAppConfiguration) criptoConfiguration.getCharactersMethods().get(methodKey);
                        if (ndpConfig != null) {
                            NormalDoubleAppView.editConfigurationViewByParameters(view, type, methodKey, ndpConfig.getNumericKeyFirstApp(), ndpConfig.getNumericKeySecondApp(),
                                    ndpConfig.getOrderingFirstAppRestriction(), ndpConfig.getExtractingFirstAppRestriction(),
                                    ndpConfig.getOrderingSecondAppRestriction(), ndpConfig.getExtractingSecondAppRestriction(),
                                    ndpConfig.isUseGlobalNumericKeyFirstApp(), ndpConfig.isUseGlobalNumericKeySecondApp()).setVisible(true);
                        } else {
                            new NormalDoubleAppView(view, type, methodKey).setVisible(true);
                        }
                        break;
                    case FLAT:
                        ndpConfig = (CryptoNormalDoubleAppConfiguration) flatConfiguration.getCharactersMethods().get(methodKey);
                        if (ndpConfig != null) {
                            NormalDoubleAppView.editConfigurationViewByParameters(view, type, methodKey, ndpConfig.getNumericKeyFirstApp(), ndpConfig.getNumericKeySecondApp(),
                                    ndpConfig.getOrderingFirstAppRestriction(), ndpConfig.getExtractingFirstAppRestriction(),
                                    ndpConfig.getOrderingSecondAppRestriction(), ndpConfig.getExtractingSecondAppRestriction(),
                                    ndpConfig.isUseGlobalNumericKeyFirstApp(), ndpConfig.isUseGlobalNumericKeySecondApp()).setVisible(true);
                        } else {
                            new NormalDoubleAppView(view, type, methodKey).setVisible(true);
                        }
                        break;
                }
                break;
            case GRADUAL_DOUBLE_APP:
                CryptoGradualDoubleAppConfiguration gdpConfig;
                switch (type) {
                    case CRIPTO:
                        gdpConfig = (CryptoGradualDoubleAppConfiguration) criptoConfiguration.getCharactersMethods().get(methodKey);
                        if (gdpConfig != null) {
                            GradualDoubleAppView.editConfigurationViewByParameters(view, type, methodKey, gdpConfig.getNumericKeyFirstApp(), gdpConfig.getNumericKeySecondApp(),
                                    gdpConfig.getOrderingFirstAppRestriction(), gdpConfig.getExtractingFirstAppRestriction(),
                                    gdpConfig.getOrderingSecondAppRestriction(), gdpConfig.getExtractingSecondAppRestriction(),
                                    gdpConfig.isUseGlobalNumericKeyFirstApp(), gdpConfig.isUseGlobalNumericKeySecondApp()).setVisible(true);
                        } else {
                            new GradualDoubleAppView(view, type, methodKey).setVisible(true);
                        }
                        break;
                    case FLAT:
                        gdpConfig = (CryptoGradualDoubleAppConfiguration) flatConfiguration.getCharactersMethods().get(methodKey);
                        if (gdpConfig != null) {
                            GradualDoubleAppView.editConfigurationViewByParameters(view, type, methodKey, gdpConfig.getNumericKeyFirstApp(), gdpConfig.getNumericKeySecondApp(),
                                    gdpConfig.getOrderingFirstAppRestriction(), gdpConfig.getExtractingFirstAppRestriction(),
                                    gdpConfig.getOrderingSecondAppRestriction(), gdpConfig.getExtractingSecondAppRestriction(),
                                    gdpConfig.isUseGlobalNumericKeyFirstApp(), gdpConfig.isUseGlobalNumericKeySecondApp()).setVisible(true);
                        } else {
                            new GradualDoubleAppView(view, type, methodKey).setVisible(true);
                        }
                        break;
                }
                break;
        }
    }

    public void addItemToJListByType(CryptoConstants.TypeCrypto type, String data) {
        JList.DropLocation jList;
        int row;
        String num, key;
        CryptoConstants.Methods method = CryptoConstants.Methods.findMethodByLabel(data);
        switch (type) {
            case CRIPTO:
                jList = jlMethodsCripto.getDropLocation();
                row = jList.getIndex();
                num = String.valueOf(criptoConfiguration.getMethodCounter(method));
                data = data.concat(" ").concat(num);
                modelListCripto.add(row, data);
                jlMethodsCripto.validate();
                key = method.getKey().concat(num);
                criptoConfiguration.getCharactersMethods().put(key, null);
                break;
            case FLAT:
                jList = jlMethodsFlat.getDropLocation();
                row = jList.getIndex();
                num = String.valueOf(flatConfiguration.getMethodCounter(method));
                data = data.concat(" ").concat(num);
                modelListFlat.add(row, data);
                jlMethodsFlat.validate();
                key = method.getKey().concat(num);
                flatConfiguration.getCharactersMethods().put(key, null);
                break;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jpGlobalVariables = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        rbGlobalPhraseFromEnd = new javax.swing.JRadioButton();
        rbGlobalPhraseFromStart = new javax.swing.JRadioButton();
        txtGlobalNumericKey = new javax.swing.JTextField();
        txtGlobalPhraseKey = new javax.swing.JTextField();
        pnlGenerateGlobalNumericKey = new javax.swing.JPanel();
        btnGenerateGlobalNumericKey = new javax.swing.JButton();
        phraseKeyDirection01 = new javax.swing.JLabel();
        numericKey01 = new javax.swing.JLabel();
        phraseKey01 = new javax.swing.JLabel();
        btnGoToHome = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        pnlClose = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        pnlMinimize = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnMinimize = new javax.swing.JButton();
        lblGoToHome = new javax.swing.JLabel();
        jplFlatDesign = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jplGenAlphFlat = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jlMethodsFlat = new javax.swing.JList<>();
        btnListRemoveItemFlat = new javax.swing.JButton();
        flatDesign01 = new javax.swing.JLabel();
        pnlGenerateFlat = new javax.swing.JPanel();
        pnlAssignSystem2 = new javax.swing.JPanel();
        btnGenerateFlat = new javax.swing.JButton();
        pnlAssignConfigSystem = new javax.swing.JPanel();
        pnlAssignSystem6 = new javax.swing.JPanel();
        btnAssignConfigSystem = new javax.swing.JButton();
        jplCriptoDesign = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jplGenAlphCripto = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jlMethodsCripto = new javax.swing.JList<>();
        btnListRemoveItemCripto = new javax.swing.JButton();
        criptoDesign01 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txaFlatGenerated = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        txaCritpoGenerated = new javax.swing.JTextArea();
        pnlResetDefault = new javax.swing.JPanel();
        pnlAssignSystem4 = new javax.swing.JPanel();
        btnResetDefault = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        pnlGenerateCripto = new javax.swing.JPanel();
        pnlAssignSystem7 = new javax.swing.JPanel();
        btnGenerateCripto = new javax.swing.JButton();
        jplMethodsDesign = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jlMethodsTypes = new javax.swing.JList<>();
        typeDesign01 = new javax.swing.JLabel();
        assignConfiguration01 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Personalizar");
        setBackground(new java.awt.Color(35, 86, 104));
        setName("Personalizar"); // NOI18N
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(139, 195, 74));
        jPanel4.setForeground(new java.awt.Color(255, 173, 51));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jpGlobalVariables.setBackground(new java.awt.Color(104, 159, 56));
        jpGlobalVariables.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jpGlobalVariables.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Clave numérica (*)");
        jpGlobalVariables.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 120, 40));

        jLabel1.setBackground(new java.awt.Color(104, 159, 56));
        jLabel1.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 214, 204));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("VARIABLES GLOBALES");
        jpGlobalVariables.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 790, 60));

        jLabel13.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel13.setText("Frase clave");
        jpGlobalVariables.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, 100, 60));

        rbGlobalPhraseFromEnd.setBackground(new java.awt.Color(242, 242, 242));
        rbGlobalPhraseFromEnd.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        rbGlobalPhraseFromEnd.setForeground(new java.awt.Color(255, 255, 255));
        rbGlobalPhraseFromEnd.setText("Desde el último");
        rbGlobalPhraseFromEnd.setContentAreaFilled(false);
        rbGlobalPhraseFromEnd.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbGlobalPhraseFromEnd.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbGlobalPhraseFromEnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbGlobalPhraseFromEndActionPerformed(evt);
            }
        });
        jpGlobalVariables.add(rbGlobalPhraseFromEnd, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 120, 120, 40));

        rbGlobalPhraseFromStart.setBackground(new java.awt.Color(242, 242, 242));
        rbGlobalPhraseFromStart.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        rbGlobalPhraseFromStart.setForeground(new java.awt.Color(255, 255, 255));
        rbGlobalPhraseFromStart.setText("Desde el inicio");
        rbGlobalPhraseFromStart.setContentAreaFilled(false);
        rbGlobalPhraseFromStart.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbGlobalPhraseFromStart.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbGlobalPhraseFromStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbGlobalPhraseFromStartActionPerformed(evt);
            }
        });
        jpGlobalVariables.add(rbGlobalPhraseFromStart, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 120, 110, 40));

        txtGlobalNumericKey.setBackground(new java.awt.Color(242, 242, 242));
        txtGlobalNumericKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtGlobalNumericKey.setForeground(new java.awt.Color(13, 13, 13));
        txtGlobalNumericKey.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtGlobalNumericKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtGlobalNumericKey.setCaretColor(new java.awt.Color(13, 13, 13));
        txtGlobalNumericKey.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtGlobalNumericKey.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtGlobalNumericKey.setVerifyInputWhenFocusTarget(false);
        txtGlobalNumericKey.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtGlobalNumericKeyFocusLost(evt);
            }
        });
        txtGlobalNumericKey.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                txtGlobalNumericKeyCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtGlobalNumericKeyInputMethodTextChanged(evt);
            }
        });
        txtGlobalNumericKey.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtGlobalNumericKeyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtGlobalNumericKeyKeyTyped(evt);
            }
        });
        jpGlobalVariables.add(txtGlobalNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 60, 490, 40));

        txtGlobalPhraseKey.setBackground(new java.awt.Color(242, 242, 242));
        txtGlobalPhraseKey.setColumns(3);
        txtGlobalPhraseKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtGlobalPhraseKey.setForeground(new java.awt.Color(13, 13, 13));
        txtGlobalPhraseKey.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtGlobalPhraseKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtGlobalPhraseKey.setCaretColor(new java.awt.Color(13, 13, 13));
        txtGlobalPhraseKey.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtGlobalPhraseKey.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtGlobalPhraseKey.setSelectionStart(1);
        txtGlobalPhraseKey.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtGlobalPhraseKeyFocusLost(evt);
            }
        });
        txtGlobalPhraseKey.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtGlobalPhraseKeyKeyReleased(evt);
            }
        });
        jpGlobalVariables.add(txtGlobalPhraseKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 120, 310, 40));

        pnlGenerateGlobalNumericKey.setBackground(new java.awt.Color(62, 94, 33));
        pnlGenerateGlobalNumericKey.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnGenerateGlobalNumericKey.setBackground(new java.awt.Color(104, 159, 56));
        btnGenerateGlobalNumericKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnGenerateGlobalNumericKey.setForeground(new java.awt.Color(242, 242, 242));
        btnGenerateGlobalNumericKey.setText("GENERAR");
        btnGenerateGlobalNumericKey.setToolTipText("");
        btnGenerateGlobalNumericKey.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        btnGenerateGlobalNumericKey.setContentAreaFilled(false);
        btnGenerateGlobalNumericKey.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGenerateGlobalNumericKey.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGenerateGlobalNumericKeyMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGenerateGlobalNumericKeyMouseExited(evt);
            }
        });
        btnGenerateGlobalNumericKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateGlobalNumericKeyActionPerformed(evt);
            }
        });
        pnlGenerateGlobalNumericKey.add(btnGenerateGlobalNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 40));

        jpGlobalVariables.add(pnlGenerateGlobalNumericKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 60, 110, 40));

        phraseKeyDirection01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        phraseKeyDirection01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        phraseKeyDirection01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                phraseKeyDirection01MouseClicked(evt);
            }
        });
        jpGlobalVariables.add(phraseKeyDirection01, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 125, -1, 30));

        numericKey01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        numericKey01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        numericKey01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                numericKey01MouseClicked(evt);
            }
        });
        jpGlobalVariables.add(numericKey01, new org.netbeans.lib.awtextra.AbsoluteConstraints(155, 65, -1, 30));

        phraseKey01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        phraseKey01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        phraseKey01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                phraseKey01MouseClicked(evt);
            }
        });
        jpGlobalVariables.add(phraseKey01, new org.netbeans.lib.awtextra.AbsoluteConstraints(155, 125, -1, 30));

        jPanel4.add(jpGlobalVariables, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 55, 830, 180));

        btnGoToHome.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnGoToHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/if_Left_arrow_2202280.png"))); // NOI18N
        btnGoToHome.setBorder(null);
        btnGoToHome.setContentAreaFilled(false);
        btnGoToHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGoToHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGoToHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGoToHomeMouseExited(evt);
            }
        });
        btnGoToHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoToHomeActionPerformed(evt);
            }
        });
        jPanel4.add(btnGoToHome, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 60, 40, 30));

        jPanel5.setBackground(new java.awt.Color(104, 159, 56));
        jPanel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel5MouseDragged(evt);
            }
        });
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel5MousePressed(evt);
            }
        });
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_icon_01.png"))); // NOI18N
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 0, 30, 30));

        jLabel9.setBackground(new java.awt.Color(104, 159, 56));
        jLabel9.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Personalizar");
        jPanel5.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1270, 30));

        pnlClose.setBackground(new java.awt.Color(204, 0, 0));
        pnlClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pnlCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pnlCloseMouseExited(evt);
            }
        });
        pnlClose.setLayout(null);

        btnClose.setBackground(new java.awt.Color(104, 159, 56));
        btnClose.setFont(new java.awt.Font("Eras Light ITC", 1, 18)); // NOI18N
        btnClose.setForeground(new java.awt.Color(255, 255, 255));
        btnClose.setText("X");
        btnClose.setToolTipText("Cerrar");
        btnClose.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 39, 60), 0, true));
        btnClose.setName(""); // NOI18N
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        pnlClose.add(btnClose);
        btnClose.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(1300, 0, 50, 30));

        pnlMinimize.setBackground(new java.awt.Color(104, 159, 56));
        pnlMinimize.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(null);
        pnlMinimize.add(jPanel6);
        jPanel6.setBounds(660, 0, 50, 30);

        btnMinimize.setBackground(new java.awt.Color(104, 159, 56));
        btnMinimize.setFont(new java.awt.Font("Eras Light ITC", 0, 48)); // NOI18N
        btnMinimize.setForeground(new java.awt.Color(255, 255, 255));
        btnMinimize.setText("-");
        btnMinimize.setToolTipText("Minimizar");
        btnMinimize.setBorder(null);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited(evt);
            }
        });
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });
        pnlMinimize.add(btnMinimize);
        btnMinimize.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlMinimize, new org.netbeans.lib.awtextra.AbsoluteConstraints(1260, 0, 50, 30));

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1350, 30));

        lblGoToHome.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        lblGoToHome.setForeground(new java.awt.Color(242, 242, 242));
        lblGoToHome.setText("Ir a inicio");
        lblGoToHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblGoToHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblGoToHomeMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblGoToHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblGoToHomeMouseExited(evt);
            }
        });
        jPanel4.add(lblGoToHome, new org.netbeans.lib.awtextra.AbsoluteConstraints(63, 55, 80, 40));

        jplFlatDesign.setBackground(new java.awt.Color(104, 159, 56));
        jplFlatDesign.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jplFlatDesign.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setBackground(new java.awt.Color(104, 159, 56));
        jLabel4.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 214, 204));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("DISEÑO DE LLANO");
        jplFlatDesign.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 0, 180, 60));

        jplGenAlphFlat.setBackground(new java.awt.Color(153, 38, 0));
        jplGenAlphFlat.setForeground(new java.awt.Color(242, 242, 242));
        jplGenAlphFlat.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jplGenAlphFlat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jplGenAlphFlatMouseClicked(evt);
            }
        });
        jplGenAlphFlat.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel8.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 16)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(242, 242, 242));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Generación de alfabeto (*)");
        jLabel8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jplGenAlphFlat.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 30, 230, 30));

        jplFlatDesign.add(jplGenAlphFlat, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 430, 90));

        jlMethodsFlat.setBackground(new java.awt.Color(155, 203, 103));
        jlMethodsFlat.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 16)); // NOI18N
        jlMethodsFlat.setForeground(new java.awt.Color(242, 242, 242));
        jlMethodsFlat.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jlMethodsFlat.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jlMethodsFlat.setDragEnabled(true);
        jlMethodsFlat.setSelectionBackground(new java.awt.Color(179, 0, 0));
        jScrollPane4.setViewportView(jlMethodsFlat);

        jplFlatDesign.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 380, 240));

        btnListRemoveItemFlat.setBackground(new java.awt.Color(255, 179, 179));
        btnListRemoveItemFlat.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        btnListRemoveItemFlat.setForeground(new java.awt.Color(62, 94, 33));
        btnListRemoveItemFlat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_remove.png"))); // NOI18N
        btnListRemoveItemFlat.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnListRemoveItemFlat.setEnabled(false);
        btnListRemoveItemFlat.setOpaque(false);
        btnListRemoveItemFlat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListRemoveItemFlatActionPerformed(evt);
            }
        });
        jplFlatDesign.add(btnListRemoveItemFlat, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 150, 50, 240));

        flatDesign01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        flatDesign01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        flatDesign01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                flatDesign01MouseClicked(evt);
            }
        });
        jplFlatDesign.add(flatDesign01, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 15, -1, 30));

        jPanel4.add(jplFlatDesign, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 260, 430, 390));

        pnlGenerateFlat.setBackground(new java.awt.Color(62, 94, 33));
        pnlGenerateFlat.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlAssignSystem2.setBackground(new java.awt.Color(62, 94, 33));
        pnlAssignSystem2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlGenerateFlat.add(pnlAssignSystem2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 370, 260, 40));

        btnGenerateFlat.setBackground(new java.awt.Color(139, 195, 74));
        btnGenerateFlat.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        btnGenerateFlat.setForeground(new java.awt.Color(242, 242, 242));
        btnGenerateFlat.setText("GENERAR LLANO");
        btnGenerateFlat.setToolTipText("");
        btnGenerateFlat.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnGenerateFlat.setContentAreaFilled(false);
        btnGenerateFlat.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGenerateFlat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGenerateFlatMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGenerateFlatMouseExited(evt);
            }
        });
        btnGenerateFlat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateFlatActionPerformed(evt);
            }
        });
        pnlGenerateFlat.add(btnGenerateFlat, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 430, 40));

        jPanel4.add(pnlGenerateFlat, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 670, 430, 40));

        pnlAssignConfigSystem.setBackground(new java.awt.Color(204, 99, 0));
        pnlAssignConfigSystem.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlAssignSystem6.setBackground(new java.awt.Color(62, 94, 33));
        pnlAssignSystem6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlAssignConfigSystem.add(pnlAssignSystem6, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 370, 260, 40));

        btnAssignConfigSystem.setBackground(new java.awt.Color(204, 99, 0));
        btnAssignConfigSystem.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        btnAssignConfigSystem.setForeground(new java.awt.Color(242, 242, 242));
        btnAssignConfigSystem.setText("ASIGNAR CONFIGURACIÓN");
        btnAssignConfigSystem.setToolTipText("");
        btnAssignConfigSystem.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnAssignConfigSystem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAssignConfigSystem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAssignConfigSystemActionPerformed(evt);
            }
        });
        pnlAssignConfigSystem.add(btnAssignConfigSystem, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 170, 40));

        jPanel4.add(pnlAssignConfigSystem, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 195, 170, 40));

        jplCriptoDesign.setBackground(new java.awt.Color(104, 159, 56));
        jplCriptoDesign.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jplCriptoDesign.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jplCriptoDesign.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setBackground(new java.awt.Color(104, 159, 56));
        jLabel11.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 1, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 214, 204));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("DISEÑO DE CRIPTO");
        jplCriptoDesign.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 0, 190, 60));

        jplGenAlphCripto.setBackground(new java.awt.Color(153, 38, 0));
        jplGenAlphCripto.setForeground(new java.awt.Color(242, 242, 242));
        jplGenAlphCripto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jplGenAlphCripto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jplGenAlphCriptoMouseClicked(evt);
            }
        });
        jplGenAlphCripto.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel7.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 16)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(242, 242, 242));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Generación de alfabeto (*)");
        jLabel7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jplGenAlphCripto.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 30, 220, 30));

        jplCriptoDesign.add(jplGenAlphCripto, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 430, 90));

        jlMethodsCripto.setBackground(new java.awt.Color(155, 203, 103));
        jlMethodsCripto.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 16)); // NOI18N
        jlMethodsCripto.setForeground(new java.awt.Color(242, 242, 242));
        jlMethodsCripto.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jlMethodsCripto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jlMethodsCripto.setDragEnabled(true);
        jlMethodsCripto.setSelectionBackground(new java.awt.Color(179, 0, 0));
        jScrollPane3.setViewportView(jlMethodsCripto);

        jplCriptoDesign.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 380, 240));

        btnListRemoveItemCripto.setBackground(new java.awt.Color(255, 179, 179));
        btnListRemoveItemCripto.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        btnListRemoveItemCripto.setForeground(new java.awt.Color(13, 13, 13));
        btnListRemoveItemCripto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_remove.png"))); // NOI18N
        btnListRemoveItemCripto.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnListRemoveItemCripto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnListRemoveItemCripto.setEnabled(false);
        btnListRemoveItemCripto.setOpaque(false);
        btnListRemoveItemCripto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListRemoveItemCriptoActionPerformed(evt);
            }
        });
        jplCriptoDesign.add(btnListRemoveItemCripto, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 150, 50, 240));

        criptoDesign01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        criptoDesign01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        criptoDesign01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                criptoDesign01MouseClicked(evt);
            }
        });
        jplCriptoDesign.add(criptoDesign01, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 15, -1, 30));

        jPanel4.add(jplCriptoDesign, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 260, 430, 390));

        txaFlatGenerated.setEditable(false);
        txaFlatGenerated.setBackground(new java.awt.Color(204, 204, 204));
        txaFlatGenerated.setColumns(20);
        txaFlatGenerated.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txaFlatGenerated.setForeground(new java.awt.Color(13, 13, 13));
        txaFlatGenerated.setLineWrap(true);
        txaFlatGenerated.setRows(5);
        txaFlatGenerated.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txaFlatGenerated.setCaretColor(new java.awt.Color(13, 13, 13));
        txaFlatGenerated.setSelectionColor(new java.awt.Color(139, 195, 74));
        jScrollPane1.setViewportView(txaFlatGenerated);

        jPanel4.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 730, 430, 120));

        txaCritpoGenerated.setEditable(false);
        txaCritpoGenerated.setBackground(new java.awt.Color(204, 204, 204));
        txaCritpoGenerated.setColumns(20);
        txaCritpoGenerated.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txaCritpoGenerated.setForeground(new java.awt.Color(13, 13, 13));
        txaCritpoGenerated.setLineWrap(true);
        txaCritpoGenerated.setRows(5);
        txaCritpoGenerated.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txaCritpoGenerated.setCaretColor(new java.awt.Color(13, 13, 13));
        txaCritpoGenerated.setSelectionColor(new java.awt.Color(139, 195, 74));
        jScrollPane2.setViewportView(txaCritpoGenerated);

        jPanel4.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 730, 430, 120));

        pnlResetDefault.setBackground(new java.awt.Color(153, 0, 0));
        pnlResetDefault.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlAssignSystem4.setBackground(new java.awt.Color(62, 94, 33));
        pnlAssignSystem4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlResetDefault.add(pnlAssignSystem4, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 370, 260, 40));

        btnResetDefault.setBackground(new java.awt.Color(179, 0, 0));
        btnResetDefault.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        btnResetDefault.setForeground(new java.awt.Color(242, 242, 242));
        btnResetDefault.setText("REESTABLECER CAMPOS POR DEFECTO");
        btnResetDefault.setToolTipText("");
        btnResetDefault.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnResetDefault.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnResetDefault.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetDefaultActionPerformed(evt);
            }
        });
        pnlResetDefault.add(btnResetDefault, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 370, 40));

        jPanel4.add(pnlResetDefault, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 810, 370, 40));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_proccess_01.png"))); // NOI18N
        jPanel4.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 60, 260, 170));

        pnlGenerateCripto.setBackground(new java.awt.Color(62, 94, 33));
        pnlGenerateCripto.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlAssignSystem7.setBackground(new java.awt.Color(62, 94, 33));
        pnlAssignSystem7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlGenerateCripto.add(pnlAssignSystem7, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 370, 260, 40));

        btnGenerateCripto.setBackground(new java.awt.Color(139, 195, 74));
        btnGenerateCripto.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 12)); // NOI18N
        btnGenerateCripto.setForeground(new java.awt.Color(242, 242, 242));
        btnGenerateCripto.setText("GENERAR CRIPTO");
        btnGenerateCripto.setToolTipText("");
        btnGenerateCripto.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnGenerateCripto.setContentAreaFilled(false);
        btnGenerateCripto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGenerateCripto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGenerateCriptoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGenerateCriptoMouseExited(evt);
            }
        });
        btnGenerateCripto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateCriptoActionPerformed(evt);
            }
        });
        pnlGenerateCripto.add(btnGenerateCripto, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 430, 40));

        jPanel4.add(pnlGenerateCripto, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 670, 430, 40));

        jplMethodsDesign.setBackground(new java.awt.Color(104, 159, 56));
        jplMethodsDesign.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jplMethodsDesign.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel12.setBackground(new java.awt.Color(104, 159, 56));
        jLabel12.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 1, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 214, 204));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("TIPOS O MÉTODOS DE CIFRADO");
        jplMethodsDesign.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 310, 60));

        jlMethodsTypes.setBackground(new java.awt.Color(155, 203, 103));
        jlMethodsTypes.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 20)); // NOI18N
        jlMethodsTypes.setForeground(new java.awt.Color(242, 242, 242));
        jlMethodsTypes.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Variación de Caracteres", "Saltos Sucesivos", "Transposición", "Tramas", "Doble Aplicación Normal", "Doble Aplicación Gradual" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jlMethodsTypes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jlMethodsTypes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jlMethodsTypes.setDragEnabled(true);
        jlMethodsTypes.setSelectionBackground(new java.awt.Color(204, 82, 0));
        jScrollPane5.setViewportView(jlMethodsTypes);

        jplMethodsDesign.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 370, 460));

        typeDesign01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        typeDesign01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        typeDesign01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                typeDesign01MouseClicked(evt);
            }
        });
        jplMethodsDesign.add(typeDesign01, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 15, -1, 30));

        jPanel4.add(jplMethodsDesign, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 260, 370, 520));

        assignConfiguration01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        assignConfiguration01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        assignConfiguration01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                assignConfiguration01MouseClicked(evt);
            }
        });
        jPanel4.add(assignConfiguration01, new org.netbeans.lib.awtextra.AbsoluteConstraints(215, 200, -1, 30));

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1350, 880));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnGoToHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoToHomeActionPerformed
        try {
            new MainView().setVisible(true);
        } catch (CryptoGlobalException ex) {
            Logger.getLogger(CustomizeView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_btnGoToHomeActionPerformed

    private void btnMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseEntered
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnMinimizeMouseEntered

    private void btnMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseExited
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);
    }//GEN-LAST:event_btnMinimizeMouseExited

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        pnlClose.setVisible(true);
        pnlClose.setBackground(new Color(230, 0, 0));
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        pnlClose.setBackground(null);
        btnClose.setVisible(true);
    }//GEN-LAST:event_btnCloseMouseExited

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnCloseActionPerformed

    private void pnlCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseEntered

    }//GEN-LAST:event_pnlCloseMouseEntered

    private void pnlCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseExited

    }//GEN-LAST:event_pnlCloseMouseExited

    private void jPanel5MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_jPanel5MouseDragged

    private void jPanel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_jPanel5MousePressed

    private void lblGoToHomeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToHomeMouseClicked
        try {
            new MainView().setVisible(true);
        } catch (CryptoGlobalException ex) {
            Logger.getLogger(CustomizeView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_lblGoToHomeMouseClicked

    private void btnGoToHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToHomeMouseEntered
        lblGoToHome.setForeground(new Color(242, 242, 242));
    }//GEN-LAST:event_btnGoToHomeMouseEntered

    private void btnGoToHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGoToHomeMouseExited
        lblGoToHome.setForeground(new Color(13, 13, 13));
    }//GEN-LAST:event_btnGoToHomeMouseExited

    private void lblGoToHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToHomeMouseEntered
        lblGoToHome.setForeground(new Color(242, 242, 242));
    }//GEN-LAST:event_lblGoToHomeMouseEntered

    private void lblGoToHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGoToHomeMouseExited
        lblGoToHome.setForeground(new Color(13, 13, 13));
    }//GEN-LAST:event_lblGoToHomeMouseExited

    private void rbGlobalPhraseFromEndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbGlobalPhraseFromEndActionPerformed
        rbGlobalPhraseFromEnd.setSelected(true);
        rbGlobalPhraseFromStart.setSelected(false);
        globalVariablesConfiguration.setDirectionPhraseKey(CryptoConstants.FromDirection.END);
    }//GEN-LAST:event_rbGlobalPhraseFromEndActionPerformed

    private void rbGlobalPhraseFromStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbGlobalPhraseFromStartActionPerformed
        rbGlobalPhraseFromStart.setSelected(true);
        rbGlobalPhraseFromEnd.setSelected(false);
        globalVariablesConfiguration.setDirectionPhraseKey(CryptoConstants.FromDirection.START);
    }//GEN-LAST:event_rbGlobalPhraseFromStartActionPerformed

    private void txtGlobalNumericKeyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGlobalNumericKeyFocusLost
        String input = txtGlobalNumericKey.getText(), separator = ",", lastLetter;
        if (input.length() > 0) {
            lastLetter = input.substring(input.length() - 1, input.length());
            if (lastLetter.equals(separator)) {
                txtGlobalNumericKey.setText((input.substring(0, input.length() - 1)));
            }
        }
        String[] numbers = input.split(",");
        Set<String> setNumbers = new HashSet<>(Arrays.asList(numbers));
        if (numbers.length != setNumbers.size()) {
            new DuplicateNumericKeyAlert(this, true).setVisible(true);
            txtGlobalNumericKey.requestFocus();
        }
    }//GEN-LAST:event_txtGlobalNumericKeyFocusLost

    private void txtGlobalPhraseKeyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGlobalPhraseKeyFocusLost
        globalVariablesConfiguration.setPhraseKey(txtGlobalPhraseKey.getText());
    }//GEN-LAST:event_txtGlobalPhraseKeyFocusLost

    private void txtGlobalNumericKeyCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtGlobalNumericKeyCaretPositionChanged

    }//GEN-LAST:event_txtGlobalNumericKeyCaretPositionChanged

    private void txtGlobalNumericKeyInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtGlobalNumericKeyInputMethodTextChanged

    }//GEN-LAST:event_txtGlobalNumericKeyInputMethodTextChanged

    private void txtGlobalNumericKeyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGlobalNumericKeyKeyTyped

    }//GEN-LAST:event_txtGlobalNumericKeyKeyTyped

    private void txtGlobalNumericKeyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGlobalNumericKeyKeyReleased
        String input = txtGlobalNumericKey.getText();
        if (input.length() > 0) {
            String[] numbers = input.split(",");
            boolean isNumeric;
            for (String number : numbers) {
                isNumeric = NumberUtils.isParsable(number);
                if (!isNumeric) {
                    new OnlyNumbersAlert(this, true).setVisible(true);
                    txtGlobalNumericKey.requestFocus();
                    return;
                }
            }
        }
    }//GEN-LAST:event_txtGlobalNumericKeyKeyReleased

    private void txtGlobalPhraseKeyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGlobalPhraseKeyKeyReleased
        String input = txtGlobalPhraseKey.getText();
        if (input.length() > 31) {
            txtGlobalPhraseKey.setText(input.substring(0, input.length() - 1));
        }
    }//GEN-LAST:event_txtGlobalPhraseKeyKeyReleased

    private void btnAssignConfigSystemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAssignConfigSystemActionPerformed
        String cripto = txaCritpoGenerated.getText(), flat = txaFlatGenerated.getText();
        if (cripto.length() > 0 && flat.length() > 0) {
            CryptoAlphabetToAction.customCripto = cripto;
            CryptoAlphabetToAction.customFlat = flat;
            new SystemConfigurationAlert(this, true, view).setVisible(true);
        } else {
            new MandatoryCriptoFlatDesignAlert(this, true).setVisible(true);
        }
    }//GEN-LAST:event_btnAssignConfigSystemActionPerformed

    private void btnGenerateCriptoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateCriptoActionPerformed
        String field = txtGlobalNumericKey.getText();
        if (field.length() == 0) {
            new MandatoryFieldAlert(this, true, "clave numérica").setVisible(true);
            txtGlobalNumericKey.requestFocus();
            return;
        }
        /*field = txtGlobalPhraseKey.getText();
        if (field.length() == 0) {
            new MandatoryFieldAlert(this, true, "frase clave").setVisible(true);
            txtGlobalPhraseKey.requestFocus();
            return;
        }*/
        if (!criptoConfiguration.getCharactersConfiguration().isIsConfigured()) {
            new GenerateAlphabetEmptyAlert(this, true).setVisible(true);
            jplGenAlphCripto.setBackground(new Color(153, 38, 0));
            return;
        } else {
            jplGenAlphCripto.setBackground(new Color(204, 82, 0));
        }
        int size = jlMethodsCripto.getModel().getSize();
        if (criptoConfiguration.getCharactersMethods().size() > 0 && size > 0) {
            for (int i = 0; i < size; i++) {
                field = jlMethodsCripto.getModel().getElementAt(i);
                String[] key = field.split(" ");
                String num = key[key.length - 1], name = field.substring(0, field.indexOf(num) - 1);
                CryptoConstants.Methods method = CryptoConstants.Methods.findMethodByCoincidenceLabel(name);
                if (criptoConfiguration.getCharactersMethods().get(method.getKey().concat(num)) == null) {
                    new MandatoryMethodsCompleteAlert(this, true).setVisible(true);
                    jlMethodsCripto.setSelectedIndex(i);
                    btnListRemoveItemCripto.setEnabled(true);
                    return;
                }
            }
        }
        try {
            List<String> methods = new ArrayList<>();
            size = jlMethodsCripto.getModel().getSize();
            for (int i = 0; i < size; i++) {
                field = jlMethodsCripto.getModel().getElementAt(i);
                methods.add(field);
            }
            String cripto = CryptoGeneratorAction.generateCrypto(globalVariablesConfiguration, methods, criptoConfiguration);
            txaCritpoGenerated.setText(cripto);
        } catch (CryptoGlobalException ex) {
            Logger.getLogger(CustomizeView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnGenerateCriptoActionPerformed

    private void btnGenerateFlatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateFlatActionPerformed
        String field = txtGlobalNumericKey.getText();
        if (field.length() == 0) {
            new MandatoryFieldAlert(this, true, "clave numérica").setVisible(true);
            txtGlobalNumericKey.requestFocus();
            return;
        }
        /*field = txtGlobalPhraseKey.getText();
        if (field.length() == 0) {
            new MandatoryFieldAlert(this, true, "frase clave").setVisible(true);
            txtGlobalPhraseKey.requestFocus();
            return;
        }*/
        if (!flatConfiguration.getCharactersConfiguration().isIsConfigured()) {
            new GenerateAlphabetEmptyAlert(this, true).setVisible(true);
            jplGenAlphFlat.setBackground(new Color(153, 38, 0));
            return;
        } else {
            jplGenAlphFlat.setBackground(new Color(204, 82, 0));
        }
        int size = jlMethodsFlat.getModel().getSize();
        if (flatConfiguration.getCharactersMethods().size() > 0 && size > 0) {
            for (int i = 0; i < size; i++) {
                field = jlMethodsFlat.getModel().getElementAt(i);
                String[] key = field.split(" ");
                String num = key[key.length - 1], name = field.substring(0, field.indexOf(num) - 1);
                CryptoConstants.Methods method = CryptoConstants.Methods.findMethodByCoincidenceLabel(name);
                if (flatConfiguration.getCharactersMethods().get(method.getKey().concat(num)) == null) {
                    new MandatoryMethodsCompleteAlert(this, true).setVisible(true);
                    jlMethodsFlat.setSelectedIndex(i);
                    btnListRemoveItemFlat.setEnabled(true);
                    return;
                }
            }
        }
        try {
            List<String> methods = new ArrayList<>();
            size = jlMethodsFlat.getModel().getSize();
            for (int i = 0; i < size; i++) {
                field = jlMethodsFlat.getModel().getElementAt(i);
                methods.add(field);
            }
            String flat = CryptoGeneratorAction.generateCrypto(globalVariablesConfiguration, methods, flatConfiguration);
            txaFlatGenerated.setText(flat);
        } catch (CryptoGlobalException ex) {
            Logger.getLogger(CustomizeView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnGenerateFlatActionPerformed

    private void btnResetDefaultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetDefaultActionPerformed
        new CleanAllOpionsAlert(this, true, view).setVisible(true);
    }//GEN-LAST:event_btnResetDefaultActionPerformed

    private void btnGenerateGlobalNumericKeyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateGlobalNumericKeyMouseEntered
        pnlGenerateGlobalNumericKey.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnGenerateGlobalNumericKeyMouseEntered

    private void btnGenerateGlobalNumericKeyMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateGlobalNumericKeyMouseExited
        pnlGenerateGlobalNumericKey.setBackground(null);
    }//GEN-LAST:event_btnGenerateGlobalNumericKeyMouseExited

    private void btnGenerateGlobalNumericKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateGlobalNumericKeyActionPerformed
        new GenerateNumericKeyView(view).setVisible(true);
    }//GEN-LAST:event_btnGenerateGlobalNumericKeyActionPerformed

    private void btnGenerateCriptoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateCriptoMouseEntered
        pnlGenerateCripto.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnGenerateCriptoMouseEntered

    private void btnGenerateCriptoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateCriptoMouseExited
        pnlGenerateCripto.setBackground(null);
    }//GEN-LAST:event_btnGenerateCriptoMouseExited

    private void btnGenerateFlatMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateFlatMouseEntered
        pnlGenerateFlat.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnGenerateFlatMouseEntered

    private void btnGenerateFlatMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateFlatMouseExited
        pnlGenerateFlat.setBackground(null);
    }//GEN-LAST:event_btnGenerateFlatMouseExited

    private void jplGenAlphCriptoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jplGenAlphCriptoMouseClicked
        if (criptoConfiguration.getCharactersConfiguration().isIsConfigured()) {
            GenerateAlphabetView.editConfigurationViewByParameters(view, CryptoConstants.TypeCrypto.CRIPTO, criptoConfiguration.getCharactersConfiguration().getOrdering(),
                    criptoConfiguration.getCharactersConfiguration().getLoops(), criptoConfiguration.getCharactersConfiguration().getInitialCharacter(),
                    criptoConfiguration.getCharactersConfiguration().isUsePhraseKey()).setVisible(true);
        } else {
            new GenerateAlphabetView(view, CryptoConstants.TypeCrypto.CRIPTO).setVisible(true);
        }
    }//GEN-LAST:event_jplGenAlphCriptoMouseClicked

    private void jplGenAlphFlatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jplGenAlphFlatMouseClicked
        if (flatConfiguration.getCharactersConfiguration().isIsConfigured()) {
            GenerateAlphabetView.editConfigurationViewByParameters(view, CryptoConstants.TypeCrypto.FLAT, flatConfiguration.getCharactersConfiguration().getOrdering(),
                    flatConfiguration.getCharactersConfiguration().getLoops(), flatConfiguration.getCharactersConfiguration().getInitialCharacter(),
                    flatConfiguration.getCharactersConfiguration().isUsePhraseKey()).setVisible(true);
        } else {
            new GenerateAlphabetView(view, CryptoConstants.TypeCrypto.FLAT).setVisible(true);
        }
    }//GEN-LAST:event_jplGenAlphFlatMouseClicked

    private void btnListRemoveItemCriptoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListRemoveItemCriptoActionPerformed
        DefaultListModel model = (DefaultListModel) jlMethodsCripto.getModel();
        int selectedIndex = jlMethodsCripto.getSelectedIndex();
        String selectedValue = jlMethodsCripto.getSelectedValue();
        if (selectedIndex != -1) {
            String[] key = selectedValue.split(" ");
            String num = key[key.length - 1], name = selectedValue.substring(0, selectedValue.indexOf(num) - 1);
            CryptoConstants.Methods method = CryptoConstants.Methods.findMethodByCoincidenceLabel(name);
            criptoConfiguration.getCharactersMethods().remove(method.getKey().concat(num));
            model.remove(selectedIndex);
            jlMethodsCripto.updateUI();
        }
        selectedIndex = jlMethodsCripto.getSelectedIndex();
        if (selectedIndex == -1 || jlMethodsCripto.getModel().getSize() == 0) {
            btnListRemoveItemCripto.setEnabled(false);
        }
    }//GEN-LAST:event_btnListRemoveItemCriptoActionPerformed

    private void btnListRemoveItemFlatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListRemoveItemFlatActionPerformed
        DefaultListModel model = (DefaultListModel) jlMethodsFlat.getModel();
        int selectedIndex = jlMethodsFlat.getSelectedIndex();
        String selectedValue = jlMethodsFlat.getSelectedValue();
        if (selectedIndex != -1) {
            String[] key = selectedValue.split(" ");
            String num = key[key.length - 1], name = selectedValue.substring(0, selectedValue.indexOf(num) - 1);
            CryptoConstants.Methods method = CryptoConstants.Methods.findMethodByCoincidenceLabel(name);
            flatConfiguration.getCharactersMethods().remove(method.getKey().concat(num));
            model.remove(selectedIndex);
            jlMethodsFlat.updateUI();
        }
        selectedIndex = jlMethodsFlat.getSelectedIndex();
        if (selectedIndex == -1 || jlMethodsFlat.getModel().getSize() == 0) {
            btnListRemoveItemFlat.setEnabled(false);
        }
    }//GEN-LAST:event_btnListRemoveItemFlatActionPerformed

    private void numericKey01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_numericKey01MouseClicked
        new NumericKeyHelper(this, true).setVisible(true);
    }//GEN-LAST:event_numericKey01MouseClicked

    private void phraseKey01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_phraseKey01MouseClicked
        new PhraseKeyHelper(this, true).setVisible(true);
    }//GEN-LAST:event_phraseKey01MouseClicked

    private void assignConfiguration01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_assignConfiguration01MouseClicked
        new AssignConfiigurationHelper(this, true).setVisible(true);
    }//GEN-LAST:event_assignConfiguration01MouseClicked

    private void phraseKeyDirection01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_phraseKeyDirection01MouseClicked
        new PhraseDirectionHelper(this, true).setVisible(true);
    }//GEN-LAST:event_phraseKeyDirection01MouseClicked

    private void criptoDesign01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_criptoDesign01MouseClicked
        new KeyDesignHelper(this, true).setVisible(true);
    }//GEN-LAST:event_criptoDesign01MouseClicked

    private void flatDesign01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_flatDesign01MouseClicked
        new KeyDesignHelper(this, true).setVisible(true);
    }//GEN-LAST:event_flatDesign01MouseClicked

    private void typeDesign01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_typeDesign01MouseClicked
        new TypeDesignHelper(this, true).setVisible(true);
    }//GEN-LAST:event_typeDesign01MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CustomizeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new CustomizeView().setVisible(true);
            //oView.showWelcomeMessage();
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel assignConfiguration01;
    private javax.swing.JButton btnAssignConfigSystem;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnGenerateCripto;
    private javax.swing.JButton btnGenerateFlat;
    private javax.swing.JButton btnGenerateGlobalNumericKey;
    private javax.swing.JButton btnGoToHome;
    private javax.swing.JButton btnListRemoveItemCripto;
    private javax.swing.JButton btnListRemoveItemFlat;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JButton btnResetDefault;
    private javax.swing.JLabel criptoDesign01;
    private javax.swing.JLabel flatDesign01;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JList<String> jlMethodsCripto;
    private javax.swing.JList<String> jlMethodsFlat;
    private javax.swing.JList<String> jlMethodsTypes;
    private javax.swing.JPanel jpGlobalVariables;
    private javax.swing.JPanel jplCriptoDesign;
    private javax.swing.JPanel jplFlatDesign;
    private javax.swing.JPanel jplGenAlphCripto;
    private javax.swing.JPanel jplGenAlphFlat;
    private javax.swing.JPanel jplMethodsDesign;
    private javax.swing.JLabel lblGoToHome;
    private javax.swing.JLabel numericKey01;
    private javax.swing.JLabel phraseKey01;
    private javax.swing.JLabel phraseKeyDirection01;
    private javax.swing.JPanel pnlAssignConfigSystem;
    private javax.swing.JPanel pnlAssignSystem2;
    private javax.swing.JPanel pnlAssignSystem4;
    private javax.swing.JPanel pnlAssignSystem6;
    private javax.swing.JPanel pnlAssignSystem7;
    private javax.swing.JPanel pnlClose;
    private javax.swing.JPanel pnlGenerateCripto;
    private javax.swing.JPanel pnlGenerateFlat;
    private javax.swing.JPanel pnlGenerateGlobalNumericKey;
    private javax.swing.JPanel pnlMinimize;
    private javax.swing.JPanel pnlResetDefault;
    private javax.swing.JRadioButton rbGlobalPhraseFromEnd;
    private javax.swing.JRadioButton rbGlobalPhraseFromStart;
    private javax.swing.JTextArea txaCritpoGenerated;
    private javax.swing.JTextArea txaFlatGenerated;
    private javax.swing.JTextField txtGlobalNumericKey;
    private javax.swing.JTextField txtGlobalPhraseKey;
    private javax.swing.JLabel typeDesign01;
    // End of variables declaration//GEN-END:variables
}
