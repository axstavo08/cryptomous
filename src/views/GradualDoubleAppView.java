/*
 * This software is a university project developed by Gustavo Ramos Montalvo as a final work of Cryptography subject.
 */
package views;

import com.sun.awt.AWTUtilities;
import java.awt.Color;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;
import logic.entity.CryptoConstants;
import logic.entity.CryptoGradualDoubleAppConfiguration;
import logic.entity.CryptoNumbersSort;
import logic.entity.CryptoNumbersSortStructure;
import org.apache.commons.lang3.math.NumberUtils;
import resources.alerts.MandatoryFieldAlert;
import resources.alerts.MandatoryMethodsCompleteAlert;
import resources.alerts.OnlyNumbersAlert;
import resources.helpers.NumericKeyHelper;

/**
 *
 * @author Gustavo Ramos M.
 */
public class GradualDoubleAppView extends javax.swing.JFrame {

    private int posX, posY;
    private final CryptoGradualDoubleAppConfiguration configuration;
    private final CustomizeView oView;
    private final CryptoConstants.TypeCrypto oType;
    private final GradualDoubleAppView mainView;
    private final String keyMethod;
    private final Color incomplete = new Color(153, 38, 0), complete = new Color(204, 82, 0);
    private CryptoConstants.DoubleApplicationType typeApplication;

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public GradualDoubleAppView(CustomizeView view, CryptoConstants.TypeCrypto type, String key) {
        initComponents();
        @SuppressWarnings("OverridableMethodCallInConstructor")
        Shape shape = new RoundRectangle2D.Double(0, 0, this.getBounds().width, this.getBounds().height, 15, 15);
        AWTUtilities.setWindowShape(this, shape);
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/images/crypto_logo.png"));
        setIconImage(icon);
        oView = view;
        mainView = this;
        oType = type;
        keyMethod = key;
        configuration = new CryptoGradualDoubleAppConfiguration();
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setBackground(null);
        btnClose.setContentAreaFilled(false);
        pnlClose.setBackground(null);
        pnlSaveConfiguration.setBackground(null);
    }

    public static GradualDoubleAppView editConfigurationViewByParameters(CustomizeView view, CryptoConstants.TypeCrypto type, String methodKey, String numericKeyFirstApp,
            String numericKeySecondApp, CryptoNumbersSort orderingRestrictionFirstApp, CryptoNumbersSort extractingRestrictionFirstApp, CryptoNumbersSort orderingRestrictionSecondApp,
            CryptoNumbersSort extractingRestrictionSecondApp, boolean useGlobalNumericKeyFirstApp, boolean useGlobalNumericKeySecondApp) {
        GradualDoubleAppView wView = new GradualDoubleAppView(view, type, methodKey);
        wView.configuration.setIsConfigured(true);
        wView.configuration.setNumericKeyFirstApp(numericKeyFirstApp);
        wView.txtNumericKeyFirstApp.setText(numericKeyFirstApp);
        wView.configuration.setOrderingFirstAppRestriction(orderingRestrictionFirstApp);
        wView.jplOrderingRestrictionFirstApp.setBackground(new Color(204, 82, 0));
        wView.configuration.setExtractingFirstAppRestriction(extractingRestrictionFirstApp);
        wView.jplExtractingRestrictionFirstApp.setBackground(new Color(204, 82, 0));
        wView.configuration.setNumericKeySecondApp(numericKeySecondApp);
        wView.txtNumericKeySecondApp.setText(numericKeySecondApp);
        wView.configuration.setOrderingSecondAppRestriction(orderingRestrictionSecondApp);
        wView.jplOrderingRestrictionSecondApp.setBackground(new Color(204, 82, 0));
        wView.configuration.setExtractingSecondAppRestriction(extractingRestrictionSecondApp);
        wView.jplExtractingRestrictionSecondApp.setBackground(new Color(204, 82, 0));
        wView.configuration.setUseGlobalNumericKeyFirstApp(useGlobalNumericKeyFirstApp);
        wView.jcbUseGlobalNumericKeyFirstApp.setSelected(useGlobalNumericKeyFirstApp);
        wView.configuration.setUseGlobalNumericKeySecondApp(useGlobalNumericKeySecondApp);
        wView.jcbUseGlobalNumericKeySecondApp.setSelected(useGlobalNumericKeySecondApp);
        return wView;
    }

    public void generateNumericKey(String numericKey) {
        if (typeApplication != null) {
            switch (typeApplication) {
                case FIRST_APPLCATION:
                    txtNumericKeyFirstApp.setText(numericKey);
                    configuration.setNumericKeyFirstApp(numericKey);
                    break;
                case SECOND_APPLICATION:
                    txtNumericKeySecondApp.setText(numericKey);
                    configuration.setNumericKeySecondApp(numericKey);
                    break;
            }
        }
    }

    public void generateVariationNumber(CryptoConstants.GradualDoubleAppRestrictionNumber restriction, CryptoNumbersSort variation) {
        switch (restriction) {
            case ORDERING_FIRST_APP:
                configuration.setOrderingFirstAppRestriction(variation);
                jplOrderingRestrictionFirstApp.setBackground(complete);
                break;
            case EXTRACTING_FIRST_APP:
                configuration.setExtractingFirstAppRestriction(variation);
                jplExtractingRestrictionFirstApp.setBackground(complete);
                break;
            case ORDERING_SECOND_APP:
                configuration.setOrderingSecondAppRestriction(variation);
                jplOrderingRestrictionSecondApp.setBackground(complete);
                break;
            case EXTRACTING_SECOND_APP:
                configuration.setExtractingSecondAppRestriction(variation);
                jplExtractingRestrictionSecondApp.setBackground(complete);
                break;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        pnlClose = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        pnlMinimize = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnMinimize = new javax.swing.JButton();
        pnlSaveConfiguration = new javax.swing.JPanel();
        pnlSaveConfiguration1 = new javax.swing.JPanel();
        btnSaveConfiguration = new javax.swing.JButton();
        jpSuccessiveBounces1 = new javax.swing.JPanel();
        txtNumericKeySecondApp = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jplOrderingRestrictionSecondApp = new javax.swing.JPanel();
        jcbUseGlobalNumericKeySecondApp = new javax.swing.JCheckBox();
        jLabel15 = new javax.swing.JLabel();
        jplExtractingRestrictionSecondApp = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        pnlGenerateNumericKey1 = new javax.swing.JPanel();
        pnlSaveConfiguration4 = new javax.swing.JPanel();
        btnGenerateNumericKeySecondApp = new javax.swing.JButton();
        numericKey02 = new javax.swing.JLabel();
        jpSuccessiveBounces2 = new javax.swing.JPanel();
        txtNumericKeyFirstApp = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jplOrderingRestrictionFirstApp = new javax.swing.JPanel();
        jcbUseGlobalNumericKeyFirstApp = new javax.swing.JCheckBox();
        jLabel17 = new javax.swing.JLabel();
        jplExtractingRestrictionFirstApp = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        pnlGenerateNumericKey2 = new javax.swing.JPanel();
        pnlSaveConfiguration5 = new javax.swing.JPanel();
        btnGenerateNumericKeyFirstApp = new javax.swing.JButton();
        numericKey01 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Personalizar");
        setBackground(new java.awt.Color(35, 86, 104));
        setName("Personalizar"); // NOI18N
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(139, 195, 74));
        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jPanel4.setForeground(new java.awt.Color(255, 173, 51));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(104, 159, 56));
        jPanel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel5MouseDragged(evt);
            }
        });
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel5MousePressed(evt);
            }
        });
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/crypto_icon_01.png"))); // NOI18N
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 0, 30, 30));

        jLabel9.setBackground(new java.awt.Color(104, 159, 56));
        jLabel9.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Doble Aplicación Gradual");
        jPanel5.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 910, 30));

        pnlClose.setBackground(new java.awt.Color(204, 0, 0));
        pnlClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                pnlCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                pnlCloseMouseExited(evt);
            }
        });
        pnlClose.setLayout(null);

        btnClose.setBackground(new java.awt.Color(104, 159, 56));
        btnClose.setFont(new java.awt.Font("Eras Light ITC", 1, 18)); // NOI18N
        btnClose.setForeground(new java.awt.Color(255, 255, 255));
        btnClose.setText("X");
        btnClose.setToolTipText("Cerrar");
        btnClose.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 39, 60), 0, true));
        btnClose.setName(""); // NOI18N
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCloseMouseExited(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        pnlClose.add(btnClose);
        btnClose.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 0, 50, 30));

        pnlMinimize.setBackground(new java.awt.Color(104, 159, 56));
        pnlMinimize.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setLayout(null);
        pnlMinimize.add(jPanel6);
        jPanel6.setBounds(660, 0, 50, 30);

        btnMinimize.setBackground(new java.awt.Color(104, 159, 56));
        btnMinimize.setFont(new java.awt.Font("Eras Light ITC", 0, 48)); // NOI18N
        btnMinimize.setForeground(new java.awt.Color(255, 255, 255));
        btnMinimize.setText("-");
        btnMinimize.setToolTipText("Minimizar");
        btnMinimize.setBorder(null);
        btnMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnMinimizeMouseExited(evt);
            }
        });
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });
        pnlMinimize.add(btnMinimize);
        btnMinimize.setBounds(0, 0, 50, 30);

        jPanel5.add(pnlMinimize, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 0, 50, 30));

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 30));

        pnlSaveConfiguration.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlSaveConfiguration1.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlSaveConfiguration.add(pnlSaveConfiguration1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 400, 180, 40));

        btnSaveConfiguration.setBackground(new java.awt.Color(139, 195, 74));
        btnSaveConfiguration.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnSaveConfiguration.setForeground(new java.awt.Color(242, 242, 242));
        btnSaveConfiguration.setText("GUARDAR CONFIGURACIÓN");
        btnSaveConfiguration.setToolTipText("");
        btnSaveConfiguration.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(104, 159, 56), 1, true));
        btnSaveConfiguration.setContentAreaFilled(false);
        btnSaveConfiguration.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSaveConfiguration.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveConfigurationMouseExited(evt);
            }
        });
        btnSaveConfiguration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveConfigurationActionPerformed(evt);
            }
        });
        pnlSaveConfiguration.add(btnSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 180, 40));

        jPanel4.add(pnlSaveConfiguration, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 630, 180, 40));

        jpSuccessiveBounces1.setBackground(new java.awt.Color(104, 159, 56));
        jpSuccessiveBounces1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jpSuccessiveBounces1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtNumericKeySecondApp.setBackground(new java.awt.Color(242, 242, 242));
        txtNumericKeySecondApp.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtNumericKeySecondApp.setForeground(new java.awt.Color(13, 13, 13));
        txtNumericKeySecondApp.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNumericKeySecondApp.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtNumericKeySecondApp.setCaretColor(new java.awt.Color(13, 13, 13));
        txtNumericKeySecondApp.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtNumericKeySecondApp.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtNumericKeySecondApp.setSelectionStart(1);
        txtNumericKeySecondApp.setVerifyInputWhenFocusTarget(false);
        txtNumericKeySecondApp.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNumericKeySecondAppFocusLost(evt);
            }
        });
        txtNumericKeySecondApp.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                txtNumericKeySecondAppCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtNumericKeySecondAppInputMethodTextChanged(evt);
            }
        });
        txtNumericKeySecondApp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumericKeySecondAppKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNumericKeySecondAppKeyTyped(evt);
            }
        });
        jpSuccessiveBounces1.add(txtNumericKeySecondApp, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, 560, 40));

        jLabel12.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Clave numérica");
        jpSuccessiveBounces1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 100, 120, 40));

        jLabel14.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Ordenamiento");
        jpSuccessiveBounces1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 180, 140, 40));

        jplOrderingRestrictionSecondApp.setBackground(new java.awt.Color(153, 38, 0));
        jplOrderingRestrictionSecondApp.setForeground(new java.awt.Color(242, 242, 242));
        jplOrderingRestrictionSecondApp.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jplOrderingRestrictionSecondApp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jplOrderingRestrictionSecondAppMouseClicked(evt);
            }
        });
        jplOrderingRestrictionSecondApp.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jpSuccessiveBounces1.add(jplOrderingRestrictionSecondApp, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 170, 240, 50));

        jcbUseGlobalNumericKeySecondApp.setBackground(new java.awt.Color(104, 159, 56));
        jcbUseGlobalNumericKeySecondApp.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        jcbUseGlobalNumericKeySecondApp.setForeground(new java.awt.Color(242, 242, 242));
        jcbUseGlobalNumericKeySecondApp.setText("Usar clave numérica global");
        jcbUseGlobalNumericKeySecondApp.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jcbUseGlobalNumericKeySecondApp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbUseGlobalNumericKeySecondAppActionPerformed(evt);
            }
        });
        jpSuccessiveBounces1.add(jcbUseGlobalNumericKeySecondApp, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 60, 370, -1));

        jLabel15.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Extracción");
        jpSuccessiveBounces1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 180, 120, 40));

        jplExtractingRestrictionSecondApp.setBackground(new java.awt.Color(153, 38, 0));
        jplExtractingRestrictionSecondApp.setForeground(new java.awt.Color(242, 242, 242));
        jplExtractingRestrictionSecondApp.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jplExtractingRestrictionSecondApp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jplExtractingRestrictionSecondAppMouseClicked(evt);
            }
        });
        jplExtractingRestrictionSecondApp.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jpSuccessiveBounces1.add(jplExtractingRestrictionSecondApp, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 170, 240, 50));

        jLabel4.setBackground(new java.awt.Color(104, 159, 56));
        jLabel4.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 214, 204));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("SEGUNDA APLICACIÓN");
        jpSuccessiveBounces1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 920, 50));

        pnlGenerateNumericKey1.setBackground(new java.awt.Color(62, 94, 33));
        pnlGenerateNumericKey1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlSaveConfiguration4.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlGenerateNumericKey1.add(pnlSaveConfiguration4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 400, 180, 40));

        btnGenerateNumericKeySecondApp.setBackground(new java.awt.Color(104, 159, 56));
        btnGenerateNumericKeySecondApp.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnGenerateNumericKeySecondApp.setForeground(new java.awt.Color(242, 242, 242));
        btnGenerateNumericKeySecondApp.setText("GENERAR");
        btnGenerateNumericKeySecondApp.setToolTipText("");
        btnGenerateNumericKeySecondApp.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        btnGenerateNumericKeySecondApp.setContentAreaFilled(false);
        btnGenerateNumericKeySecondApp.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGenerateNumericKeySecondApp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGenerateNumericKeySecondAppMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGenerateNumericKeySecondAppMouseExited(evt);
            }
        });
        btnGenerateNumericKeySecondApp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateNumericKeySecondAppActionPerformed(evt);
            }
        });
        pnlGenerateNumericKey1.add(btnGenerateNumericKeySecondApp, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 40));

        jpSuccessiveBounces1.add(pnlGenerateNumericKey1, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 100, 110, 40));

        numericKey02.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        numericKey02.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        numericKey02.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                numericKey02MouseClicked(evt);
            }
        });
        jpSuccessiveBounces1.add(numericKey02, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 105, -1, 30));

        jPanel4.add(jpSuccessiveBounces1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 340, 920, 250));

        jpSuccessiveBounces2.setBackground(new java.awt.Color(104, 159, 56));
        jpSuccessiveBounces2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        jpSuccessiveBounces2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtNumericKeyFirstApp.setBackground(new java.awt.Color(242, 242, 242));
        txtNumericKeyFirstApp.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        txtNumericKeyFirstApp.setForeground(new java.awt.Color(13, 13, 13));
        txtNumericKeyFirstApp.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNumericKeyFirstApp.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        txtNumericKeyFirstApp.setCaretColor(new java.awt.Color(13, 13, 13));
        txtNumericKeyFirstApp.setSelectedTextColor(new java.awt.Color(242, 242, 242));
        txtNumericKeyFirstApp.setSelectionColor(new java.awt.Color(139, 195, 74));
        txtNumericKeyFirstApp.setSelectionStart(1);
        txtNumericKeyFirstApp.setVerifyInputWhenFocusTarget(false);
        txtNumericKeyFirstApp.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNumericKeyFirstAppFocusLost(evt);
            }
        });
        txtNumericKeyFirstApp.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                txtNumericKeyFirstAppCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtNumericKeyFirstAppInputMethodTextChanged(evt);
            }
        });
        txtNumericKeyFirstApp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumericKeyFirstAppKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNumericKeyFirstAppKeyTyped(evt);
            }
        });
        jpSuccessiveBounces2.add(txtNumericKeyFirstApp, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, 560, 40));

        jLabel13.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Clave numérica");
        jpSuccessiveBounces2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 100, 120, 40));

        jLabel16.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Ordenamiento");
        jpSuccessiveBounces2.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 180, 140, 40));

        jplOrderingRestrictionFirstApp.setBackground(new java.awt.Color(153, 38, 0));
        jplOrderingRestrictionFirstApp.setForeground(new java.awt.Color(242, 242, 242));
        jplOrderingRestrictionFirstApp.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jplOrderingRestrictionFirstApp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jplOrderingRestrictionFirstAppMouseClicked(evt);
            }
        });
        jplOrderingRestrictionFirstApp.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jpSuccessiveBounces2.add(jplOrderingRestrictionFirstApp, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 170, 240, 50));

        jcbUseGlobalNumericKeyFirstApp.setBackground(new java.awt.Color(104, 159, 56));
        jcbUseGlobalNumericKeyFirstApp.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        jcbUseGlobalNumericKeyFirstApp.setForeground(new java.awt.Color(242, 242, 242));
        jcbUseGlobalNumericKeyFirstApp.setText("Usar clave numérica global");
        jcbUseGlobalNumericKeyFirstApp.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jcbUseGlobalNumericKeyFirstApp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbUseGlobalNumericKeyFirstAppActionPerformed(evt);
            }
        });
        jpSuccessiveBounces2.add(jcbUseGlobalNumericKeyFirstApp, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 60, 370, -1));

        jLabel17.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 13)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Extracción");
        jpSuccessiveBounces2.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 180, 120, 40));

        jplExtractingRestrictionFirstApp.setBackground(new java.awt.Color(153, 38, 0));
        jplExtractingRestrictionFirstApp.setForeground(new java.awt.Color(242, 242, 242));
        jplExtractingRestrictionFirstApp.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jplExtractingRestrictionFirstApp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jplExtractingRestrictionFirstAppMouseClicked(evt);
            }
        });
        jplExtractingRestrictionFirstApp.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jpSuccessiveBounces2.add(jplExtractingRestrictionFirstApp, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 170, 240, 50));

        jLabel5.setBackground(new java.awt.Color(104, 159, 56));
        jLabel5.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 214, 204));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("PRIMERA APLICACIÓN");
        jpSuccessiveBounces2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 920, 50));

        pnlGenerateNumericKey2.setBackground(new java.awt.Color(62, 94, 33));
        pnlGenerateNumericKey2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlSaveConfiguration5.setBackground(new java.awt.Color(62, 94, 33));
        pnlSaveConfiguration5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        pnlGenerateNumericKey2.add(pnlSaveConfiguration5, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 400, 180, 40));

        btnGenerateNumericKeyFirstApp.setBackground(new java.awt.Color(104, 159, 56));
        btnGenerateNumericKeyFirstApp.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 11)); // NOI18N
        btnGenerateNumericKeyFirstApp.setForeground(new java.awt.Color(242, 242, 242));
        btnGenerateNumericKeyFirstApp.setText("GENERAR");
        btnGenerateNumericKeyFirstApp.setToolTipText("");
        btnGenerateNumericKeyFirstApp.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(62, 94, 33), 1, true));
        btnGenerateNumericKeyFirstApp.setContentAreaFilled(false);
        btnGenerateNumericKeyFirstApp.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGenerateNumericKeyFirstApp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnGenerateNumericKeyFirstAppMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnGenerateNumericKeyFirstAppMouseExited(evt);
            }
        });
        btnGenerateNumericKeyFirstApp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateNumericKeyFirstAppActionPerformed(evt);
            }
        });
        pnlGenerateNumericKey2.add(btnGenerateNumericKeyFirstApp, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, 40));

        jpSuccessiveBounces2.add(pnlGenerateNumericKey2, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 100, 110, 40));

        numericKey01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/interrogation_icon_01.png"))); // NOI18N
        numericKey01.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        numericKey01.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                numericKey01MouseClicked(evt);
            }
        });
        jpSuccessiveBounces2.add(numericKey01, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 105, -1, 30));

        jPanel4.add(jpSuccessiveBounces2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, 920, 250));

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseEntered
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(new Color(62, 94, 33));
    }//GEN-LAST:event_btnMinimizeMouseEntered

    private void btnMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnMinimizeMouseExited
        btnMinimize.setContentAreaFilled(false);
        pnlMinimize.setVisible(true);
        pnlMinimize.setBackground(null);
    }//GEN-LAST:event_btnMinimizeMouseExited

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void btnCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseEntered
        pnlClose.setVisible(true);
        pnlClose.setBackground(new Color(230, 0, 0));
    }//GEN-LAST:event_btnCloseMouseEntered

    private void btnCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseExited
        pnlClose.setBackground(null);
        btnClose.setVisible(true);
    }//GEN-LAST:event_btnCloseMouseExited

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void pnlCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseEntered

    }//GEN-LAST:event_pnlCloseMouseEntered

    private void pnlCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCloseMouseExited

    }//GEN-LAST:event_pnlCloseMouseExited

    private void jPanel5MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseDragged
        Point point = MouseInfo.getPointerInfo().getLocation();
        setLocation(point.x - posX, point.y - posY);
    }//GEN-LAST:event_jPanel5MouseDragged

    private void jPanel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MousePressed
        posX = evt.getX();
        posY = evt.getY();
    }//GEN-LAST:event_jPanel5MousePressed

    private void btnSaveConfigurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveConfigurationActionPerformed
        String field = txtNumericKeyFirstApp.getText();
        if (field.length() == 0) {
            String globalNumericKey = oView.getGlobalNumericKey();
            if (globalNumericKey == null || globalNumericKey.length() == 0) {
                new MandatoryFieldAlert(this, true, "clave numérica").setVisible(true);
                txtNumericKeyFirstApp.requestFocus();
                return;
            } else {
                configuration.setNumericKeyFirstApp(globalNumericKey);
            }
        }
        field = txtNumericKeySecondApp.getText();
        if (field.length() == 0) {
            String globalNumericKey = oView.getGlobalNumericKey();
            if (globalNumericKey == null || globalNumericKey.length() == 0) {
                new MandatoryFieldAlert(this, true, "clave numérica").setVisible(true);
                txtNumericKeySecondApp.requestFocus();
                return;
            } else {
                configuration.setNumericKeySecondApp(globalNumericKey);
            }
        }
        if (!configuration.getOrderingFirstAppRestriction().isIsConfigured()) {
            configuration.setOrderingFirstAppRestriction(new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                    new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null));
            /*new MandatoryMethodsCompleteAlert(this, true).setVisible(true);
            jplOrderingRestrictionFirstApp.setBackground(new Color(153, 38, 0));
            return;*/
        }
        if (!configuration.getExtractingFirstAppRestriction().isIsConfigured()) {
            configuration.setExtractingFirstAppRestriction(new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                    new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null));
            /*new MandatoryMethodsCompleteAlert(this, true).setVisible(true);
            jplExtractingRestrictionFirstApp.setBackground(new Color(153, 38, 0));
            return;*/
        }
        if (!configuration.getOrderingSecondAppRestriction().isIsConfigured()) {
            configuration.setOrderingSecondAppRestriction(new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                    new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null));
            /*new MandatoryMethodsCompleteAlert(this, true).setVisible(true);
            jplOrderingRestrictionSecondApp.setBackground(new Color(153, 38, 0));
            return;*/
        }
        if (!configuration.getExtractingSecondAppRestriction().isIsConfigured()) {
            configuration.setExtractingSecondAppRestriction(new CryptoNumbersSort(CryptoNumbersSort.TypeSort.NORMAL,
                    new CryptoNumbersSortStructure(CryptoNumbersSortStructure.DirectionSort.ASCENDANT, null), null));
            /*new MandatoryMethodsCompleteAlert(this, true).setVisible(true);
            jplExtractingRestrictionSecondApp.setBackground(new Color(153, 38, 0));
            return;*/
        }
        oView.setGradualDoubleAppConfiguration(oType, keyMethod, configuration.getNumericKeyFirstApp(), configuration.getNumericKeySecondApp(), configuration.getOrderingFirstAppRestriction(),
                configuration.getExtractingFirstAppRestriction(), configuration.getOrderingSecondAppRestriction(), configuration.getExtractingSecondAppRestriction(),
                configuration.isUseGlobalNumericKeyFirstApp(), configuration.isUseGlobalNumericKeySecondApp());
        dispose();
    }//GEN-LAST:event_btnSaveConfigurationActionPerformed

    private void btnSaveConfigurationMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseEntered
        pnlSaveConfiguration.setBackground(new Color(104, 159, 56));
    }//GEN-LAST:event_btnSaveConfigurationMouseEntered

    private void btnSaveConfigurationMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveConfigurationMouseExited
        pnlSaveConfiguration.setBackground(null);
    }//GEN-LAST:event_btnSaveConfigurationMouseExited

    private void txtNumericKeySecondAppFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNumericKeySecondAppFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumericKeySecondAppFocusLost

    private void txtNumericKeySecondAppCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtNumericKeySecondAppCaretPositionChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumericKeySecondAppCaretPositionChanged

    private void txtNumericKeySecondAppInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtNumericKeySecondAppInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumericKeySecondAppInputMethodTextChanged

    private void txtNumericKeySecondAppKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumericKeySecondAppKeyReleased
        String input = txtNumericKeySecondApp.getText();
        if (input.length() > 0) {
            String[] numbers = input.split(",");
            boolean isNumeric;
            for (String number : numbers) {
                isNumeric = NumberUtils.isParsable(number);
                if (!isNumeric) {
                    new OnlyNumbersAlert(this, true).setVisible(true);
                    txtNumericKeySecondApp.requestFocus();
                    return;
                }
            }
        }
    }//GEN-LAST:event_txtNumericKeySecondAppKeyReleased

    private void txtNumericKeySecondAppKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumericKeySecondAppKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumericKeySecondAppKeyTyped

    private void btnGenerateNumericKeySecondAppMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeySecondAppMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGenerateNumericKeySecondAppMouseEntered

    private void btnGenerateNumericKeySecondAppMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeySecondAppMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGenerateNumericKeySecondAppMouseExited

    private void btnGenerateNumericKeySecondAppActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeySecondAppActionPerformed
        typeApplication = CryptoConstants.DoubleApplicationType.SECOND_APPLICATION;
        new GenerateNumericKeyView(mainView).setVisible(true);
    }//GEN-LAST:event_btnGenerateNumericKeySecondAppActionPerformed

    private void jplOrderingRestrictionSecondAppMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jplOrderingRestrictionSecondAppMouseClicked
        if (configuration.getOrderingSecondAppRestriction().isIsConfigured()) {
            VariationNumericKeyView.editConfigurationViewByParameters(mainView, CryptoConstants.GradualDoubleAppRestrictionNumber.ORDERING_SECOND_APP,
                    configuration.getOrderingSecondAppRestriction()).setVisible(true);
        } else {
            new VariationNumericKeyView(mainView, CryptoConstants.GradualDoubleAppRestrictionNumber.ORDERING_SECOND_APP).setVisible(true);
        }
    }//GEN-LAST:event_jplOrderingRestrictionSecondAppMouseClicked

    private void jcbUseGlobalNumericKeySecondAppActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbUseGlobalNumericKeySecondAppActionPerformed
        if (jcbUseGlobalNumericKeySecondApp.isSelected()) {
            String key = oView.getGlobalNumericKey();
            if (key == null) {
                new MandatoryFieldAlert(this, true, "clave global").setVisible(true);
                jcbUseGlobalNumericKeySecondApp.setSelected(false);
                return;
            }
            configuration.setUseGlobalNumericKeySecondApp(true);
            txtNumericKeySecondApp.setText(key);
            configuration.setNumericKeySecondApp(key);
        } else {
            configuration.setUseGlobalNumericKeySecondApp(false);
        }
    }//GEN-LAST:event_jcbUseGlobalNumericKeySecondAppActionPerformed

    private void jplExtractingRestrictionSecondAppMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jplExtractingRestrictionSecondAppMouseClicked
        if (configuration.getExtractingSecondAppRestriction().isIsConfigured()) {
            VariationNumericKeyView.editConfigurationViewByParameters(mainView, CryptoConstants.GradualDoubleAppRestrictionNumber.EXTRACTING_SECOND_APP,
                    configuration.getExtractingSecondAppRestriction()).setVisible(true);
        } else {
            new VariationNumericKeyView(mainView, CryptoConstants.GradualDoubleAppRestrictionNumber.EXTRACTING_SECOND_APP).setVisible(true);
        }
    }//GEN-LAST:event_jplExtractingRestrictionSecondAppMouseClicked

    private void txtNumericKeyFirstAppFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNumericKeyFirstAppFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumericKeyFirstAppFocusLost

    private void txtNumericKeyFirstAppCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtNumericKeyFirstAppCaretPositionChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumericKeyFirstAppCaretPositionChanged

    private void txtNumericKeyFirstAppInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtNumericKeyFirstAppInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumericKeyFirstAppInputMethodTextChanged

    private void txtNumericKeyFirstAppKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumericKeyFirstAppKeyReleased
        String input = txtNumericKeyFirstApp.getText();
        if (input.length() > 0) {
            String[] numbers = input.split(",");
            boolean isNumeric;
            for (String number : numbers) {
                isNumeric = NumberUtils.isParsable(number);
                if (!isNumeric) {
                    new OnlyNumbersAlert(this, true).setVisible(true);
                    txtNumericKeyFirstApp.requestFocus();
                    return;
                }
            }
        }
    }//GEN-LAST:event_txtNumericKeyFirstAppKeyReleased

    private void txtNumericKeyFirstAppKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumericKeyFirstAppKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumericKeyFirstAppKeyTyped

    private void jplOrderingRestrictionFirstAppMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jplOrderingRestrictionFirstAppMouseClicked
        if (configuration.getOrderingFirstAppRestriction().isIsConfigured()) {
            VariationNumericKeyView.editConfigurationViewByParameters(mainView, CryptoConstants.GradualDoubleAppRestrictionNumber.ORDERING_FIRST_APP,
                    configuration.getOrderingFirstAppRestriction()).setVisible(true);
        } else {
            new VariationNumericKeyView(mainView, CryptoConstants.GradualDoubleAppRestrictionNumber.ORDERING_FIRST_APP).setVisible(true);
        }
    }//GEN-LAST:event_jplOrderingRestrictionFirstAppMouseClicked

    private void jcbUseGlobalNumericKeyFirstAppActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbUseGlobalNumericKeyFirstAppActionPerformed
        if (jcbUseGlobalNumericKeyFirstApp.isSelected()) {
            String key = oView.getGlobalNumericKey();
            if (key == null) {
                new MandatoryFieldAlert(this, true, "clave global").setVisible(true);
                jcbUseGlobalNumericKeyFirstApp.setSelected(false);
                return;
            }
            configuration.setUseGlobalNumericKeyFirstApp(true);
            txtNumericKeyFirstApp.setText(key);
            configuration.setNumericKeyFirstApp(key);
        } else {
            configuration.setUseGlobalNumericKeyFirstApp(false);
        }
    }//GEN-LAST:event_jcbUseGlobalNumericKeyFirstAppActionPerformed

    private void jplExtractingRestrictionFirstAppMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jplExtractingRestrictionFirstAppMouseClicked
        if (configuration.getExtractingFirstAppRestriction().isIsConfigured()) {
            VariationNumericKeyView.editConfigurationViewByParameters(mainView, CryptoConstants.GradualDoubleAppRestrictionNumber.EXTRACTING_FIRST_APP,
                    configuration.getExtractingFirstAppRestriction()).setVisible(true);
        } else {
            new VariationNumericKeyView(mainView, CryptoConstants.GradualDoubleAppRestrictionNumber.EXTRACTING_FIRST_APP).setVisible(true);
        }
    }//GEN-LAST:event_jplExtractingRestrictionFirstAppMouseClicked

    private void btnGenerateNumericKeyFirstAppMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeyFirstAppMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGenerateNumericKeyFirstAppMouseEntered

    private void btnGenerateNumericKeyFirstAppMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeyFirstAppMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGenerateNumericKeyFirstAppMouseExited

    private void btnGenerateNumericKeyFirstAppActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateNumericKeyFirstAppActionPerformed
        typeApplication = CryptoConstants.DoubleApplicationType.FIRST_APPLCATION;
        new GenerateNumericKeyView(mainView).setVisible(true);
    }//GEN-LAST:event_btnGenerateNumericKeyFirstAppActionPerformed

    private void numericKey02MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_numericKey02MouseClicked
        new NumericKeyHelper(this, true).setVisible(true);
    }//GEN-LAST:event_numericKey02MouseClicked

    private void numericKey01MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_numericKey01MouseClicked
        new NumericKeyHelper(this, true).setVisible(true);
    }//GEN-LAST:event_numericKey01MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GradualDoubleAppView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new GradualDoubleAppView(null, null, null).setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnGenerateNumericKeyFirstApp;
    private javax.swing.JButton btnGenerateNumericKeySecondApp;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JButton btnSaveConfiguration;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JCheckBox jcbUseGlobalNumericKeyFirstApp;
    private javax.swing.JCheckBox jcbUseGlobalNumericKeySecondApp;
    private javax.swing.JPanel jpSuccessiveBounces1;
    private javax.swing.JPanel jpSuccessiveBounces2;
    private javax.swing.JPanel jplExtractingRestrictionFirstApp;
    private javax.swing.JPanel jplExtractingRestrictionSecondApp;
    private javax.swing.JPanel jplOrderingRestrictionFirstApp;
    private javax.swing.JPanel jplOrderingRestrictionSecondApp;
    private javax.swing.JLabel numericKey01;
    private javax.swing.JLabel numericKey02;
    private javax.swing.JPanel pnlClose;
    private javax.swing.JPanel pnlGenerateNumericKey1;
    private javax.swing.JPanel pnlGenerateNumericKey2;
    private javax.swing.JPanel pnlMinimize;
    private javax.swing.JPanel pnlSaveConfiguration;
    private javax.swing.JPanel pnlSaveConfiguration1;
    private javax.swing.JPanel pnlSaveConfiguration4;
    private javax.swing.JPanel pnlSaveConfiguration5;
    private javax.swing.JTextField txtNumericKeyFirstApp;
    private javax.swing.JTextField txtNumericKeySecondApp;
    // End of variables declaration//GEN-END:variables
}
